(* ::Package:: *)

(*General Relativity calculator based on James B. Hartle Mathematica's notebooks (http://web.physics.ucsb.edu/~gravitybook/mathematica.html)*)

(*Author: Giovanni Formighieri (giovform@gmail.com). Please report any bugs to this e-mail. Code comments will be added in future releases. Follow the example notebook for instructions.*)

$Assumptions=True;
Quiet[Remove["Global`*"]];

fieldEquationsRHSConstant=(8*Pi*G)/(c^4);
hiddenArgsOutputFunctionList={};

Setup[coordinatesArg_,metricTensorSignatureArg_,covMetricTensorOrLineElement_]:=Module[{},
    coordinates=coordinatesArg;
    metricTensorSignature=metricTensorSignatureArg;
    nCoords=Length[coordinates];
    timeComponentSign=-1*Commonest[metricTensorSignature][[1]];
    timeComponentPosition=Position[metricTensorSignature,timeComponentSign][[1,1]];
    If[Head[covMetricTensorOrLineElement]===List,
        covMetricTensor=covMetricTensorOrLineElement;
        lineElement=CovariantMetricTensorToLineElement[covMetricTensor];
    ,
        lineElement=covMetricTensorOrLineElement;
        covMetricTensor=LineElementToCovariantMetricTensor[lineElement];
    ];
    conMetricTensor=Inverse[covMetricTensor];
    covMetricTensorDeterminant=Simplify[Det[covMetricTensor]];
]

Calculate[covStressEnergyTensorArg_ :Null]:=Module[{},
    covStressEnergyTensor=If[SameQ[covStressEnergyTensorArg,Null],ConstantArray[0,{nCoords,nCoords}],covStressEnergyTensorArg];
    conStressEnergyTensor=CovariantToContravariant[covStressEnergyTensor];
    mixStressEnergyTensor=CovariantToMixed[covStressEnergyTensor];
    affineConnections=Simplify[Table[(1/2)*Sum[(conMetricTensor[[dvi,dvl]])*(D[covMetricTensor[[dvk,dvl]],coordinates[[dvj]]]+D[covMetricTensor[[dvj,dvl]],coordinates[[dvk]]]-D[covMetricTensor[[dvj,dvk]],coordinates[[dvl]]]),{dvl,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords},{dvk,1,nCoords}]];
    mixRiemannTensor=Simplify[Table[D[affineConnections[[dvi,dvl,dvj]],coordinates[[dvk]]]-D[affineConnections[[dvi,dvk,dvj]],coordinates[[dvl]]]+Sum[affineConnections[[dvi,dvk,dvm]]*affineConnections[[dvm,dvl,dvj]]-affineConnections[[dvi,dvl,dvm]]*affineConnections[[dvm,dvk,dvj]],{dvm,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords},{dvk,1,nCoords},{dvl,1,nCoords}]];
    covRicciTensor=Simplify[Table[Sum[mixRiemannTensor[[dvk,dvi,dvk,dvj]],{dvk,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords}]];
    conRicciTensor=CovariantToContravariant[covRicciTensor];
    mixRicciTensor=CovariantToMixed[covRicciTensor];
    ricciScalar1=Simplify[Sum[mixRicciTensor[[dvi,dvi]],{dvi,1,nCoords}]];
    ricciScalar2=Simplify[-fieldEquationsRHSConstant*Sum[mixStressEnergyTensor[[dvi,dvi]],{dvi,1,nCoords}]];
    covEinsteinTensor1=Simplify[covRicciTensor-(1/2)*ricciScalar1*covMetricTensor];
    conEinsteinTensor1=CovariantToContravariant[covEinsteinTensor1];
    mixEinsteinTensor1=CovariantToMixed[covEinsteinTensor1];
    covEinsteinTensor2=Simplify[covRicciTensor-(1/2)*ricciScalar2*covMetricTensor];
    conEinsteinTensor2=CovariantToContravariant[covEinsteinTensor2];
    mixEinsteinTensor2=CovariantToMixed[covEinsteinTensor2];
    covFieldEquations1=Table[covEinsteinTensor1[[dvi,dvj]]==fieldEquationsRHSConstant*covStressEnergyTensor[[dvi,dvj]],{dvi,1,nCoords},{dvj,1,nCoords}];
    conFieldEquations1=Table[conEinsteinTensor1[[dvi,dvj]]==fieldEquationsRHSConstant*conStressEnergyTensor[[dvi,dvj]],{dvi,1,nCoords},{dvj,1,nCoords}];
    mixFieldEquations1=Table[mixEinsteinTensor1[[dvi,dvj]]==fieldEquationsRHSConstant*mixStressEnergyTensor[[dvi,dvj]],{dvi,1,nCoords},{dvj,1,nCoords}];
    covFieldEquations2=Table[covEinsteinTensor2[[dvi,dvj]]==fieldEquationsRHSConstant*covStressEnergyTensor[[dvi,dvj]],{dvi,1,nCoords},{dvj,1,nCoords}];
    conFieldEquations2=Table[conEinsteinTensor2[[dvi,dvj]]==fieldEquationsRHSConstant*conStressEnergyTensor[[dvi,dvj]],{dvi,1,nCoords},{dvj,1,nCoords}];
    mixFieldEquations2=Table[mixEinsteinTensor2[[dvi,dvj]]==fieldEquationsRHSConstant*mixStressEnergyTensor[[dvi,dvj]],{dvi,1,nCoords},{dvj,1,nCoords}];
    simplifiedCovFieldEquations1=Simplify[covFieldEquations1];
    simplifiedConFieldEquations1=Simplify[conFieldEquations1];
    simplifiedMixFieldEquations1=Simplify[mixFieldEquations1];
    simplifiedCovFieldEquations2=Simplify[covFieldEquations2];
    simplifiedConFieldEquations2=Simplify[conFieldEquations2];
    simplifiedMixFieldEquations2=Simplify[mixFieldEquations2];
]

CalculateCovariantPerfectFluidStressEnergyTensor[]:=Module[{},
    covFourVelocity=ReplacePart[{0,0,0,0},timeComponentPosition->timecomponent];
    covFourVelocity[[timeComponentPosition]]=Solve[Sum[Sum[conMetricTensor[[dvi,dvj]]*covFourVelocity[[dvi]]*covFourVelocity[[dvj]],{dvi,1,nCoords}],{dvj,1,nCoords}]==timeComponentSign*c^2,covFourVelocity[[timeComponentPosition]]][[1,1,2]];
    covPerfectFluidStressEnergyTensor=Simplify[Table[(\[Rho]+(p/(c^2)))*covFourVelocity[[dvi]]*covFourVelocity[[dvj]]-timeComponentSign*p*covMetricTensor[[dvi,dvj]],{dvi,1,nCoords},{dvj,1,nCoords}]];
    conPerfectFluidStressEnergyTensor=CovariantToContravariant[covPerfectFluidStressEnergyTensor];
    mixPerfectFluidStressEnergyTensor=CovariantToMixed[covPerfectFluidStressEnergyTensor];
    AddAssumption[\[Rho]>0];
    AddAssumption[p>0];
    covPerfectFluidStressEnergyTensor
]

CalculateCovariantElectromagneticStressEnergyTensor[covElectromagneticTensorArg_]:=Module[{},
    covElectromagneticTensor=covElectromagneticTensorArg;
    conElectromagneticTensor=CovariantToContravariant[covElectromagneticTensor];
    mixElectromagneticTensor=CovariantToMixed[covElectromagneticTensor];
    covElectromagneticStressEnergytensor=Simplify[Table[-timeComponentSign*(1/\[Mu]0)*(Sum[CovariantToMixed[covElectromagneticTensor][[dvi,dvk]]*covElectromagneticTensor[[dvj,dvk]],{dvk,1,nCoords}]-(1/4)*Sum[Sum[covMetricTensor[[dvi,dvj]]*covElectromagneticTensor[[dvk,dvl]]*conElectromagneticTensor[[dvk,dvl]],{dvk,1,nCoords}],{dvl,1,nCoords}]),{dvi,1,nCoords},{dvj,1,nCoords}]];
    conElectromagneticStressEnergytensor=CovariantToContravariant[covElectromagneticStressEnergytensor];    
    mixElectromagneticStressEnergytensor=CovariantToMixed[covElectromagneticStressEnergytensor];
    AddAssumption[\[Mu]0>0];
    covElectromagneticStressEnergytensor
]

(*Print functions*)
PrintCoordinates[]:=TraditionalForm[StringReplace[ToString[coordinates],{"{"->"(","}"->")"}]]
PrintMetricTensorSignature[]:=TraditionalForm[StringReplace[ToString[metricTensorSignature],{"{"->"(","}"->")","-1"->"-","1"->"+"}]]
PrintCovariantMetricTensor[]:=PrintMatrix[covMetricTensor,"g","\[Mu]\[Nu]",""]
PrintContravariantMetricTensor[]:=PrintMatrix[conMetricTensor,"g","","\[Mu]\[Nu]"]
PrintLineElement[]:=PrintMatrix[lineElement,"ds","","2"]
PrintAssumptions[]:=pdPrint[Table[$Assumptions[[dvi]],{dvi,1,Length[$Assumptions]}],1]
PrintCovariantMetricTensorDeterminant[]:=PrintMatrix[covMetricTensorDeterminant,"g","",""]
PrintCovariantPerfectFluidStressEnergyTensor[]:=PrintMatrix[covPerfectFluidStressEnergyTensor,"L","\[Mu]\[Nu]",""]
PrintContravariantPerfectFluidStressEnergyTensor[]:=PrintMatrix[conPerfectFluidStressEnergyTensor,"L","","\[Mu]\[Nu]"]
PrintMixedPerfectFluidStressEnergyTensor[]:=PrintMatrix[mixPerfectFluidStressEnergyTensor,"L","\[Mu]","\[Nu]"]
PrintCovariantElectromagneticTensor[]:=PrintMatrix[covElectromagneticTensor,"F","\[Mu]\[Nu]",""]
PrintContravariantElectromagneticTensor[]:=PrintMatrix[conElectromagneticTensor,"F","","\[Mu]\[Nu]"]
PrintMixedElectromagneticTensor[]:=PrintMatrix[mixElectromagneticTensor,"F","\[Mu]","\[Nu]"]
PrintCovariantElectromagneticStressEnergyTensor[]:=PrintMatrix[covElectromagneticStressEnergytensor,"U","\[Mu]\[Nu]",""]
PrintContravariantElectromagneticStressEnergyTensor[]:=PrintMatrix[conElectromagneticStressEnergytensor,"U","","\[Mu]\[Nu]"]
PrintMixedElectromagneticStressEnergyTensor[]:=PrintMatrix[mixElectromagneticStressEnergytensor,"U","\[Mu]","\[Nu]"]
PrintCovariantStressEnergyTensor[]:=PrintMatrix[covStressEnergyTensor,"T","\[Mu]\[Nu]",""]
PrintContravariantStressEnergyTensor[]:=PrintMatrix[conStressEnergyTensor,"T","","\[Mu]\[Nu]"]
PrintMixedStressEnergyTensor[]:=PrintMatrix[mixStressEnergyTensor,"T","\[Mu]","\[Nu]"]
PrintAffineConnections[]:=pdPrint[Table[If[UnsameQ[affineConnections[[dvi,dvj,dvk]],0],{Subsuperscript["\[CapitalGamma]",StringJoin["  ",ToString[coordinates[[dvj]]],ToString[coordinates[[dvk]]]],StringJoin[ToString[coordinates[[dvi]]]]]," = ",affineConnections[[dvi,dvj,dvk]],"                    "}],{dvi,1,nCoords},{dvj,1,nCoords},{dvk,1,nCoords}],12]
PrintMixedRiemannTensor[]:=pdPrint[Table[If[UnsameQ[mixRiemannTensor[[dvi,dvj,dvk,dvl]],0],{Subsuperscript["R",StringJoin["  ",ToString[coordinates[[dvj]]],ToString[coordinates[[dvk]]],ToString[coordinates[[dvl]]]],StringJoin[ToString[coordinates[[dvi]]]]]," = ",mixRiemannTensor[[dvi,dvj,dvk,dvl]],"                    "}],{dvi,1,nCoords},{dvj,1,nCoords},{dvk,1,nCoords},{dvl,1,nCoords}],8]
PrintCovariantRicciTensor[]:=pdPrint[Table[If[UnsameQ[covRicciTensor[[dvi,dvj]],0],{Subscript["R",StringJoin[ToString[coordinates[[dvi]]],ToString[coordinates[[dvj]]]]]," = ",covRicciTensor[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintContravariantRicciTensor[]:=pdPrint[Table[If[UnsameQ[conRicciTensor[[dvi,dvj]],0],{Superscript["R",StringJoin[ToString[coordinates[[dvi]]],ToString[coordinates[[dvj]]]]]," = ",conRicciTensor[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintMixedRicciTensor[]:=pdPrint[Table[If[UnsameQ[mixRicciTensor[[dvi,dvj]],0],{Subsuperscript["R",StringJoin[ToString[coordinates[[dvj]]]],StringJoin["  ",ToString[coordinates[[dvi]]]]]," = ",mixRicciTensor[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintRicciScalar[]:=pdPrint[Table[{"R"," = ",ricciScalar1," = ",ricciScalar2},{1}],5]
PrintCovariantEinsteinTensor1[]:=pdPrint[Table[If[UnsameQ[covEinsteinTensor1[[dvi,dvj]],0],{Subscript["G",StringJoin[ToString[coordinates[[dvi]]],ToString[coordinates[[dvj]]]]]," = ",covEinsteinTensor1[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintContravariantEinsteinTensor1[]:=pdPrint[Table[If[UnsameQ[conEinsteinTensor1[[dvi,dvj]],0],{Superscript["G",StringJoin[ToString[coordinates[[dvi]]],ToString[coordinates[[dvj]]]]]," = ",conEinsteinTensor1[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintMixedEinsteinTensor1[]:=pdPrint[Table[If[UnsameQ[mixEinsteinTensor1[[dvi,dvj]],0],{Subsuperscript["G",StringJoin[ToString[coordinates[[dvj]]]],StringJoin["  ",ToString[coordinates[[dvi]]]]]," = ",mixEinsteinTensor1[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintCovariantEinsteinTensor2[]:=pdPrint[Table[If[UnsameQ[covEinsteinTensor2[[dvi,dvj]],0],{Subscript["G",StringJoin[ToString[coordinates[[dvi]]],ToString[coordinates[[dvj]]]]]," = ",covEinsteinTensor2[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintContravariantEinsteinTensor2[]:=pdPrint[Table[If[UnsameQ[conEinsteinTensor2[[dvi,dvj]],0],{Superscript["G",StringJoin[ToString[coordinates[[dvi]]],ToString[coordinates[[dvj]]]]]," = ",conEinsteinTensor2[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintMixedEinsteinTensor2[]:=pdPrint[Table[If[UnsameQ[mixEinsteinTensor2[[dvi,dvj]],0],{Subsuperscript["G",StringJoin[ToString[coordinates[[dvj]]]],StringJoin["  ",ToString[coordinates[[dvi]]]]]," = ",mixEinsteinTensor2[[dvi,dvj]]}],{dvi,1,nCoords},{dvj,1,nCoords}],3]
PrintCovariantFieldEquations1[]:=PrintFieldEquations[covFieldEquations1]
PrintContravariantFieldEquations1[]:=PrintFieldEquations[conFieldEquations1]
PrintMixedFieldEquations1[]:=PrintFieldEquations[mixFieldEquations1]
PrintCovariantFieldEquations2[]:=PrintFieldEquations[covFieldEquations2]
PrintContravariantFieldEquations2[]:=PrintFieldEquations[conFieldEquations2]
PrintMixedFieldEquations2[]:=PrintFieldEquations[mixFieldEquations2]
PrintSimplifiedCovariantFieldEquations1[]:=PrintFieldEquations[simplifiedCovFieldEquations1]
PrintSimplifiedContravariantFieldEquations1[]:=PrintFieldEquations[simplifiedConFieldEquations1]
PrintSimplifiedMixedFieldEquations1[]:=PrintFieldEquations[simplifiedMixFieldEquations1]
PrintSimplifiedCovariantFieldEquations2[]:=PrintFieldEquations[simplifiedCovFieldEquations2]
PrintSimplifiedContravariantFieldEquations2[]:=PrintFieldEquations[simplifiedConFieldEquations2]
PrintSimplifiedMixedFieldEquations2[]:=PrintFieldEquations[simplifiedMixFieldEquations2]

(*Tensor conversion functions*)
CovariantToContravariant[tensor_]:=Simplify[Table[Sum[Sum[conMetricTensor[[dvi,dvk]]*conMetricTensor[[dvj,dvl]]*tensor[[dvk,dvl]],{dvk,1,nCoords}],{dvl,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords}]]
ContravariantToCovariant[tensor_]:=Simplify[Table[Sum[Sum[covMetricTensor[[dvi,dvk]]*covMetricTensor[[dvj,dvl]]*tensor[[dvk,dvl]],{dvk,1,nCoords}],{dvl,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords}]]
CovariantToMixed[tensor_]:=Simplify[Table[Sum[conMetricTensor[[dvj,dvk]]*tensor[[dvi,dvk]],{dvk,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords}]]
ContravariantToMixed[tensor_]:=Simplify[Table[Sum[covMetricTensor[[dvi,dvk]]*tensor[[dvk,dvj]],{dvk,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords}]]
MixedToCovariant[tensor_]:=Simplify[Table[Sum[covMetricTensor[[dvj,dvk]]*tensor[[dvi,dvk]],{dvk,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords}]]
MixedToContravariant[tensor_]:=Simplify[Table[Sum[conMetricTensor[[dvj,dvk]]*tensor[[dvi,dvk]],{dvk,1,nCoords}],{dvi,1,nCoords},{dvj,1,nCoords}]]

(*Friendly printing functions*)
pdConv[f_]:=Module[{convertedExpression},
    convertedExpression=TraditionalForm[f/.Derivative[inds__][g_][vars__]:>Apply[Defer[D[g[vars],##]]&,Transpose[{{vars},{inds}}]/.{{var_,0}:>Sequence[],{var_,1}:>{var}}]];
    If[hiddenArgsOutputFunctionList=!={},convertedExpression=convertedExpression/.Table[hiddenArgsOutputFunctionList[[dvi]]->Head[hiddenArgsOutputFunctionList[[dvi]]],{dvi,1,Length[hiddenArgsOutputFunctionList]}]];
    convertedExpression
]
pdPrint[list_,nCols_]:=pdConv[TableForm[Partition[DeleteCases[Flatten[list],Null],nCols,nCols,{1,1},{}],TableSpacing->{5,0}]]
PrintExpression[expression_ :Null]:=pdConv[If[UnsameQ[expression,Null],expression,%]]
PrintMatrix[matrix_,letter_,subscript_,superscript_]:=pdPrint[{Subsuperscript[letter,subscript,If[UnsameQ[subscript,""],StringJoin["  ",superscript],superscript]]," = ", MatrixForm[matrix]},3]
PrintFieldEquations[equations_]:=Module[{dvi,dvj,dvk,dvl,equationsTemp,equationsList,equationsLabel},
    equationsTemp=equations;
    equationsList={};
    For[dvi=1,dvi<nCoords+1,dvi++,For[dvj=1,dvj<nCoords+1,dvj++,
        If[UnsameQ[equationsTemp[[dvi,dvj]],True],
            equationsLabel=StringJoin["(",ToString[coordinates[[dvi]]],ToString[coordinates[[dvj]]],")"];
            For[dvk=1,dvk<nCoords+1,dvk++,For[dvl=1,dvl<nCoords+1,dvl++,
                If[dvk!=dvi&&dvl!=dvj&&UnsameQ[equationsTemp[[dvk,dvl]],True]
                    &&(SameQ[ExpandAll[HomogenizeEquation[simplifiedMixFieldEquations1[[dvi,dvj]]][[1]]],ExpandAll[HomogenizeEquation[simplifiedMixFieldEquations1[[dvk,dvl]]][[1]]]]||
                       SameQ[(-1)*ExpandAll[HomogenizeEquation[simplifiedMixFieldEquations1[[dvi,dvj]]][[1]]],ExpandAll[HomogenizeEquation[simplifiedMixFieldEquations1[[dvk,dvl]]][[1]]]]),
                    equationsTemp[[dvk,dvl]]=True;
                    equationsLabel=StringJoin[equationsLabel," (",ToString[coordinates[[dvk]]],ToString[coordinates[[dvl]]],")"];
                ];
            ]];
            equationsList=Append[equationsList,{StringJoin[equationsLabel,"        "],equationsTemp[[dvi,dvj]]}];
        ];
    ]];
    pdPrint[equationsList,2]
]

(*Other functions*)
AddHiddenArgsOutputFunction[hiddenArgsOutputFunction_]:=AppendTo[hiddenArgsOutputFunctionList,hiddenArgsOutputFunction]
AddAssumption[assumption_]:=$Assumptions=$Assumptions&&assumption
RuleToEquation[rule_]:=rule[[1]]==rule[[2]]
EquationToRule[equation_]:=equation[[1]]->equation[[2]]
HomogenizeEquation[equation_]:=equation[[1]]-equation[[2]]==0
GetAlternateExpressionForms[expression_]:=Module[{alternateFormData},
    alternateFormData={};
    alternateFormData=Quiet[Check[TimeConstrained[ReleaseHold[WolframAlpha[ToString[expression,InputForm],{"AlternateForm","Input"}]],60],{}]];
    Flatten[Table[alternateFormData[[dvi,2]],{dvi,Length[alternateFormData]}]]
]
GetSIPhysicalConstantValue[constantName_]:=QuantityMagnitude[UnitConvert[Quantity[constantName]]]
ExpressionTermsList[expression_]:=Flatten[{Replace[expression,HoldPattern[Plus[a___]]:>{a}]}]
LineElementToCovariantMetricTensor[lineElement_]:=Module[{infinitesimals,termsList,indexList},
    infinitesimals=ToExpression[Map[Function[StringJoin["d",#]],Map[ToString,coordinates]]];
    termsList=ExpressionTermsList[Collect[lineElement,infinitesimals]];
    covMetricTensor=IdentityMatrix[Length[coordinates]];
    Do[
        indexList={};
        Do[
            If[!FreeQ[termsList[[dvi]],infinitesimals[[dvj]]],AppendTo[indexList,dvj]]
        ,{dvj,1,Length[infinitesimals]}];
        If[Length[indexList]==1,AppendTo[indexList,indexList[[1]]],termsList[[dvi]]=termsList[[dvi]]/2];
        covMetricTensor[[indexList/.List->Sequence]]=termsList[[dvi]];
        covMetricTensor[[Reverse[indexList]/.List->Sequence]]=termsList[[dvi]];
    ,{dvi,1,Length[termsList]}];
    covMetricTensor/.Table[infinitesimals[[dvi]]->1,{dvi,1,Length[infinitesimals]}]/.c->1
]
CovariantMetricTensorToLineElement[covMetricTensor_]:=Module[{infinitesimals},
    infinitesimals=ToExpression[Map[Function[StringJoin["d",#]],Map[ToString,coordinates]]];
    infinitesimals[[timeComponentPosition]]=infinitesimals[[timeComponentPosition]]*c;
    Sum[Sum[covMetricTensor[[dvi,dvj]]*infinitesimals[[dvi]]*infinitesimals[[dvj]],{dvi,1,nCoords}],{dvj,1,nCoords}]
]
BuildFunction[name_,parameters_,body_]:=Evaluate[name]->Function[Evaluate[parameters],Evaluate[body]]
BuildFunctionFromRule[rule_]:=Module[{name,parameters,body},
    name=Head[rule[[1]]];
    parameters=Table[rule[[1]][[dvi]],{dvi,1,Length[rule[[1]]]}];
    body=rule[[2]];
    BuildFunction[name,parameters,body]
]

AddAssumption[G>0];
AddAssumption[c>0];
