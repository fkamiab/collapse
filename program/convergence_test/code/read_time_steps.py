import numpy as np
import pylab as pl
import matplotlib as mpl
import plot as plot

def read_variables(number_variables, space_res, time_res, final_time):

    curve_number=number_variables
    output_res=time_res
    final_time=final_time

    r=np.empty([curve_number, space_res])
    time=np.empty_like(r[:,1])
    p=np.empty_like(r)
    Paether=np.empty_like(r)
    ur=np.empty_like(r)
    alpha=np.empty_like(r)
    a=np.empty_like(r)
    b=np.empty_like(r)
    K=np.empty_like(r)
    Sr_tilde=np.empty_like(r)
    tau_tilde=np.empty_like(r)

    
    for x in range(0, curve_number):
        i=np.int(x*(output_res)/((curve_number-1)))
        time[x]=final_time*i/(output_res)
        filename="../data/res{}/time_step_{}.out".format(space_res,i)
        r[x], p[x], Paether[x], ur[x], alpha[x], a[x], b[x], K[x], Sr_tilde[x], tau_tilde[x] =np.loadtxt(filename, skiprows=1, unpack=True)
        
    return r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde, time 
