import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
import math

def giveB():
    r0, p0, ptilde0, m0 = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    #r0, p0, Paether0, m0, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)

    NS_radius=1.33478413497
    NS_M= 0.188869961957

    pprime0=der.first(p0, r0)

    

    p_inter = interpolate.interp1d(r0, p0, kind='cubic')
    pprime_inter = interpolate.interp1d(r0, pprime0, kind='cubic')
    #ptilde_inter = interpolate.interp1d(r0, ptilde0, kind='cubic')
    m_inter = interpolate.interp1d(r0, m0, kind='cubic')
    
    r=np.linspace(0.0, NS_radius, 200)
    p=p_inter(r)
    #ptilde=ptilde_inter(r)
    m=m_inter(r)
    pprime=pprime_inter(r)

    p_inter = interpolate.interp1d(r, p, kind='cubic')
    pprime_inter = interpolate.interp1d(r, pprime, kind='cubic')

   

    

    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    
    
    B_0=1.0-2.0*NS_M/NS_radius
    p_0=p_inter(NS_radius)


    def get_r(tau):
        return NS_radius-tau

    state0= np.array([B_0])
    tau0=0.0
    state=state0
    def f(tau, state):
        r = get_r(tau)
        B = state[0]
        fB=B*(2.0*pprime_inter(get_r(tau))/(rho(p_inter(get_r(tau)))+p_inter(get_r(tau))))
        return np.array([fB])
    state_int = ode(f)
    state_int.set_integrator('dopri5',max_step=1E-3)
    state_int.set_initial_value(state0, tau0)
    tau_array=np.linspace(0.0, NS_radius, 200)
    state_soln = np.empty((len(tau_array),len(state0)))
    state_soln[0] = state0
    for i,tau in enumerate(tau_array[1:]):
        state_int.integrate(tau)
        state_soln[i+1] = state_int.y
    ###########################################################################

    B=state_soln[..., 0]
    #pl.plot(r0[1:], np.sqrt(1-2.0*m0[1:]/r0[1:]))
    return get_r(tau_array)[::-1], np.sqrt(B[::-1])

    




#if __name__ == "__main__":
    
  
