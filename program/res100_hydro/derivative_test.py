import derivative as der
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as pl
import global_variables as gv

r = np.linspace(0,4,39*gv.RES)
test = 0.5*(np.tanh(6*gv.radius_truncation_factor*(1.4-r)))
#test2 = 0.5*(np.tanh(6*gv.radius_truncation_factor*(1.4-r)))+0.1
pl.plot(r,der.first(test,r))#-der.first(test2,r))
pl.show()
