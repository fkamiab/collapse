STEP=1E-4
ptilde_RES=5000
EOS_RES=100+1
EOS_lp_min=-12.0
EOS_lp_max=1.0
K=1.0 #Factor in the equation of state
#epsilon=4.0312e-3
epsilon=5.0e-3
#Gamma=27.0/15.0 #Power in the equation of state
Gamma=27.0/15.0 #Power in the equation of state
delta_rhothree_frac=1.0/100.0 #Not being used
Lambda=1.0/4.0
#Lambda=0.0
GN=1.0 #Newton's constant
G=GN/(1.0-Lambda) #The aether's modified Newton's constant
p0=0.012 #central pressure of the star
RES=100 #Resolution of spatial grid
pressure_ratio_truncate=1e-6 #Ratio of the pressure at the radius of the neutron star to the central pressure, which is used in finding the radius of the star
radius_truncation_factor=1.5 #Slope of transition of the star to its atmosphere (in the TANH function)
RN_factor_truncate=1.0
ur_order=-1e-2 #Value of the trace of the extrinsic curvature constant everywhere in the initial conditions
#ur_order=0.0
ur_sigma=0.1
TIME=1.e-5
RESTIME=10
radius_factor=3.0 #the ratio of the distance up to which the profile is integrated, to the radius of the star
printme=0
NUM_EVOLVED_VARIABLES = 5
build_initial_profile = 0

profile_method=3
flag_plot=0
flag_trunc=1
flag_evolve=1
