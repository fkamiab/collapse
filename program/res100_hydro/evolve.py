import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math




if __name__ == "__main__":


    #r, p, Paether, m, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)
    r, p, Paether, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    NS_radius=1.33478413497
    
    ''' 
    p_inter=interpolate.interp1d(r, p, kind='cubic')
    Paether_inter=interpolate.interp1d(r, Paether, kind='cubic')
    m_inter=interpolate.interp1d(r, m, kind='cubic')
    r=np.linspace(0.0, NS_radius, gv.RES)
    p=p_inter(r)
    Paether=Paether_inter(r)
    m=m_inter(r)
    '''
     
    ur_mu=NS_radius*(1.0-0.7)
    ur_sigma=gv.ur_sigma
    ur_a=1.0/ur_sigma/np.sqrt(2.0*np.pi)
    ur_b=ur_mu
    ur_c = ur_sigma
    ur_order=gv.ur_order
    ur=-ur_order*np.ones_like(r)*(ur_a*np.exp(-(r-ur_b)*(r-ur_b)/2.0/ur_c/ur_c))*r*r/NS_radius/NS_radius

    #ur=-ur_order*np.ones_like(r)*(ur_a*np.exp(-(r-ur_b)*(r-ur_b)/2.0/ur_c/ur_c))*(np.cos(11.0*np.pi*r/2.0/NS_radius))*r*r/NS_radius/NS_radius

    
    
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    b=np.zeros_like(r)+1.0
    a=np.insert(a_1, 0, 1.0)
    i_R=np.int(NS_radius/np.diff(r)[0] )
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)

    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)


    Sr_tilde=sqrt_det*(rho(p)+p)*W*ur

    tau_tilde= sqrt_det*((rho(p)+p)*W*W-p)-sqrt_det*W*np.power(p/gv.K, 1.0/Gamma)

    def F(r_value, p_value, ur_value, a_value, b_value):
        metric_determinant=r_value*r_value*r_value*r_value*a_value*a_value*b_value*b_value*b_value*b_value
        sqrt_det=np.sqrt(metric_determinant)
        W=np.sqrt(1.0+ur_value*ur_value/a_value/a_value)
        Gamma=gv.Gamma
        def rho(p_value):
            return np.power(p_value/gv.K, 1.0/Gamma)+p_value/(Gamma-1.0)
        Sr_tilde_value=sqrt_det*(rho(p_value)+p_value)*W*ur_value
        tau_tilde_value= sqrt_det*((rho(p_value)+p_value)*W*W-p_value)-sqrt_det*W*np.power(p_value/gv.K, 1.0/Gamma)
        array_F=np.vstack((Sr_tilde_value, tau_tilde_value))
        return array_F
        


    p0=np.ones_like(r)
    ur0=np.ones_like(r)*1e-3
    YY=np.vstack((Sr_tilde, tau_tilde))
    #YY = np.ones((200, 200))
    #print XX-YY
    #plot.connect(r, XX[0])
    x0=np.vstack((p0, ur0)).ravel()
    pu=np.vstack((p, ur)).ravel()

    def FF(p,ur):
        out = F(r,p,ur,a,b)-YY
        out[0,0] = ur[0]
        out[1,0] = der.first(p,r)[0]
        return out.ravel()
    #FF=lambda p, ur : (F(r, p, ur, a, b)-YY).ravel()

    def FFF(pu):
        p_1=np.split(pu, 2)[0]
        ur_1=np.split(pu, 2)[1]
        #p_1=pu[0]
        #ur_1=pu[1]
        return FF(p_1, ur_1)

    #print FFF(pu)
    sol=optimize.root(FFF, x0)
    p_sol,ur_sol=np.split(sol.x,2)
    #ur_sol=np.split(sol.x, 2)[1]

    #solur=optimize.fsolve(FFF, x0)
    #pl.plot(r, p_sol)
    pl.plot(r, der.first(rho(p),r))
    pl.show()
