#!/usr/bin/env python

import numpy as np
import pylab as pl
import derivative_mirror as der
print der.order
RESOLUTIONS = [20,40,80]
R_MAX=4
XS = map(lambda x: np.linspace(0,R_MAX,x),RESOLUTIONS)
DXS = map(lambda x: x[1]-x[0],XS)
XS = map(lambda x: x+0.5*(x[1]-x[0]),XS)
print XS
def get_y_even(x):
    return np.exp(-x*x)

def get_d1_even(x):
    return -2*np.exp(-x*x)*x

def get_d2_even(x):
    return -2*np.exp(-x*x)+4*np.exp(-x*x)*x*x

def get_y_odd(x):
    return x*get_y_even(x)

def get_d1_odd(x):
    return np.exp(-x*x) - 2*np.exp(-x*x)*x*x

def get_d2_odd(x):
    return -6*np.exp(-x*x)*x + 4*np.exp(-x*x)*x**3

def get_d4_even(x):
    return (12*np.exp(-x*x)
            - 48*np.exp(-x*x)*x*x
            +16*np.exp(-x*x)*x**4)

def get_d4_odd(x):
    return (60*np.exp(-x*x)*x
            -80*np.exp(-x*x)*x*x*x
            +16*np.exp(-x*x)*x**5)


Y_EVENS = map(lambda x: get_y_even(x), XS)
Y_ODDS = map(lambda x: get_y_odd(x), XS)

d1_evens = [None for i in RESOLUTIONS]
d1_odds = [None for i in RESOLUTIONS]
d2_evens = [None for i in RESOLUTIONS]
d2_odds = [None for i in RESOLUTIONS]
e1_evens = [None for i in RESOLUTIONS]
e1_odds = [None for i in RESOLUTIONS]
e2_evens = [None for i in RESOLUTIONS]
e2_odds = [None for i in RESOLUTIONS]

d4_evens = [None for i in RESOLUTIONS]
d4_odds = [None for i in RESOLUTIONS]
e4_evens = [None for i in RESOLUTIONS]
e4_odds = [None for i in RESOLUTIONS]

for i in range(len(XS)):
    d1_evens[i] = der.first(Y_EVENS[i],XS[i],der.EVEN)
    #d1_evens[i][0] = 0
    d1_odds[i] = der.first(Y_ODDS[i],XS[i],der.ODD)
    #d1_odds[i] = der.set_to_zero_at_origin(d1_odds[i],XS[i])
    d2_odds[i] = der.second(Y_ODDS[i],XS[i],der.ODD)
    #d2_odds[i][0] = 0
    d2_evens[i] = der.second(Y_EVENS[i],XS[i],der.EVEN)
    #d2_evens[i] = der.set_to_zero_at_origin(d2_evens[i],XS[i])
    d4_evens[i] = der.second(der.second(Y_EVENS[i],XS[i],der.EVEN),XS[i],der.EVEN)
    d4_odds[i] = der.second(der.second(Y_ODDS[i],XS[i],der.ODD),XS[i],der.ODD)
    e1_evens[i] = (get_d1_even(XS[i]) - d1_evens[i])*RESOLUTIONS[i]**der.order
    e2_evens[i] = (get_d2_even(XS[i]) - d2_evens[i])*RESOLUTIONS[i]**der.order
    e1_odds[i] = (get_d1_odd(XS[i]) - d1_odds[i])*RESOLUTIONS[i]**der.order
    e2_odds[i] = (get_d2_odd(XS[i]) - d2_odds[i])*RESOLUTIONS[i]**der.order
    e4_evens[i] = (get_d4_even(XS[i]) - d4_evens[i])*RESOLUTIONS[i]**der.order
    e4_odds[i] = (get_d4_odd(XS[i]) - d4_odds[i])*RESOLUTIONS[i]**der.order

# lines = [pl.plot(XS[i],d2_evens[i]) for i in range(len(XS))]
# pl.show()
lines = [pl.plot(XS[i],e1_evens[i]) for i in range(len(XS))]
pl.title('d1 even')
pl.show()

lines = [pl.plot(XS[i],e2_evens[i]) for i in range(len(XS))]
pl.title('d2 even')
pl.show()

lines = [pl.plot(XS[i],e1_odds[i]) for i in range(len(XS))]
pl.title('d1 odd')
pl.show()

lines = [pl.plot(XS[i],e2_odds[i]) for i in range(len(XS))]
pl.title('d2 odd')
pl.show()

lines = [pl.plot(XS[i],e4_evens[i]) for i in range(len(XS))]
pl.title('d4 even')
pl.show()

lines = [pl.plot(XS[i],e4_odds[i]) for i in range(len(XS))]
pl.title('d4 odd')
pl.show()
