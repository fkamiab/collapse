import numpy as np
import pylab as pl
import matplotlib as mpl
import plot as plot


if __name__ == "__main__":

    curve_number=7
    output_res=10
    final_time=1e-05

    r=np.empty([output_res+1, 100])
    p=np.empty_like(r)
    Paether=np.empty_like(r)
    ur=np.empty_like(r)
    alpha=np.empty_like(r)
    a=np.empty_like(r)
    b=np.empty_like(r)
    K=np.empty_like(r)
    Sr_tilde=np.empty_like(r)
    tau_tilde=np.empty_like(r)

    mpl.rcParams.update({'font.size':15})
    for x in range(0, curve_number):
        i=np.int(x*output_res/((curve_number-1)+1e-10))
        time=final_time*i/(output_res-1)
        filename="time_step_{}.out".format(i)
        r[i], p[i], Paether[i], ur[i], alpha[i], a[i], b[i], K[i], Sr_tilde[i], tau_tilde[i] =np.loadtxt(filename, skiprows=1, unpack=True)
        #pl.plot(r[i], np.log10(p[i])-np.log10(p[0]), lw=3, label=r'$t={}$'.format(time))
        pl.plot(r[i], K[i], lw=3, label=r'$t={}$'.format(time))
        #pl.plot(r[i], np.log10(p[i])-np.log10(p[0]))

    pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    pl.xlabel(r'$r$', fontsize=20)
    #pl.ylabel(r'$\log(p)_{(t)}-\log(p)_{(t=0)}$', fontsize=20)
    pl.ylabel(r'$K$', fontsize=20)
    pl.savefig('K.pdf', bbox_inches='tight')
    #print i, K[i]
    #pl.show()

   

    '''
    x=np.zeros_like(r)
    mpl.rcParams.update({'font.size':19})
    pl.plot(r, Paether0, lw=3, label=r'$\mathcal{P}_{aether}$')
    pl.plot(r, p0, lw=3, label=r'$p_{matter}$')
    pl.plot(r, x, linestyle='--', lw=3)
    #pl.yscale('log')
    pl.legend(loc=0, fontsize=24)
    pl.xlabel(r'$r$', fontsize=22)
    pl.ylabel(r'Pressure', fontsize=19)
    pl.savefig('p_aether_matter.pdf', bbox_inches='tight')
    #pl.show()
    quit()'''
 
    

  
 
