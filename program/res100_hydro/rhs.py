#!/usr/bin/env python

"""
rhs.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-17 17:29:16 (jmiller)>

This file defines the right-hand-side of the pde system
(d/dt) U = f(t,U)

More precisely, the time evolution of the system U is is given by
(d/dt) U = - (d/dr) FLUX(t,U) + M(t,U)

we caluclate everything on the right-hand-side here.

U = [D_tilde,Sr_tilde,tau_tilde,a,b,K]

where a and b are metric components and
where K is the trace of the extrinsic curvature.
"""

import pylab as pl
import numpy as np
import global_variables as gv
import derivative_mirror as der
import derivative_4_3 as der3
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
import Paether as PaetherFinder
import plot as plot
import Hamiltonian_Constraint as Hamiltonian
NUM_VARIABLES = gv.NUM_EVOLVED_VARIABLES
CONVERSION_MESSAGE="conserved -> primitive failed!"

def flux(r,rho,p,ur,Sr_tilde,tau_tilde,a,b,K,alpha):
    """
    Calculates the flux term for the conserved quantities.
    Defined as FLUX(t,U) above.

    Returns 6 arrays, one for each variable in U. However, the flux
    for a and b is trivial.
    """
    gamma_half = geometry.get_gamma_half(r,a,b)
    T_ru_rd = tmunu.get_T_ru_rd(rho,p,ur,alpha,a)
    #T_ru_rd = der.set_to_zero_at_origin(T_ru_rd, r)
    T_tu_ru = tmunu.get_T_tu_ru(rho,p,ur,alpha,a)
    #T_tu_ru[0] = 0
    vr = tmunu.get_vr(ur,alpha,a)
    W=tmunu.get_w(ur, a)
    
    # fSr moved to source because part of
    # the gradient is divergent and that part
    # cancels with a source term.
    fSr = np.zeros_like(r) #alpha*gamma_half*T_ru_rd
    ftau = (alpha**2)*gamma_half*T_tu_ru - gamma_half*W*tmunu.get_rho0(p)*vr
    fa = np.zeros_like(r)
    fb = np.zeros_like(r)
    fk = np.zeros_like(r)

   
    return np.vstack((fSr,ftau,fa,fb,fk))

def source(r,rho,p,ur,Sr_tilde,tau_tilde,a,b,
           alpha,Krr,Ktt, Paether):
    """
    Calculates the source terms, M(t,U) as described above.

    Returns 6 arrays, one for each variable (other than r and alpha)
    given as input. For now, a, b, and K don't evolve, so their source
    terms are trivial.
    
    For now, since gravity is static, all K can be assumed to be zero.
    """
    # TODO: Is tdef flux(r,rho,p,ur,D_tilde,Sr_tilde,tau_tilde,a,b,K,alpha):
    """
    Calculates the flux term for the conserved quantities.
    Defined as FLUX(t,U) above.

    Returns 6 arrays, one for each variable in U. However, the flux
    for a and b is trivial.
    """
    K=Krr+2.0*Ktt
    
    # TODO: Is this right?
    TthetathetaupTIMESrquared=p/b/b 
    gamma_half = geometry.get_gamma_half(r,a,b)
    T_tu_tu = tmunu.get_T_tu_tu(rho,p,ur,alpha,a)
    T_ru_ru = tmunu.get_T_ru_ru(rho,p,ur,alpha,a)
    T_ru_rd = tmunu.get_T_ru_rd(rho,p,ur,alpha,a)
    T_ru_rd_0 = tmunu.get_T_ru_rd_0(rho,p,ur,alpha,a)

    # m values
    
    # (d/dr) flux(Sr), calculated here for regularization
    mfSr = (alpha*gamma_half*der.first(T_ru_rd,r, der.EVEN)
            + alpha*a*der.first(b*b*r*r,r, der.EVEN)*T_ru_rd_0
            + der.first(alpha,r, der.EVEN)*T_ru_rd*gamma_half)
    # source term for Sr
    mSr = alpha*(0.5*gamma_half*(T_tu_tu*der.first(-alpha*alpha,r, der.EVEN)
                                 + T_ru_ru*der.first(a*a,r, der.EVEN)))
                 # a dirty hack. We set the divergent term in the
                 # source to zero. It cancels witha  flux term.
                 # + a*p*(b*b*2.0*r + 2.0*b*der.first(b,r)*r*r))
    
    mSr -= mfSr
    mtau = alpha*gamma_half*(T_ru_ru*a*a*Krr
                             + 2.0*b*b*Ktt*TthetathetaupTIMESrquared)
    # Metric variables
    ma=-alpha*a*Krr
    mb=-alpha*b*Ktt
    W=tmunu.get_w(ur, a)
    rhothree=rho*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)

    #plot.connect(r, ma)
    S_K=(rho+p)*ur*ur/a/a +3.0*p -3.0*gv.Lambda*(3.0*p-rho) +3.0*Paether
    C_K= Krr*Krr+2.0*Ktt*Ktt+4.0*np.pi*gv.G*(S_K+rhothree)
    #mK=-der.first(gamma_half*der.first(alpha,r, der.EVEN)/a/a, r, der.ODD) +alpha*gamma_half*(C_Kdens-K*K)
    #mK=-(der.first(gamma_half/a/a,r, der.EVEN)*der.first(alpha,r, der.EVEN) + (gamma_half/a/a)*der.second(alpha,r, der.EVEN)) +alpha*gamma_half*(C_Kdens-K*K)

    #plot.connect(r, mSr)
    mK=np.empty_like(ma)
    #mK[1:]=(-1.0/a[1:]/a[1:])*(der.second(alpha,r, der.EVEN)[1:] +2.0*der.first(alpha,r, der.EVEN)[1:]/r[1:] - der.first(alpha,r, der.EVEN)[1:]*der.first(a,r, der.EVEN)[1:]/a[1:] +2.0*der.first(alpha,r, der.EVEN)[1:]*der.first(b,r, der.EVEN)[1:]/b[1:])+alpha[1:]*C_K[1:]
    #mK[0]=(-1.0/a[0]/a[0])*(3.0*der.second(alpha,r, der.EVEN)[0] - der.first(alpha,r, der.EVEN)[0]*der.first(a,r, der.EVEN)[0]/a[0] +2.0*der.first(alpha,r, der.EVEN)[0]*der.first(b,r, der.EVEN)[0]/b[0])+alpha[0]*C_K[0]

    #mK=(-1.0/a/a)*(der.first(der.first(alpha,r, der.EVEN), r, der.ODD) +2.0*der.first(alpha,r, der.EVEN)/r - der.first(alpha,r, der.EVEN)*der.first(a,r, der.EVEN)/a +2.0*der.first(alpha,r, der.EVEN)*der.first(b,r, der.EVEN)/b)+alpha*C_K
    '''
    pl.plot(r, -der.first(der.first(alpha,r, der.EVEN), r, der.ODD) )
    pl.plot(r,-2.0*der.first(alpha,r, der.EVEN)/r )
    pl.plot(r, + der.first(alpha,r, der.EVEN)*der.first(a,r, der.EVEN)/a)
    pl.plot(r, -2.0*der.first(alpha,r, der.EVEN)*der.first(b,r, der.EVEN)/b)
    pl.plot(r, alpha*C_K)
    pl.plot(r, mK, 'ro')
    pl.show()'''

    drr=r[3]-r[2]
    alphaprime=der.first(alpha, r, der.EVEN)
    alprime=np.ones_like(alpha)*alphaprime
    alprime[:-1]=alphaprime[1:]
    term0=3.0*((r+drr)*(r+drr)*alprime -r*r*alphaprime   )/((r+drr)**3 -r**3)   
    term1= der.first(der.first(alpha,r, der.EVEN), r, der.ODD)
    term2=2.0*der.first(alpha,r, der.EVEN)/r


    mK=(-1.0/a/a)*(der.first(der.first(alpha,r, der.EVEN), r, der.ODD) +2.0*der.first(alpha,r, der.EVEN)/r - der.first(alpha,r, der.EVEN)*der.first(a,r, der.EVEN)/a +2.0*der.first(alpha,r, der.EVEN)*der.first(b,r, der.EVEN)/b)+alpha*C_K
    #mK= (-1.0/a/a)*(term0 - der.first(alpha,r, der.EVEN)*der.first(a,r, der.EVEN)/a +2.0*der.first(alpha,r, der.EVEN)*der.first(b,r, der.EVEN)/b)+alpha*C_K

    '''rsquaredalphaprime=r*r*alphaprime
    rralphaprime=np.ones_like(alpha)*rsquaredalphaprime
    rralphaprime[:-1]=rsquaredalphaprime[1:]
    print '{}\t{}\n'.format(rralphaprime[10], rsquaredalphaprime[10])
    term0=3.0*(rralphaprime -rsquaredalphaprime  )/((r+drr)**3 -r**3) '''
    #print term0

    '''
    drr=r[3]-r[2]
    aprime=der.first(a, r, der.EVEN)
    alprime=aprime
    alprime[:-1]=aprime[1:]
    term0=3.0*((r+drr)*(r+drr)*alprime -r*r*aprime   )/((r+drr)**3 -r**3)   
    term1= der.first(der.first(a,r, der.EVEN), r, der.ODD)
    term2=2.0*der.first(a,r, der.EVEN)/r'''


    #pl.plot(r, term0)
    #pl.plot(r, term1+term2)
    #pl.show()
    #quit()
    term3=der.first(alpha,r, der.EVEN)*der.first(a,r, der.EVEN)/a 
    term4=2.0*der.first(alpha,r, der.EVEN)*der.first(b,r, der.EVEN)/b
    term5=alpha*C_K*(-a*a)
    #pl.plot(r, term1)
    #pl.plot(r, term2)
    #pl.plot(r, term3)
    #pl.plot(r, term4)
    #pl.show()
    #quit()

    '''
    pl.plot(r, der.second(term1,r, der.EVEN))
    pl.plot(r, der.second(term2,r, der.EVEN))
    pl.plot(r, der.second(term3,r, der.EVEN))
    pl.plot(r, der.second(term4,r, der.EVEN))
    pl.plot(r, der.second(-alpha*C_K*a*a,r, der.EVEN))
    pl.plot(r, der.second(mK,r, der.EVEN) , 'ro')
    pl.show()
    quit()'''


    #plot.connectpoints(r, der.second(der.second(Paether, r, der.EVEN), r, der.EVEN))

    #plot.connect(r, der.first(alpha,r, der.EVEN))
    #term1=-der.first(gamma_half/a/a,r, der.EVEN)*der.first(alpha,r, der.EVEN)
    #term2=(gamma_half/a/a)*der.second(alpha,r, der.EVEN)
    #term3=alpha*gamma_half*(C_Kdens-K*K)
    #pl.plot(r[1:], der.first(term1[1:]/gamma_half[1:],r, der.EVEN))
    #pl.plot(r[1:], der.first(term1[1:]/gamma_half[1:],r, der.EVEN), 'ro')
    #pl.plot(r[1:], der.first(term2[1:]/gamma_half[1:], r, der.EVEN))
    #pl.plot(r[1:], der.first(term3[1:]/gamma_half[1:], r, der.EVEN))
    #pl.plot(r[1:], der.first(mK[1:]/gamma_half[1:], r, der.EVEN))
    #pl.show()
    #quit()
    

    return np.vstack((mSr,mtau,ma,mb,mK))



def rhs(t,r,U):
    """
    The right-hand-side operator:
    M(t,U) - (d/dr)FLUX(t,U)
    """
    if gv.printme==0:
        print "\t t = {}".format(t)
    U_2d = utilities.unflatten(U,NUM_VARIABLES)
    Sr_tilde = U_2d[0]
    tau_tilde = U_2d[1]
    a = U_2d[2]
    b = U_2d[3]
    K = U_2d[4]
    p, ur, a ,b=variables.primitive_variables(r, Sr_tilde, tau_tilde, a, b)
    rho = tmunu.get_rho(p)
    
    #plot.connect(r, ur)
    #plot.connect(r, K)
    #plot.connect(r, der.first(K,r, der.EVEN))
    
    
    # For now, since gravity is static, all K can be assumed to be zero.
    gamma_half=geometry.get_gamma_half(r, a, b)
    
    #plot.connectpoints(r, K)
    Ktt, Krr = K_solver.giveK(r, p, K, ur, a, b)

    Paether=PaetherFinder.get_Paether(r, p, ur, Ktt, Krr, a, b)
    #plot.connect(r, Paether)

    alpha=alphafinder.give_alpha_direct(r, p, Paether)
    #plot.connect(r, alpha)
    #plot.connect(r, ur)
    #plot.connect(r, K)

##########
    '''    sqrt_det=geometry.get_gamma_half(r,a,b)
    K=Krr+2.0*Ktt
    urup=ur/a/a
    W=tmunu.get_w(ur, a)
    rho=tmunu.get_rho(p)
    dpdrho=tmunu.get_dpdrho(p)
    sump=rho+p

    
    A0=W
    B0=sump*ur/W/a/a
    Q0=sump*W/a/a
    L0=W*(gv.Gamma-1.0)*ur/a/a
    C0=np.zeros_like(r)
    #ratio_C = np.empty_like(r)
    #ratio_C[1:] = urup[1:]/r[1:]
    
    #ratio_C[0]=der.first(urup, r, der.ODD)[0]
    #ratio_C=der.set_to_zero_at_origin(ratio_C,r)

    ratio_C=urup/r
    
    
    
    C0= sump*der.first(urup,r, der.ODD) +sump*urup*(2.0*der.first(b,r, der.EVEN)/b +der.first(a,r, der.EVEN)/a) + sump*2.0*ratio_C +der.first(rho, r, der.EVEN)*urup +  (sump)*(-W*Krr -2.0*W*Ktt + Krr*ur*ur/a/a/W)
    S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r, der.EVEN)*urup*urup/a +der.first(urup, r, der.ODD)*urup)+der.first(p,r, der.EVEN)*urup*urup +der.first(p,r, der.EVEN)/a/a
    Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
    Omega=Omega0*(3.0*dpdrho-1.0)
    f2= sump*(3.0*dpdrho-1.0)
    AAA=3.0*f2*ur[0]/a/a/r[0]
    
    pl.plot(r, -(-AAA+f2*(alpha[0]*Omega[0]-K[0]))/(f2*alpha-1)**2)
    #pl.plot(r, f2*(alpha))
    #print alpha[0]
    pl.show()
    #quit()'''


############



    #plot.connect(r, alpha)
    

    #print t
    
    #plot.connect(r, alpha)
    #plot.connect(r, K)
    
    '''x=Hamiltonian.giveHamiltonian(r, a, b, p, ur, Ktt, Krr)
    quit()'''

    # get primitives
    #try:
    
    #except: # placeholder for better error handling.
    #    utilities.simple_error_message(CONVERSION_MESSAGE,
    #                                   D_tilde,Sr_tilde,tau_tilde)

    # Get flux
    fU_2D = flux(r,rho,p,ur,Sr_tilde,tau_tilde,a,b,K,alpha)
    # Get source terms
    mU_2D = source(r,rho,p,ur,Sr_tilde,tau_tilde,a,b,
                   alpha,Krr,Ktt, Paether)
    # Get 2D rhs
    rhs_2D = np.empty_like(fU_2D)
    # Sr_tilde has vanishing flux
    rhs_2D[0] = mU_2D[0] #- der.first(fU_2D[0],r,der.EVEN)
    # tau tilde has odd flux
    rhs_2D[1] = mU_2D[1] - der.first(fU_2D[1],r,der.ODD)
    # a,b,K all have vanishing flux
    rhs_2D[2] = mU_2D[2]
    rhs_2D[3] = mU_2D[3]
    rhs_2D[4] = mU_2D[4]



    rhs_2D[0][len(r)-1]=0.0 
    rhs_2D[1][len(r)-1]=0.0 
    rhs_2D[2][len(r)-1]=0.0 
    rhs_2D[3][len(r)-1]=0.0 
    rhs_2D[4][len(r)-1]=0.0 


    # Sr,tau,K densitized
    #rhs_2D[0][0] = 0
    #rhs_2D[1][0] = 0
    #rhs_2D[-1][0] = 0

    Sr_tilde_dot=rhs_2D[0]
    tau_tilde_dot=rhs_2D[1]
    a_dot=rhs_2D[2]
    b_dot=rhs_2D[3]
    K_dot=rhs_2D[4]

    #plot.connect(r[10:],der.first(K,r, der.EVEN)[10:])
    #plot.connect(r, K_dot)
        
    rhs_1D = utilities.flatten(rhs_2D)

    return rhs_1D



def make_rhs(r):
    """
    Given a radius grid, make the right-hand-side function, which is a closure.
    """
    return lambda t,U: rhs(t,r,U)

