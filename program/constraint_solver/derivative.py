import pylab as pl
import numpy as np
import plot



def first(f, r):
    h=np.diff(r)[1]
    der_f=np.zeros(len(r))   
    Q=f[2:]-f[:-2]
    Q=np.lib.pad(Q, (1,1), 'edge')
    W=f[4:]-f[:-4]
    W=np.lib.pad(W, (2,2), 'edge')
    der_f=(1.0/h)*((2.0/3.0)*Q - (1.0/12.0)*W)
    
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    der_f[0]=(1.0/h)*(q00*f[0]+q10*f[1]+q20*f[2]+q30*f[3]+q40*f[4]+q50*f[5])
    der_f[len(r)-1]=(1.0/h)*(-q00*f[len(r)-1]-q10*f[len(r)-2]-q20*f[len(r)-3]-q30*f[len(r)-4]-q40*f[len(r)-5]-q50*f[len(r)-6])

    q01 = -1.0/2.0
    q11 = 0.0
    q21 = 1.0/2.0
    q31 = 0.0
    q41 = 0.0
    q51 = 0.0
    der_f[1]=(1.0/h)*(q01*f[0]+q11*f[1]+q21*f[2]+q31*f[3]+q41*f[4]+q51*f[5])
    der_f[len(r)-2]=(1.0/h)*(-q01*f[len(r)-1]-q11*f[len(r)-2]-q21*f[len(r)-3]-q31*f[len(r)-4]-q41*f[len(r)-5]-q51*f[len(r)-6])


    q02 = 4.0/43.0
    q12 = -59.0/86.0
    q22 = 0.0
    q32 = 59.0/86.0
    q42 = -4.0/43.0
    q52 = 0.0
    der_f[2]=(1.0/h)*(q02*f[0]+q12*f[1]+q22*f[2]+q32*f[3]+q42*f[4]+q52*f[5])
    der_f[len(r)-3]=(1.0/h)*(-q02*f[len(r)-1]-q12*f[len(r)-2]-q22*f[len(r)-3]-q32*f[len(r)-4]-q42*f[len(r)-5]-q52*f[len(r)-6])

    q03 = 3.0/98.0
    q13 = 0.0
    q23 = -59.0/98.0
    q33 = 0.0
    q43 = 32.0/49.0
    q53 = -4.0/49.0
    der_f[3]=(1.0/h)*(q03*f[0]+q13*f[1]+q23*f[2]+q33*f[3]+q43*f[4]+q53*f[5])
    der_f[len(r)-4]=(1.0/h)*(-q03*f[len(r)-1]-q13*f[len(r)-2]-q23*f[len(r)-3]-q33*f[len(r)-4]-q43*f[len(r)-5]-q53*f[len(r)-6])


    return der_f


def second(f, r):
    der_f=first(f,r)
    return first(der_f,r)
    



if __name__ == "__main__":
    RES=10
    r=np.linspace(-10, 10, RES)
    def f(r):
        return r*r
    def g(r):
        return 2.0*r
    def k(r):
        return 2.0*np.ones_like(r)

    pl.plot(r, first(f(r),r))
    pl.plot(r, g(r), 'bo')
    pl.show()
    '''print der(f,r) - g(r)
    pl.semilogy(r, first(f,r) - g(r))
    pl.show()'''



