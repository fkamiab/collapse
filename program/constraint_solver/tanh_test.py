import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
import math




if __name__ == "__main__":
    
    r=np.linspace(0, 6, 100)
    RN=1.2
    plot.connect(r, 0.5+0.5*np.tanh(100.0*(RN-r)))
