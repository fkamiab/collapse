import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
import global_variables_initial_profile as gv

SKIP_ROWS=30


r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)

r2, p2, ptilde2, m2 = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25_test.out', skiprows=1, unpack=True)


def rho(p):
    return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
def rhotilde(p):
    return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

pl.plot(r,np.log10(ptilde))
pl.plot(r2, np.log10(ptilde2))
#pl.plot(r,ptilde)
#pl.plot(r2, ptilde2)
#pl.plot(r, ptilde)
pl.show()

#plot.connect(r,m)
