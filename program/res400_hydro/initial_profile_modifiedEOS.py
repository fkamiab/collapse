import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
from scipy.optimize import bisect
from scipy.interpolate import UnivariateSpline
from scipy.integrate import ode
import global_variables as gv
import derivative as der
import energy_momentum as tmunu
from scipy import interpolate


i, lp_EOS, lrho_EOS, lrho0_EOS = np.loadtxt('EOS_RES{}_eps{}.out'.format(gv.EOS_RES, gv.epsilon), skiprows=1, unpack=True)
lrho_inter= interpolate.interp1d(lp_EOS, lrho_EOS , kind=5) 
    
def rho(p):
    lp=np.log10(p)
    lrho=lrho_inter(lp)
    rho=10.0**lrho
    return rho



def create_file(filename):
    "Creates a new file with filename"
    with open(filename,'w') as f:
        f.write('# pcentral\tM\tR\n')

def save_row(p, R,M,filename):
    "Appends R and M to a file with filename"
    with open(filename,"a") as f:
        f.write("{} {} {}\n".format(p, R,M))


def PTILDE(p0, ptilde0, RN):    
 
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[1]








def pressure(p0, ptilde0, RN):    

    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[0]







def mass(p0, ptilde0, RN):    

    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[2]





def profile(p0, ptilde0, RN, radius_factor, RES):    
 
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= (-(p+rho(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN-r)))
            print 'r={}\t\tp={}'.format(r,p)
            fptilde=(-(ptilde+rhotilde(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN-r)))
            fm=4.0*np.pi*r*r*rhotilde(p)
   
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    state_int.set_integrator('dop853',max_step=1e-4)
    state_int.set_initial_value(state0, r0)
    r_array=np.linspace(0.0, radius_factor*RN, RES)
    state_soln = np.empty((len(r_array),len(state0)))
    state_soln[0] = state0
    for i,r in enumerate(r_array[1:]):
        if not state_int.successful():
            raise ValueError("Integrator failed.")
        state_int.integrate(r)
        state_soln[i+1] = state_int.y
    return state_soln


def RNcorrect(p0):
    RN=6.0
    pressure_ratio_truncate=gv.pressure_ratio_truncate
    ptilde0_correct=bisect(lambda ptilde0: PTILDE(p0,ptilde0, RN), p0, p0+p0/6)
    Radius_correct=bisect(lambda r: pressure(p0,ptilde0_correct, r)-pressure_ratio_truncate*p0, 1, RN)
    return Radius_correct
    

def give_initial_profile(p0, radius_factor, RES):
    RN=6.0
    Radius_correct=RNcorrect(p0)
    ptilde0_correct=bisect(lambda ptilde0: PTILDE(p0,ptilde0, RN), p0, p0+p0/6)
    Mass_correct=mass(p0,ptilde0_correct, Radius_correct)
    state_soln=profile(p0, ptilde0_correct, Radius_correct, radius_factor, RES)
    r=np.linspace(0.0, gv.radius_factor*Radius_correct, gv.RES)
    p=state_soln[..., 0]
    ptilde=state_soln[..., 1]
    m=state_soln[..., 2]
    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
    Paether=(1.0-gv.Lambda)*ptilde-(1.0-3.0*gv.Lambda)*p-gv.Lambda*rho(p)
    filename="profile_centralpressure{}_RES{}_Lambda{}.out".format(p0, RES, gv.Lambda)
    print "Printing {} \n".format(filename)
    np.savetxt(filename, np.c_[r, p, Paether, m], delimiter='\t', header='Mass={} and Radius={} and Columns are \tr\tp\tPaether\tm'.format(state_soln[len(r)-1, 2], Radius_correct))     
    return r, p, Paether, m, Radius_correct
     

   









    
if __name__ == "__main__":

    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    p0=gv.p0
    ptilde0=gv.p0


    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            print 'r={}\tlp={}\n'.format(r, np.log10(p))
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= (-(p+rho(p))*tov/r/r)
            fptilde=(-(ptilde+rhotilde(p))*tov/r/r)
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dop853',max_step=1e-4)
    state_int.set_initial_value(state0, r0)
    r_array=np.linspace(0.0, 10, 100)
    state_soln = np.empty((len(r_array),len(state0)))
    state_soln[0] = state0
    for i,r in enumerate(r_array[1:]):
        if not state_int.successful():
            raise ValueError("Integrator failed.")
        state_int.integrate(r)
        state_soln[i+1] = state_int.y
    p=state_soln[..., 0]
    ptilde=state_soln[..., 1]
    m=state_soln[..., 2]
 
    pl.plot(r_array, np.log10(p))
    pl.plot(r_array, np.log10(np.abs(ptilde)), 'ro')
    pl.show()
