#!/usr/bin/env python

"""evolve.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 22:30:47 (jmiller)>

This file is the main evolution loop for the system with hydro
variables only.
"""

import numpy as np
import global_variables as gv
#import derivative as der
import derivative_mirror as der
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import rhs
from scipy.integrate import ode
import pylab as pl
import initial_velocity as velocity
import plot as plot
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
import Paether as PaetherFinder
import initial_K as initial_K
from scipy import interpolate
import initial_profile as initial
from scipy import optimize



def give_rho0_min():
    def residual(rho0_min):
        res=(gv.K/(gv.Gamma-1.0))*np.abs(rho0_min)**(gv.Gamma-1.0) + gv.epsilon*np.log(np.abs(rho0_min))-1.0
        return res
    rho0_min_trial=0.01
    sol = optimize.root(lambda rho0_min: residual(rho0_min), rho0_min_trial)
    rho0_min_sol=sol.x
    
    return np.abs(rho0_min_sol)

rho0_min_sol=give_rho0_min()

    
def rho_EOS_from_rho0(rho0):
    energy_density= rho0*(1.0 + (gv.K/(gv.Gamma-1.0))*rho0**(gv.Gamma-1.0) - (gv.K/(gv.Gamma-1.0))*rho0_min_sol**(gv.Gamma-1.0) + gv.epsilon*np.log(rho0) - gv.epsilon*np.log(rho0_min_sol))
    return energy_density


def rho0_EOS_from_rho(rho):
    def residual_local(rho0, rho):
        res=rho_EOS_from_rho0(np.abs(rho0))-rho
        return res
    rho0_EOS_array_trial=np.ones_like(rho)
    sol = optimize.root(lambda rho0: residual_local(rho0, rho), rho0_EOS_array_trial)
    return np.abs(sol.x)
    


def pressure_EOS_from_rho0(rho0):
    return gv.K*rho0**(gv.Gamma) +gv.epsilon*rho0


def pressure_EOS_from_rho(rho):
    rho0=rho0_EOS_from_rho(rho)
    return pressure_EOS_from_rho0(rho0)
    
    

def rho0_EOS_from_pressure(p):
    def residual_local(rho0, p):
        res=pressure_EOS_from_rho0(np.abs(rho0))-p
        return res
    rho0_EOS_array_trial=np.ones_like(p)
    sol = optimize.root(lambda rho0: residual_local(rho0, p), rho0_EOS_array_trial)
    return np.abs(sol.x)
    


def rho_EOS_from_pressure(p):
    def residual_local(rho, p):
        res=pressure_EOS_from_rho(np.abs(rho))-p
        return res
    rho_EOS_array_trial=np.ones_like(p)
    sol = optimize.root(lambda rho: residual_local(rho, p), rho_EOS_array_trial)
    return np.abs(sol.x)




if __name__ == "__main__":


  
    '''SETTING INITIAL PRESSURE PROFILE FOR THE STAR'''
    #r, p0, Paether0, m0, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)    
    r, p0, Paether0, m0 = np.loadtxt('profile_centralpressure0.012_RES{}_Lambda0.25.out'.format(gv.RES), skiprows=1, unpack=True)
    NS_radius=1.33478413497
    ###################################################



    lp_min=gv.EOS_lp_min
    lp_max=gv.EOS_lp_max
    lp_res= gv.EOS_RES
    a_lp=(lp_max-lp_min)/(lp_res-1.0)

    
    pressure_array=np.logspace(lp_min, lp_max, lp_res)
    rho_array=rho_EOS_from_pressure(pressure_array)
    rho0_array=rho0_EOS_from_pressure(pressure_array)
    indice_array= ((np.log10(pressure_array)-lp_min)/a_lp)
    indice_array=np.rint(indice_array)

    filename="EOS_RES{}_eps{}.out".format(lp_res, gv.epsilon)
    print "Printing {} \n".format(filename)
    np.savetxt(filename, np.c_[indice_array, np.log10(pressure_array), np.log10(rho_array), np.log10(rho0_array)], delimiter='\t', header='RES={}. Indice is equal to int((log10p-lp_min)/a) where a = (lp_max-lp_min)/(RES-1.0) where lp_min={} and lp_max={}. Columns are indice, log10p, log10rho, log10rho0'.format(lp_res,lp_min, lp_max))     


    '''
    print np.log10(rho_array)
    #plot.connectpoints(np.log10(pressure_array), np.log10(pressure_array))



    p_test=2.5e-4
    lp_test=np.log10(p_test)
    i=int((lp_test-lp_min)/a_lp)
    print i'''
