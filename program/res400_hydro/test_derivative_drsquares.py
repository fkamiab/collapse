import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative_mirror as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math
import initial_velocity as vel
import initial_K as Kfirst




if __name__ == "__main__":


    #r, p, Paether, m, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)
    r, p, Paether, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    NS_radius=1.33478413497
    
    '''
    p_inter=interpolate.interp1d(r, p, kind='cubic')
    Paether_inter=interpolate.interp1d(r, Paether, kind='cubic')
    m_inter=interpolate.interp1d(r, m, kind='cubic')
    r=np.linspace(0.0, NS_radius, gv.RES)
    p=p_inter(r)
    Paether=Paether_inter(r)
    m=m_inter(r)'''
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    b=np.zeros_like(r)+1.0
    a=np.insert(a_1, 0, 1.0)


    RMAX=1.4
    RES=200
    x=np.linspace(RMAX/(2.0*gv.RES-1.0), RMAX, RES)
    
    
    A= np.cos(10.*x)
    B=-10.*np.sin(10.*x)/x
    pl.plot(x, der.first(A, x, der.EVEN)/x, 'bo')
    #pl.plot(x, 2.0*der.first(A, x*x, der.EVEN), 'go')
    pl.plot(x, 2.0*der.first(A, x, der.EVEN)/der.first(x*x, x, der.EVEN), 'go')
    pl.plot(x, B)
    pl.show()

