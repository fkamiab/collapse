import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative_mirror as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math
import initial_velocity as vel
import initial_K as Kfirst


def giveK(r, p, K, ur, a, b):
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    D=-4.0*np.pi*gv.G*(rho(p)+p)*ur
    def residual(Ktt, r, D, K):
        res=np.empty_like(r)
        res=r*der.first(Ktt,r, der.EVEN)-D*r+(3.0*Ktt-K)
        #res[0]=der.first(Ktt,r, der.EVEN)[0]
        return res
    Ktt0=np.ones_like(r)
    sol = optimize.root(lambda Ktt: residual(Ktt, r, D, K), Ktt0)
    Ktt=sol.x
    Krr=K-2.0*Ktt
    return Ktt, Krr





def giveK_other(r, p, ur, a, b):
    aprime=der.first(a,r)
    bprime=der.first(b,r)
    bdoubleprime=der.second(b,r)
    R_1= -(2.0/(r[1:]*r[1:]*a[1:]*a[1:]*a[1:]*b[1:]*b[1:]))*(-2.0*r[1:]*b[1:]*aprime[1:]*(r[1:]*bprime[1:]+b[1:])+a[1:]*(r[1:]*r[1:]*bprime[1:]*bprime[1:] +2.0*r[1:]*b[1:]*(r[1:]*bdoubleprime[1:] +3.0*bprime[1:]) +b[1:]*b[1:]) -a[1:]*a[1:]*a[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*R_1[0]-q20*R_1[1]-q30*R_1[2]-q40*R_1[3]-q50*R_1[4])/q00
    R=np.insert(R_1, 0, first_element)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    #R=ohr
    def residual(Ktt_sol, r, b, bprime, Sr, rhothree, R):
        res=np.empty_like(r)
        res=4.0*b*r*Ktt_sol*der.first(Ktt_sol, r)+16.0*np.pi*gv.G*b*r*Ktt_sol*Sr+(b+r*bprime)*(6.0*Ktt_sol*Ktt_sol-16.0*np.pi*gv.G*rhothree+R)
        #res[0]=Ktt_sol[0]-np.sqrt((16.0*np.pi*gv.G*rhothree[0]-R[0])/6.0)
        res[0]=der.first(Ktt_sol,r)[0]
        #res[0]=Ktt_sol[0]
        return res
    Ktt0=np.ones_like(r)*0.0001
    sol = optimize.root(lambda Ktt_sol: residual(Ktt_sol, r, b, bprime, Sr, rhothree, R), Ktt0)
    Ktt=sol.x


    KrrKtt=16.0*np.pi*gv.G*rhothree-R+2.0*Ktt*Ktt
    Krr=KrrKtt/(Ktt+1e-10)
    return Ktt, Krr

if __name__ == "__main__":


    #r, p, Paether, m, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)
    r, p, Paether, m = np.loadtxt('profile_centralpressure0.012_RES400_Lambda0.25.out', skiprows=1, unpack=True)
    NS_radius=1.33478413497
    
    '''
    p_inter=interpolate.interp1d(r, p, kind='cubic')
    Paether_inter=interpolate.interp1d(r, Paether, kind='cubic')
    m_inter=interpolate.interp1d(r, m, kind='cubic')
    r=np.linspace(0.0, NS_radius, gv.RES)
    p=p_inter(r)
    Paether=Paether_inter(r)
    m=m_inter(r)'''
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    b=np.zeros_like(r)+1.0
    a=np.insert(a_1, 0, 1.0)
    ur=vel.velocity(r, NS_radius)
 
    
    Ktt, Krr= giveK(r, p, ur, a, b)
    Ktt2, Krr2= Kfirst.giveKinitial(r, p, Paether, ur, a, b)
    #pl.plot(r, Ktt)
    #pl.plot(r, Krr)
    #pl.plot(r, Ktt2)
    pl.plot(r, Krr2)
    pl.plot(r, -0.5*Ktt2)
    pl.show()

    
