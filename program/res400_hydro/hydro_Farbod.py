import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math
import initial_velocity as velocity
import initial_K as initial_K
import variable_change as var
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
import boundary_conditions as fixbc

# FOR DEBUGGING. DELETE LATER
plot_index = 0
vars_to_plot = 7
arrays_for_plotting = np.empty((2,vars_to_plot,gv.RES))


def evolve(r, Sr_tilde0, tau_tilde0, a0, b0, K_dens0):
    p, ur, a, b =  var.primitive_variables(r, Sr_tilde0, tau_tilde0, a0, b0)
    t0=0.0
    p_min=np.min(p)
    sr_min = np.min(Sr_tilde0)
    tau_min = np.min(tau_tilde0)
    grid_size = len(a0) # arbitrary
    def flatten(a):
        """
        Given an array of shape (5,grid_size), return 1d array
        of length 5xgrid_size
        """
        return np.ravel(a)
    def unflatten(a):
        """
        Given a raveled array, resize it to the shape (5,grid_size)
        and return
        """
        return np.reshape(a,(5,grid_size))
    state0= flatten(np.array([Sr_tilde0, tau_tilde0, a0, b0, K_dens0]))
    state=state0

################################################################################
    def f(t, state_flat):
     
        state = unflatten(state_flat)
        Sr_tilde=state[0]
        tau_tilde=state[1]
        a=state[2]
        b=state[3]
        K_dens=state[4]
 

        #BOUNDARY CONDITIONS
        # Sr_tilde and tau_tilde are zero at origin by construction
        #Sr_tilde=fixbc.central_derzero(Sr_tilde)
        #tau_tilde=fixbc.central_derzero(tau_tilde)
        a=fixbc.central_derzero(a)
        b=fixbc.central_derzero(b)
        K_dens=fixbc.central_derzero(K_dens)
        '''Sr_tilde[0]=0.0
        tau_tilde[0]=0.0
        a[0]=1.0
        b=fixbc.central_derzero(b)
        K_dens=fixbc.central_derzero(K_dens)'''
                
        p, ur, a, b =  var.primitive_variables(r, Sr_tilde, tau_tilde, a, b)
        
        #plot.connect(r, Sr_tilde)
        
        #quit()

        p=fixbc.central_derzero(p)
        ur[0]=0.0
        
        if gv.printme==0:
            print "t = {}\tp[10] = {}".format(t,p[10])
    #p=np.abs(p)
            metric_determinant=r*r*r*r*a*a*b*b*b*b
            sqrt_det=np.sqrt(metric_determinant)
            K=np.zeros_like(r)
            K[1:]=K_dens[1:]/sqrt_det[1:]
            q00 = -24.0/17.0
            q10 = 59.0/34.0
            q20 = -4.0/17.0
            q30 = -3.0/34.0
            q40 = 0.0
            q50 = 0.0
            first_element= (-q10*K[1:][0]-q20*K[1:][1]-q30*K[1:][2]-q40*K[1:][3]-q50*K[1:][4])/q00
            K=np.insert(K[1:], 0, first_element)
            Ktt, Krr = K_solver.giveK(r, p, K, ur, a, b)
            Krr=fixbc.central_derzero(Krr)
            Ktt=fixbc.central_derzero(Ktt)
            urup=ur/a/a
            W=np.sqrt(1.0+ur*ur/a/a)
            Gamma=gv.Gamma
            def rho(p):
                return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
            drhodp= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
            dpdrho=1.0/drhodp
            sump=rho(p)+p
            A0=W
            B0=(rho(p)+p)*ur/W/a/a
            Q0=(rho(p)+p)*W/a/a
            L0=W*(Gamma-1.0)*ur/a/a
            C0=np.zeros_like(r)
            ratio_C=urup[1:]/r[1:]
            q00 = -24.0/17.0
            q10 = 59.0/34.0
            q20 = -4.0/17.0
            q30 = -3.0/34.0
            q40 = 0.0
            q50 = 0.0
            first_element= (-q10*ratio_C[0]-q20*ratio_C[1]-q30*ratio_C[2]-q40*ratio_C[3]-q50*ratio_C[4])/q00
            ratio_C=np.insert(ratio_C, 0, first_element)
            C0= sump*der.first(urup,r) +sump*urup*(2.0*der.first(b,r)/b +der.first(a,r)/a) + sump*2.0*ratio_C +der.first(rho(p), r)*urup +  (sump)*(-W*Krr -2.0*W*Ktt + Krr*ur*ur/a/a/W)
            S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r)*urup*urup/a +der.first(urup, r)*urup)+der.first(p,r)*urup*urup +der.first(p,r)/a/a
            Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
            Paether=gv.Lambda*Omega0*(3.0*dpdrho-1.0)/K
            Paether=fixbc.central_derzero(Paether)
            alpha=alphafinder.give_alpha_integrate(r, p, Paether)
            #alpha=fixbc.outer_derzero(alpha)

            # FOR DEBUGGING. DELETE LATER
            '''global plot_index
            global arrays_for_plotting
            global vars_to_plot
            print plot_index
            arrays_for_plotting[plot_index,0] = r
            arrays_for_plotting[plot_index,1] = Sr_tilde
            arrays_for_plotting[plot_index,2] = tau_tilde
            arrays_for_plotting[plot_index,3] = a
            arrays_for_plotting[plot_index,4] = b
            arrays_for_plotting[plot_index,5] = ur
            arrays_for_plotting[plot_index,6] = K
            for i in range(1,vars_to_plot):
                if plot_index == 0:
                    continue
                lines = [pl.plot(r,arrays_for_plotting[j,i]) for j in range(plot_index+1)]
                pl.title('{}'.format(i))
                pl.show()
                pl.plot(r, arrays_for_plotting[plot_index,i] - arrays_for_plotting[plot_index-1,i])
                pl.title('{} difference'.format(i))
                pl.show()
            plot_index += 1'''
            #plot.connect(r, 2.0*Ktt+Krr-1e-4)
            #plot.connect(r, Krr)
            #plot.connect(r, der.first(K,r))

            #plot.connect(r, alpha)
            #plot.connect(r, b)
            vrup=alpha*ur/(W*a*a)
            Tttup=sump*W*W/alpha/alpha - p/alpha/alpha
            Trrup=sump*W*W*vrup*vrup/alpha/alpha + p/a/a
            TthetathetaupTIMESrquared=p/b/b
    #M1=0.5*alpha*a*b*b*(r*r*Tttup*der.first(-alpha*alpha,r)   + r*r*Trrup*der.first(a*a,r) + 2.0*TthetathetaupTIMESrquared*der.first(b*b*r*r, r))
            M1= 0.5*alpha*sqrt_det*Tttup*der.first(-alpha*alpha,r) +0.5*alpha*sqrt_det*Trrup*der.first(a*a,r) + alpha*a*p*(b*b*2.0*r +2.0*b*der.first(b,r)*r*r)

            '''pl.plot(r, 0.5*alpha*sqrt_det*Tttup*der.first(-alpha*alpha,r))
            pl.plot(r, 0.5*alpha*sqrt_det*Trrup*der.first(a*a,r))
            pl.plot(r, alpha*a*p*(b*b*2.0*r +2.0*b*der.first(b,r)*r*r))
            pl.plot(r, M1)
            pl.show()'''
    #M1[-1]= 0.5*alpha[-1]*sqrt_det[-1]*Tttup[-1]*(-(1.0-alpha[-1]*alpha[-1]/r[-1])) +0.5*alpha[-1]*sqrt_det[-1]*Trrup[-1]*der.first(a*a,r)[-1] + alpha[-1]*a[-1]*p[-1]*der.first(b*b*r*r, r)[-1]
            M2=sqrt_det*alpha*(a*a*Krr*Trrup +2.0*b*b*Ktt*TthetathetaupTIMESrquared)
            Trrupdown=sump*vrup*vrup*W*W*a*a/alpha/alpha + p
            Ttrup=sump*W*W*vrup/alpha/alpha
            Fr1=alpha*sqrt_det*Trrupdown
            Fr2= alpha*alpha*sqrt_det*Ttrup - vrup*sqrt_det*W*np.power(p/gv.K, 1.0/Gamma)
            Fr1=fixbc.central_derzero(Fr1)
            Fr2=fixbc.central_derzero(Fr2)
            rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
            S_Kdens=(rho(p)+p)*ur*ur/a/a +3.0*p -3.0*gv.Lambda*(3.0*p-rho(p)) +3.0*Paether
            C_Kdens= Krr*Krr+2.0*Ktt*Ktt+4.0*np.pi*gv.G*(S_Kdens+rhothree)
            fSr_tilde=M1-der.first(Fr1, r)
            
            ftau_tilde=M2-der.first(Fr2, r)
            fa=-alpha*a*Krr
            fb=-alpha*b*Ktt
            fKdens=-der.first(sqrt_det*der.first(alpha,r)/a/a, r) +alpha*sqrt_det*(C_Kdens-K*K)
            plot.connect(r, der.first(-alpha*alpha,r))
            plot.connect(r, fSr_tilde)
            #plot.connect(r, ftau_tilde)
            quit()
            '''for i in range(len(fSr_tilde)):
            if Sr_tilde[i] + gv.STEP*fSr_tilde[i] < sr_min:
            fSr_tilde[i] = 0
            if tau_tilde[i] + gv.STEP*ftau_tilde[i] < tau_min:
            ftau_tilde[i] = 0'''
            return flatten(np.array([fSr_tilde, ftau_tilde, fa, fb, fKdens]))
 ###########################################################################   


    def solout(t,state_flat):
        print "solout called."
        state = unflatten(state_flat)
        Sr_tilde=state[0]
        tau_tilde=state[1]
        a=state[2]
        b=state[3]
        K_dens=state[4]
        try:
            p, ur, a, b =  var.primitive_variables(r, Sr_tilde, tau_tilde, a, b)
        except:
            print "solout failed!"
            return -1
        def rho(p):
            return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
        if np.any(p < 0):
            print "solout failed!"
            return -1
        try:
            mrho = rho(p)
        except:
            print "solout failed!"
            return -1
        if np.any(mrho < 0):
            print "solout failed!"
            return -1
        return 0

    def fix_state(state_flat):
        """
        Fixes the state so the atmosphere is 'physical'
        """
        print "Fixing state!"
        state = unflatten(state_flat)
        Sr_tilde=state[0]
        tau_tilde=state[1]
        a=state[2]
        b=state[3]
        K_dens=state[4]
        for i in range(len(Sr_tilde)):
            if Sr_tilde[i] < sr_min:
                Sr_tilde[i] = sr_min
            if tau_tilde[i] < tau_min:
                tau_tilde[i] = tau_min
        return flatten(np.array([Sr_tilde,tau_tilde,a,b,K_dens]))

    state_int = ode(f)
    state_int.set_integrator('dopri5',max_step=gv.STEP,first_step=gv.STEP)
    #state_int.set_integrator('dopri5',first_step=gv.STEP)
    #state_int.set_solout(solout)
    state_int.set_initial_value(state0, t0)
    t_array=np.linspace(0.0, gv.TIME, gv.RESTIME)
    n_rows,n_columns = unflatten(state0).shape
    state_soln = np.empty((len(t_array),n_rows,n_columns))
    state_soln[0] = unflatten(state0)
    print "t_final = {}".format(gv.TIME)
    #t_slack = 1E-4
    for i,t in enumerate(t_array[1:]):
        state_int.integrate(t)
        
        while state_int.t < t:
            if solout(state_int.t,state_int.y) < 0:
                state_temp = fix_state(state_int.y)
                t_temp = state_int.t
                state_int = ode(f)
                state_int.set_integrator('dopri5')
                state_int.set_solout(solout)
                state_int.set_initial_value(state_temp, t_temp)
        
        state_soln[i+1] = unflatten(state_int.y)
        if gv.printme==1:
            print "t = {}/{}\na[10] = {}".format(t,t_array[-1],state_soln[i+1,2,10])
    Sr_tilde_array=state_soln[..., 0, ...]
    tau_tilde_array=state_soln[..., 1,...]
    a_array=state_soln[..., 2,...]
    b_array=state_soln[..., 3,...]
    K_dens_array=state_soln[..., 4,...]
    return Sr_tilde_array,  tau_tilde_array, a_array, b_array, K_dens_array


if __name__ == "__main__":

  
    '''SETTING INITIAL PRESSURE PROFILE FOR THE STAR'''
    r, p, Paether, m, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)    
    #r, p, Paether, m = np.loadtxt('profile_centralpressure0.012_RES500_Lambda0.25.out', skiprows=1, unpack=True)
    #plot.connect(r, der.first(Paether,r))
    #NS_radius=1.33478413497
    ###################################################

    '''
    # uncomment this code to focus only within the radius of the star
    p_inter=interpolate.interp1d(r, p, kind='cubic')
    Paether_inter=interpolate.interp1d(r, Paether, kind='cubic')
    m_inter=interpolate.interp1d(r, m, kind='cubic')
    r=np.linspace(0.0, NS_radius, gv.RES)
    p=p_inter(r)
    Paether=Paether_inter(r)
    m=m_inter(r)'''
 
    '''SETTING INITIAL SPATIAL METRIC COMPONENTS FOR THE STAR'''
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)    
    b=np.ones_like(r)  
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
 
    '''SETTING INITIAL VELOCITY PROFILE FOR THE STAR'''
    ur=velocity.velocity(r, p, Paether, a, b, NS_radius)

    #plot.connect(r, der.first(der.first(ur,r),r))
    #plot.connect(r, ur)

    ###################################################
    
    Sr_tilde, tau_tilde, a, b= var.hydro_variables(r, p, ur, a, b)
    #Ktt0, Krr0=initial_K.giveKinitial(r, p, Paether, ur, a, b)
    K=gv.ur_order*np.ones_like(r)
    Ktt, Krr = K_solver.giveK(r, p, K, ur, a, b)
    #pl.plot(r, Ktt0)
    #pl.plot(r, Krr0)
    #pl.plot(r, K)
    #pl.show()
    

    K_dens=sqrt_det*K
    #plot.connect(r, np.log10(p))
 
    urup=ur/a/a
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    drhodp= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
    dpdrho=1.0/drhodp
    sump=rho(p)+p
    A0=W
    B0=(rho(p)+p)*ur/W/a/a
    Q0=(rho(p)+p)*W/a/a
    L0=W*(Gamma-1.0)*ur/a/a
    C0=np.zeros_like(r)
    ratio_C=urup[1:]/r[1:]
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*ratio_C[0]-q20*ratio_C[1]-q30*ratio_C[2]-q40*ratio_C[3]-q50*ratio_C[4])/q00
    ratio_C=np.insert(ratio_C, 0, first_element)
    C0= sump*der.first(urup,r) +sump*urup*(2.0*der.first(b,r)/b +der.first(a,r)/a) + sump*2.0*ratio_C +der.first(rho(p), r)*urup +  (sump)*(-W*Krr -2.0*W*Ktt + Krr*ur*ur/a/a/W)
    S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r)*urup*urup/a +der.first(urup, r)*urup)+der.first(p,r)*urup*urup +der.first(p,r)/a/a
    Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
    Paether=gv.Lambda*Omega0*(3.0*dpdrho-1.0)/K

   
    alpha1=alphafinder.give_alpha_direct(r, p, Paether)
    alpha2=alphafinder.give_alpha_integrate(r, p, Paether)

    #rrr, alpha0=B_calc.giveB()
    #plot.connect (r, alpha2-alpha1)'''

    '''pl.plot(r, alpha1)
    pl.plot(r, alpha2)
    pl.plot(rrr, alpha0)
    pl.show()'''
    #Sr_tilde_array,  tau_tilde_array, a_array, b_array, K_dens_array=evolve(r, Sr_tilde, tau_tilde, a, b, K_dens)
    #print a_array

 



    
