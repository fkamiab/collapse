import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative_mirror as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math
import initial_velocity as vel
import initial_K as Kfirst


def giveHamiltonian(r, a, b, p, ur, Ktt, Krr):

    #r=r+1000
    aprime=der.first(a,r, der.EVEN)
    bprime=der.first(b,r, der.EVEN)
    bdoubleprime=der.second(b,r, der.EVEN)

    #r=r-1000

    R= -(2.0/(r*r*a*a*a*b*b))*(-2.0*r*b*aprime*(r*bprime+b)+a*(r*r*bprime*bprime +2.0*r*b*(r*bdoubleprime +3.0*bprime) +b*b) -a*a*a)

    #R= -(2.0/(r*r*a*a*a*b*b))*(-2.0*r*aprime + a- a*a*a)

    #R= -(2.0/(a*a*a*b*b))*(-2.0*2.0*aprime) -(2.0/(r*r*a*a*a*b*b))*(a- a*a*a)

    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)

    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    hamilton=ohr-R
#-4.0*Krr*Ktt-2.0*Ktt*Ktt
    #mthird=der.first(der.second(m, r, der.EVEN), r, der.EVEN)
    
    #pl.plot(r, R)
    #pl.plot(r, der.first(der.second(m, r, der.ODD), r, der.ODD))
    #pl.plot(r, 8.*np.pi*(rho(p)+p))

    #pl.plot(r, der.second(m, r, der.ODD)/r)
    #pl.plot(r, 8.*np.pi*(rho(p)+p))
    #r=100.0+r

    #pl.plot(r, der.first(m, r, der.ODD)/r/r)
    #pl.plot(r, 3.0*der.first(m, r*r*r, der.ODD))

    #pl.plot(r, 4.*np.pi*(rho(p)+p))


    #pl.plot(r, R)
    #pl.plot(r, ohr-R)
    #pl.show()
    
    #plot.connect(r[:-1], ur[:-1])
    
    print (ohr-R)[0]
    plot.connect(r, ohr-R)

    return ohr-R


#if __name__ == "__main__":



    
