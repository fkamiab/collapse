import numpy as np
import pylab as pl
import matplotlib as mpl
import plot as plot


if __name__ == "__main__":


    r=np.empty([50, 200])
    p=np.empty_like(r)
    Paether=np.empty_like(r)
    ur=np.empty_like(r)
    alpha=np.empty_like(r)
    a=np.empty_like(r)
    b=np.empty_like(r)
    K=np.empty_like(r)
    Sr_tilde=np.empty_like(r)
    tau_tilde=np.empty_like(r)

    for x in range(0, 9):
        i=x*5
        filename="time_step_{}.out".format(i)
        r[i], p[i], Paether[i], ur[i], alpha[i], a[i], b[i], K[i], Sr_tilde[i], tau_tilde[i] =np.loadtxt(filename, skiprows=1, unpack=True)
        pl.plot(r[i], K[i])
        

    pl.show()

   
 
    

  
 
