import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math
import initial_velocity as velocity
import initial_K as initial_K



def hydro_variables(r, p, ur, a, b):
    '''Finds hydro variables from primitive ones'''
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    Sr_tilde=sqrt_det*(rho(p)+p)*W*ur
    tau_tilde= sqrt_det*((rho(p)+p)*W*W-p)-sqrt_det*W*np.power(p/gv.K, 1.0/Gamma)
    return Sr_tilde, tau_tilde, a, b
#################################################



'''Finds primitive variables from hydro ones'''
def primitive_variables(r, Sr_tilde, tau_tilde, a, b):
    def F(r_value, p_value, ur_value, a_value, b_value):
        metric_determinant=r_value*r_value*r_value*r_value*a_value*a_value*b_value*b_value*b_value*b_value
        sqrt_det=np.sqrt(metric_determinant)
        W=np.sqrt(1.0+ur_value*ur_value/a_value/a_value)
        Gamma=gv.Gamma
        def rho(p_value):
            #return np.power(p_value/gv.K, 1.0/Gamma)+p_value/(Gamma-1.0)
            return np.power(np.abs(p_value)/gv.K, 1.0/Gamma)+np.abs(p_value)/(Gamma-1.0)
        Sr_tilde_value=sqrt_det*(rho(p_value)+p_value)*W*ur_value
        tau_tilde_value= sqrt_det*((rho(p_value)+p_value)*W*W-p_value)-sqrt_det*W*np.power(np.abs(p_value)/gv.K, 1.0/Gamma)
        array_F=np.vstack((Sr_tilde_value, tau_tilde_value))
        return array_F
    p0=np.ones_like(r)
    ur0=np.ones_like(r)*1e-3
    YY=np.vstack((Sr_tilde, tau_tilde))
    x0=np.vstack((p0, ur0)).ravel()
    #pu=np.vstack((p, ur)).ravel()
    def FF(p,ur):
        out = F(r,p,ur,a,b)-YY
        out[0,0] = ur[0]
        out[1,0] = der.first(p,r)[0]
        return out.ravel()
    def FFF(pu):
        p_1=np.split(pu, 2)[0]
        ur_1=np.split(pu, 2)[1]
        return FF(p_1, ur_1)
    sol=optimize.root(FFF, x0)
    p_sol,ur_sol=np.split(sol.x,2)
    return p_sol, ur_sol, a, b
###################################################################
    


if __name__ == "__main__":

  
    '''SETTING INITIAL PRESSURE PROFILE FOR THE STAR'''
    #r, p, Paether, m, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)    
    r, p, Paether, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    NS_radius=1.33478413497
    ###################################################



 
    '''SETTING INITIAL VELOCITY PROFILE FOR THE STAR'''
    ur=velocity.velocity(r, NS_radius)
    ###################################################


    '''SETTING INITIAL SPATIAL METRIC COMPONENTS FOR THE STAR'''
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)    
    b=np.zeros_like(r)+1.0   
    ###################################################


    
    Sr_tilde, tau_tilde, a, b= hydro_variables(r, p, ur, a, b)
    
    Ktt, Krr=initial_K.giveKinitial(r, p, Paether, ur, a, b)
 
    #pl.plot(r, Ktt)
    #pl.plot(r, Krr)
    #pl.show()

 
    
