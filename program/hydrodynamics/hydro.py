#!/usr/bin/env python

"""evolve.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 22:30:47 (jmiller)>

This file is the main evolution loop for the system with hydro
variables only.
"""

import numpy as np
import global_variables as gv
#import derivative as der
import derivative_mirror as der
import derivative_mirror_4 as der4
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import rhs
from scipy.integrate import ode
import pylab as pl
import matplotlib as mpl
import initial_velocity as velocity
import plot as plot
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
import Paether as PaetherFinder
import initial_K as initial_K
from scipy import interpolate
#import initial_profile as initial
import initial_profile_NOshooting as initial
import Hamiltonian_Constraint as Hamiltonian


def evolve(r, Sr_tilde0, tau_tilde0, a0, b0, K0):
    """
    The main loop. Evolves system from t0 to tfinal with nt steps
    with initial data given by r,rho0,p0,ur0,a,b,K,alpha.
    """
    # initialize output arrays
    t_array = np.linspace(0.0,gv.TIME,gv.RESTIME)
    soln_array = np.empty((len(t_array),gv.NUM_EVOLVED_VARIABLES,gv.RES))

    # initial state
    U0 = np.vstack((Sr_tilde0,tau_tilde0,a0,b0,K0))
    soln_array[0] = U0
 
   
    U0_flat = utilities.flatten(U0)

    # make right-hand-side
    my_rhs = rhs.make_rhs(r)

    integrator = ode(my_rhs)
    #integrator.set_integrator('dopri5' ,max_step=gv.STEP,first_step=gv.STEP)
    integrator.set_integrator('dopri5' , first_step=gv.STEP)
    integrator.set_initial_value(U0_flat,0.0)

    for i, t in enumerate(t_array[1:]):
        integrator.integrate(t)
        if gv.printme==1:
            print "integrated to {} out of {}.".format(t,gv.TIME)
        soln_array[i+1] = utilities.unflatten(integrator.y,
                                              gv.NUM_EVOLVED_VARIABLES)

        
        Sr_tilde_step=soln_array[i+1,0,...]
        tau_tilde_step=soln_array[i+1,1,...]
        a_step=soln_array[i+1,2,...]
        b_step=soln_array[i+1,3,...]
        K_step=soln_array[i+1,4,...]
        p_step, ur_step, a_step ,b_step=variables.primitive_variables(r, Sr_tilde_step, tau_tilde_step, a_step, b_step)
        Ktt_step, Krr_step = K_solver.giveK(r, p_step, K_step, ur_step, a_step, b_step)
        Paether_step=PaetherFinder.get_Paether(r, p_step, ur_step, Ktt_step, Krr_step, a_step, b_step)
        alpha_step=alphafinder.give_alpha_direct(r, p_step, Paether_step)


        filename="./time_outputs/time_step_{}.out".format(i+1)
        np.savetxt(filename, np.c_[r, p_step, Paether_step, ur_step, alpha_step, a_step, b_step, K_step, Sr_tilde_step, tau_tilde_step], delimiter='\t', header='Time={} and Columns are r\tp\tPaether\tur\talpha\ta\tb\tK\tSr_tilde\ttau_tilde'.format(t))
 

        #pl.plot(r, soln_array[i+1,4,...]) 
        #pl.plot(r, soln_array[i+1,4,...], 'ro')
        #pl.show()
#pl.savefig('K_{}_{}.png'.format(i,t))
        #pl.figure()

                

    Sr_tilde_soln=soln_array[..., 0, ...]
    tau_tilde_soln=soln_array[..., 1, ...]
    a_soln=soln_array[..., 2, ...]
    b_soln=soln_array[..., 3, ...]
    K_soln=soln_array[..., 4, ...]

    


    # and return
    return t_array, Sr_tilde_soln, tau_tilde_soln, a_soln, b_soln, K_soln

if __name__ == "__main__":


  
    '''SETTING INITIAL PRESSURE PROFILE FOR THE STAR'''
    #r, p0, Paether0, m0, a0, b0, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)   
    
    r, p0, Paether0, m0, a0, b0 = np.loadtxt('profile_centralpressure0.012_RES{}_Lambda0.25.out'.format(gv.RES), skiprows=1, unpack=True)
    #r2, p02, Paether02, m02, a02, b02 = np.loadtxt('profile_centralpressure0.012_RES{}_Lambda0.25.out'.format(gv.RES), skiprows=1, unpack=True)

    

    NS_radius=1.33177406643
    #plot.connect(r, m0)

    #plot.connect(r, 1.0/np.sqrt(m0/(4.0*np.pi*r*r*r/3.0)))
    #quit()
    ###################################################
    '''
    x=np.zeros_like(r)
    mpl.rcParams.update({'font.size':19})
    pl.plot(r, Paether0, lw=3, label=r'$\mathcal{P}_{aether}$')
    pl.plot(r, p0, lw=3, label=r'$p_{matter}$')
    pl.plot(r, x, linestyle='--', lw=3)
    #pl.yscale('log')
    pl.legend(loc=0, fontsize=24)
    pl.xlabel(r'$r$', fontsize=22)
    pl.ylabel(r'Pressure', fontsize=19)
    pl.savefig('p_aether_matter.pdf', bbox_inches='tight')
    #pl.show()
    quit()'''

    rho0=tmunu.get_rho(p0)
    '''SETTING INITIAL SPATIAL METRIC COMPONENTS FOR THE STAR'''
    sqrt_det=geometry.get_gamma_half(r,a0,b0)


    
    '''SETTING INITIAL VELOCITY PROFILE FOR THE STAR'''
    '''
    p0_inter= interpolate.interp1d(r, p0, kind=5)
    Paether0_inter= interpolate.interp1d(r, Paether0, kind=5)
    rho0_inter= interpolate.interp1d(r, p0, kind=5)
    a0_inter= interpolate.interp1d(r, a0, kind=5)
    b0_inter= interpolate.interp1d(r, b0, kind=5)
    

    r_lowres=np.linspace(0.0, r[len(r)-1], 50)
    p0_lowres=p0_inter(r_lowres)
    Paether0_lowres=Paether0_inter(r_lowres)
    rho0_lowres=rho0_inter(r_lowres)
    a0_lowres=a0_inter(r_lowres)
    b0_lowres=b0_inter(r_lowres)

    '''
    
    #ur0_lowres=velocity.velocity(r_lowres, p0_lowres, Paether0_lowres, a0_lowres, b0_lowres, NS_radius)
    
    ur0=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, gv.ur_order)
    
    '''ur0=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, 1e-2)   
    ur1=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, 5.*1e-3)
    ur2=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, 1e-3)
    
    pur0=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, -1e-2)   
    pur1=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, -5.*1e-3)
    pur2=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, -1e-3)
    
    
    
    mpl.rcParams.update({'font.size':15})
    pl.plot(r, pur0, lw=3, label=r'$K_0=-0.01$') 
    pl.plot(r, pur1, lw=3, label=r'$K_0=-0.005$')
    pl.plot(r, pur2, lw=3, label=r'$K_0=-0.001$')
    pl.plot(r, ur2, lw=3, label=r'$K_0=+0.001$')   
    pl.plot(r, ur1, lw=3, label=r'$K_0=+0.005$')
    pl.plot(r, ur0, lw=3, label=r'$K_0=+0.01$')
    ax = pl.subplot(111)
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    
    
 
    
    pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    pl.xlabel(r'$r$', fontsize=20)
    pl.ylabel(r'$u_r$', fontsize=20)
    pl.savefig('ur.pdf', bbox_inches='tight')
    #pl.show()
    quit()'''

     
    K=np.ones_like(r)*gv.ur_order
    Ktt0, Krr0 = K_solver.giveK(r, p0, K, ur0, a0, b0)

    #x=Hamiltonian.giveHamiltonian(r, a0, b0, p0, ur0, Ktt0, Krr0)

    
    
 

    #ur0_inter= interpolate.interp1d(r_lowres, ur0_lowres, kind=5)
    #ur0=ur0_inter(r)

    
    #plot.connectpoints(r, ur0)
    #plot.connectpoints(r, der.first(ur0,r, der.ODD))
    #plot.connectpoints(r, der.second(ur0, r, der.ODD))
    #plot.connectpoints(r, der.second(der.second(ur0, r, der.ODD), r, der.ODD))
    

    



    

    '''SETTING EXTRINSIC CURVATURE'''
 
    '''Ktt, Krr=initial_K.giveKinitial(r, p0, Paether0, ur0, a0, b0)
    K=2.0*Ktt+Krr
    pl.plot(r, Ktt)
    pl.plot(r, Krr)
    pl.plot(r, K)
    pl.show()
    quit()'''


    K0=gv.ur_order*np.ones_like(r)
    #K_dens0=sqrt_det*K0

    Sr_tilde0, tau_tilde0, a0, b0= variables.hydro_variables(r, p0, ur0, a0, b0)
    '''plot.connect(r, Sr_tilde)
    p,ur,a,b=variables.primitive_variables(r, Sr_tilde, tau_tilde, a, b)
    plot.connect(r, ur)'''

    '''alpha0=alphafinder.give_alpha_integrate(r, p0, Paether0)
    Ktt0, Krr0 = K_solver.giveK(r, p0, K, ur0, a0, b0)
    Paether=PaetherFinder.get_Paether(r, p0, ur0, Ktt0, Krr0, a0, b0)
    alpha2=alphafinder.give_alpha_integrate(r, p0, Paether)
    pl.plot(r, alpha0)
    pl.plot(r, alpha2, 'ro')
    pl.show()'''


    #plot.connectpoints(r, der.second(ur0, r, der.ODD))
    #plot.connectpoints(r, der.second(der.second(ur0, r, der.ODD), r, der.ODD))
    #plot.connectpoints(r, der.second(der.second(p0*r, r, der.ODD), r, der.ODD))
    #quit()
    #r,rho0,p0,ur0,a0,b0,K0,alpha0 = initial_data.gaussian_packet()
    #print "Beginning evolution"

    alpha0=alphafinder.give_alpha_direct(r, p0, Paether0)


    filename="./time_outputs/time_step_{}.out".format(0)
    np.savetxt(filename, np.c_[r, p0, Paether0, ur0, alpha0, a0, b0, K0, Sr_tilde0, tau_tilde0], delimiter='\t', header='Time={} and Columns are r\tp\tPaether\tur\talpha\ta\tb\tK\tSr_tilde\ttau_tilde'.format(0))
 
               
               

    t_array, Sr_tilde_soln, tau_tilde_soln, a_soln, b_soln, K_soln = evolve(r, Sr_tilde0, tau_tilde0, a0, b0, K0)



