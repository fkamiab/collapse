#!/usr/bin/env python

"""evolve.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 22:30:47 (jmiller)>

This file is the main evolution loop for the system with hydro
variables only.
"""

import numpy as np
import global_variables as gv
import derivative_mirror as der
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import rhs
from scipy.integrate import ode
import pylab as pl
import initial_velocity as velocity
import plot as plot
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
from scipy import interpolate
from numpy.polynomial import polynomial




def get_Paether(r, p, ur, Ktt, Krr, a, b):

  

    
    sqrt_det=geometry.get_gamma_half(r,a,b)
    K=Krr+2.0*Ktt
    urup=ur/a/a
    W=tmunu.get_w(ur, a)
    rho=tmunu.get_rho(p)
    dpdrho=tmunu.get_dpdrho(p)
    sump=rho+p

    
    A0=W
    B0=sump*ur/W/a/a
    Q0=sump*W/a/a
    L0=W*(gv.Gamma-1.0)*ur/a/a
    C0=np.zeros_like(r)
    #ratio_C = np.empty_like(r)
    #ratio_C[1:] = urup[1:]/r[1:]
    
    #ratio_C[0]=der.first(urup, r, der.ODD)[0]
    #ratio_C=der.set_to_zero_at_origin(ratio_C,r)

    ratio_C=urup/r
    
    
    
    C0= sump*der.first(urup,r, der.ODD) +sump*urup*(2.0*der.first(b,r, der.EVEN)/b +der.first(a,r, der.EVEN)/a) + sump*2.0*ratio_C +der.first(rho, r, der.EVEN)*urup +  (sump)*(-W*Krr -2.0*W*Ktt + Krr*ur*ur/a/a/W)
    S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r, der.EVEN)*urup*urup/a +der.first(urup, r, der.ODD)*urup)+der.first(p,r, der.EVEN)*urup*urup +der.first(p,r, der.EVEN)/a/a
    Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
    Paether=gv.Lambda*Omega0*(3.0*dpdrho-1.0)/K
    
    #Paether = der.set_to_zero_at_origin(Paether, r)

    

    return Paether

            
