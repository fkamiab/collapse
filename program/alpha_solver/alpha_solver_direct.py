import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import K_solver as K_solver
import derivative as der
import B_calc as B_calc
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import optimize
from scipy import interpolate
import math





if __name__ == "__main__":

    r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    #r, p, ptilde, m =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)

    #NS_radius=initial.RNcorrect(gv.p0)
    NS_radius=1.33478413497
    i_R=np.int(NS_radius/np.diff(r)[0] )

    
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)
    b=np.zeros_like(a)+1
    ur=np.zeros_like(r)
    urup=ur/a/a
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma

    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Ktt, Krr= K_solver.findK(r, p, ptilde, m)
    K=Krr+2.0*Ktt

    sump=rho(p)+p
    sump = sump + gv.delta_rhothree_frac*sump
    #plot.connect(r, a)

    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    A0=W
    B0=(rho(p)+p)*ur/W/a/a
    Q0=(rho(p)+p)*W/a/a
    L0=W*(Gamma-1.0)*ur/a/a
    C0=np.zeros_like(r)
    C0[1:]=((sump[1:])/(sqrt_det[1:]))*der.first(sqrt_det[1:]*urup[1:], r[1:]) + der.first(rho(p[1:]), r[1:])*urup[1:] +  (sump[1:])*(-W[1:]*Krr[1:] -2.0*W[1:]*Ktt[1:] + Krr[1:]*ur[1:]*ur[1:]/a[1:]/a[1:]/W[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*C0[1:][0]-q20*C0[1:][1]-q30*C0[1:][2]-q40*C0[1:][3]-q50*C0[1:][4])/q00
    C0=np.insert(C0[1:], 0, first_element)
    S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r)*urup*urup/a +der.first(urup, r)*urup)+der.first(p,r)*urup*urup +der.first(p,r)/a/a
    Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
    Omega=Omega0*(3.0*Gamma-4.0)

    pprime=der.first(p,r)
    rhoprime=der.first(rho(p), r)
    Kprime=der.first(K,r)

    pl.plot(r, der.first(p,r)/der.first(rho(p),r))
    #pl.plot(r, -C0)
    pl.show()
    
    '''
    def residual(alpha, K, Omega, pprime, rhoprime, Kprime):
        res=np.empty_like(r)
        #res[:i_R]=K[:i_R]*der.first(Omega*alpha,r)[:i_R] - (3.0*pprime[:i_R]-rhoprime[:i_R])*K[:i_R]*K[:i_R]*alpha[:i_R] + alpha[:i_R]*Omega[:i_R]*Kprime[:i_R]
        #res[i_R+1:]=K[i_R+1:]*der.first(Omega*alpha,r)[i_R+1:] - (3.0*pprime[i_R+1:]-rhoprime[i_R+1:])*K[i_R+1:]*K[i_R+1:]*alpha[i_R+1:] + alpha[i_R+1:]*Omega[i_R+1:]*Kprime[i_R+1:]
        res=K*der.first(Omega*alpha,r) - (3.0*pprime-rhoprime)*K*K*alpha - alpha*Omega*Kprime
        alpha_squared=alpha*alpha
        res[-1]=der.first(alpha_squared, r)[-1]- (1.0-alpha_squared[-1] )/r[-1]
        #res= Omega*K*der.first(alpha,r)- (3.0*pprime-rhoprime)*K*K-Omega*Kprime
        #res[i_R]=der.first(alpha_squared, r)[i_R]- (1.0-alpha_squared[i_R] )/r[i_R]
        res[0]= der.first(alpha,r)[0]
        return res


    r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES500_Lambda0.25.out', skiprows=1, unpack=True)
    #r, p, ptilde, m =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)
    alpha0=np.ones_like(r)
    sol = optimize.root(lambda alpha: residual(alpha, K, Omega, pprime, rhoprime, Kprime),alpha0)
    print sol.x
    #pl.plot(r, np.exp(sol.x)/Omega)
    pl.plot(r, sol.x)
    pl.show()
    
    '''
