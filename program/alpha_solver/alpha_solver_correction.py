import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import K_solver_direct as K_solver
import derivative as der
import B_calc as B_calc
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
import math





if __name__ == "__main__":
    
    r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    #r, p, ptilde, m =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)

    #NS_radius=initial.RNcorrect(gv.p0)
    NS_radius=1.33478413497
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)
    b=np.zeros_like(a)+1    
    ur=np.zeros_like(r)
    urup=ur/a/a
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)

    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    rhofactor=2.0*((-0.5*np.tanh(3.5*(r/NS_radius-1.0))+0.5)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius))*gv.delta_rhothree_frac
    rhothree=rhothree+rhofactor*rhothree
    Ktt, Krr= K_solver.findK(r, p, ptilde, m)
    K=Krr+2.0*Ktt

    sump=rho(p)+p
    #sump = sump + gv.delta_rhothree_frac*sump
    #sump= sump+sump*rhofactor

    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    A0=W
    B0=(rho(p)+p)*ur/W/a/a
    Q0=(rho(p)+p)*W/a/a
    L0=W*(Gamma-1.0)*ur/a/a
    C0=np.zeros_like(r)
    C0[1:]=((sump[1:])/(sqrt_det[1:]))*der.first(sqrt_det[1:]*urup[1:], r[1:]) + der.first(rho(p[1:]), r[1:])*urup[1:] +  (sump[1:])*(-W[1:]*Krr[1:] -2.0*W[1:]*Ktt[1:] + Krr[1:]*ur[1:]*ur[1:]/a[1:]/a[1:]/W[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*C0[1:][0]-q20*C0[1:][1]-q30*C0[1:][2]-q40*C0[1:][3]-q50*C0[1:][4])/q00
    C0=np.insert(C0[1:], 0, first_element)
    S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r)*urup*urup/a +der.first(urup, r)*urup)+der.first(p,r)*urup*urup +der.first(p,r)/a/a
    Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
    Omega=Omega0*(3.0*Gamma-4.0)
        
    

    
    Paether=np.ones_like(r)
    derp_rho=1.0/((1.0/gv.Gamma)*(p**(1.0/gv.Gamma -1.0)) + 1.0/(1.0-gv.Gamma) )
    Paether[1:]= gv.Lambda*Omega0[1:]*(3.0*derp_rho[1:]-1.0)/K[1:]
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*Paether[1:][0]-q20*Paether[1:][1]-q30*Paether[1:][2]-q40*Paether[1:][3]-q50*Paether[1:][4])/q00
    Paether=np.insert(Paether[1:], 0, first_element)

    alpha=np.ones_like(r)
    v=-180
    alpha[:v]=Paether[:v]*a[:v]*a[:v]*Krr[:v]/(der.first(Paether,r)[:v] - gv.Lambda*(3.0*der.first(p,r)[:v] -der.first(rho(p), r)[:v]))
    #print alpha[:v]
    print (der.first(Paether,r)[:v] - gv.Lambda*(3.0*der.first(p,r)[:v] -der.first(rho(p), r)[:v]))

#Paether[:v]*a[:v]*a[:v]*Krr[:v]

#/(der.first(Paether,r)[:v] - gv.Lambda*(3.0*der.first(p,r)[:v] -der.first(rho(p), r)[:v]))



    rB, B=B_calc.giveB()
    pl.plot(r[:v], alpha[:v])
    #pl.plot(rB, B)
    pl.show()
    #plot.connect()
