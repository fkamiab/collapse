K=1.0
Gamma=9.0/5.0
delta_rhothree_frac=1.0/100.0
Lambda=1.0/4.0
GN=1.0
G=GN/(1.0-Lambda)
p0=0.012
RES=200
pressure_ratio_truncate=1e-7
radius_truncation_factor=50.0
RN_factor_truncate=1.0
ur_order=1e-4
ur_sigma=0.1

radius_factor=3.0 #the ratio of the distance up to which the profile is integrated, to the radius of the star

