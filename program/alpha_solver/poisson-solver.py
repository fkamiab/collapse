#!/usr/bin/env python

import numpy as np
from scipy import optimize
import matplotlib as mpl
from matplotlib import pyplot as plt

x = np.linspace(0,1,200)
dx = x[1]-x[0]

def der(y):
    yprime = np.empty_like(x)
    yprime[1:-1] = (y[2:] - y[:-2])/(2*dx)
    yprime[0] = (y[1]-y[0])/(dx)
    yprime[-1] = (y[-1]-y[-2])/(dx)
    return yprime

def residual(y):
    """
    When the ODE and boundary conditions are satisfied,
    residual should vanish at all grid points
    """
    ypp = der(der(y))
    res = np.empty_like(y)
    # interior
    res[1:-1] = ypp[1:-1] - 20*x[1:-1]**3 - 6
    # boundary
    res[0] = y[0]
    res[-1] = y[-1]-5
    return res


# sol is a solution object
# sol.x = the solution to the ODE
sol = optimize.root(residual,y0)

plt.plot(x,sol.x)
plt.show()
