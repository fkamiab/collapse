import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math





def findK(r, p, ptilde, m):
    NS_radius=1.33478413497
    h=np.diff(r)[1]
    def int_array(Radius):
        return int(Radius/h)
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)
    b=np.zeros_like(a)+1    
    aprime=der.first(a,r)
    bprime=der.first(b,r)
    bdoubleprime=der.second(b,r)
    R_1= -(2.0/(r[1:]*r[1:]*a[1:]*a[1:]*a[1:]*b[1:]*b[1:]))*(-2.0*r[1:]*b[1:]*aprime[1:]*(r[1:]*bprime[1:]+b[1:])+a[1:]*(r[1:]*r[1:]*bprime[1:]*bprime[1:] +2.0*r[1:]*b[1:]*(r[1:]*bdoubleprime[1:] +3.0*bprime[1:]) +b[1:]*b[1:]) -a[1:]*a[1:]*a[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*R_1[0]-q20*R_1[1]-q30*R_1[2]-q40*R_1[3]-q50*R_1[4])/q00
    R=np.insert(R_1, 0, first_element)
    ur=np.zeros_like(R)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    R=ohr
    #rhofactor=np.ones_like(r)*gv.delta_rhothree_frac*r*r*r*r
    rhofactor=2.0*((-0.5*np.tanh(3.5*(r/NS_radius-1.0))+0.5)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius))*gv.delta_rhothree_frac
#np.tanh(r/10.0)    
    rhothree=rhothree+rhofactor*rhothree
    #rhothree= rhothree+ gv.delta_rhothree_frac*rhothree

    RHO=16.0*np.pi*gv.G*rhothree-R
    RHOprime=der.first(RHO,r)
    a_inter = interpolate.interp1d(r, a, kind='cubic')
    b_inter = interpolate.interp1d(r, b, kind='cubic')
    bprime_inter = interpolate.interp1d(r, bprime, kind='cubic')
    R_inter = interpolate.interp1d(r, R, kind='cubic')
    Sr_inter = interpolate.interp1d(r, Sr, kind='cubic')
    rhothree_inter = interpolate.interp1d(r, rhothree, kind='cubic')
    RHO_inter=interpolate.interp1d(r, RHO, kind='cubic')
    RHOprime_inter=interpolate.interp1d(r, RHOprime, kind='cubic')
    #Ktt0=np.sqrt((16.0*np.pi*gv.G*rhothree[0]-R[0])/6.0)
    #Krr0=(16.0*np.pi*gv.G*rhothree[0]-R[0]-2.0*Ktt0*Ktt0)/(4.0*Ktt0)
    #Ktt0=0.0
    #Krr0=0.0
    
 
    def residual(Ktt_sol, r, b, bprime, Sr, rhothree, R):
        res=np.empty_like(r)
        res=4.0*b*r*Ktt_sol*der.first(Ktt_sol, r)+16.0*np.pi*gv.G*b*r*Ktt_sol*Sr+(b+r*bprime)*(6.0*Ktt_sol*Ktt_sol-16.0*np.pi*gv.G*rhothree+R)
        res[0]=Ktt_sol[0]
        return res

    Ktt0=np.ones_like(r)
    sol = optimize.root(lambda Ktt_sol: residual(Ktt_sol, r, b, bprime, Sr, rhothree, R),Ktt0)
    #print sol.x
    #pl.plot(r, np.exp(sol.x)/Omega)
    #pl.plot(r, sol.x)
    #pl.show()

    Ktt=sol.x
    Krr=np.empty_like(r)
    Krr[1:]=(16.0*np.pi*gv.G*rhothree[1:]-R[1:] -2.0*Ktt[1:]*Ktt[1:])/(4.0*Ktt[1:])
    Krr[0]=0.0
    #print Ktt


    return Ktt, Krr


if __name__ == "__main__":
    r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)
    NS_radius=1.33478413497

    Ktt, Krr=  findK(r, p, ptilde, m)
    K=Krr+2.0*Ktt
    
    ur=np.zeros_like(r)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    factor=2.0*((-0.5*np.tanh(3.5*(r/NS_radius-1.0))+0.5)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius))*gv.delta_rhothree_frac
#np.ones_like(r)*gv.delta_rhothree_frac*(r/NS_radius)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius)*np.tanh(1.0-r/NS_radius)
    rhothree1=rhothree+factor*rhothree
    
    #pl.plot(r, factor)
    #pl.plot(r, np.log10(rhothree))
    #pl.plot(r, np.log10(rhothree1))
    #pl.plot(r, (rhothree))
    #pl.plot(r, (rhothree1))
    #pl.show()


    pl.plot(r, der.first(K,r))
    #pl.plot(r, Ktt)
    #pl.plot(r, Krr)
    pl.show()
    
    #factor=np.tanh(r/10.0)
    #plot.connect(r, factor)
