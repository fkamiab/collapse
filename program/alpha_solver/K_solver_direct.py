import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import B_calc as B_calc
import math



'''

def findK(r, p, ptilde, m):
    NS_radius=1.33478413497
    h=np.diff(r)[1]
    def int_array(Radius):
        return int(Radius/h)
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)
    b=np.zeros_like(a)+1    
    aprime=der.first(a,r)
    bprime=der.first(b,r)
    bdoubleprime=der.second(b,r)
    R_1= -(2.0/(r[1:]*r[1:]*a[1:]*a[1:]*a[1:]*b[1:]*b[1:]))*(-2.0*r[1:]*b[1:]*aprime[1:]*(r[1:]*bprime[1:]+b[1:])+a[1:]*(r[1:]*r[1:]*bprime[1:]*bprime[1:] +2.0*r[1:]*b[1:]*(r[1:]*bdoubleprime[1:] +3.0*bprime[1:]) +b[1:]*b[1:]) -a[1:]*a[1:]*a[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*R_1[0]-q20*R_1[1]-q30*R_1[2]-q40*R_1[3]-q50*R_1[4])/q00
    R=np.insert(R_1, 0, first_element)
    ur=np.zeros_like(R)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    R=ohr
    #rhofactor=np.ones_like(r)*gv.delta_rhothree_frac*r*r*r*r
    rhofactor=2.0*((-0.5*np.tanh(3.5*(r/NS_radius-1.0))+0.5)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius)*(r/NS_radius))*gv.delta_rhothree_frac
#np.tanh(r/10.0)    
    rhothree=rhothree+rhofactor*rhothree
    #rhothree= rhothree+ gv.delta_rhothree_frac*rhothree

    RHO=16.0*np.pi*gv.G*rhothree-R
    RHOprime=der.first(RHO,r)
    a_inter = interpolate.interp1d(r, a, kind='cubic')
    b_inter = interpolate.interp1d(r, b, kind='cubic')
    bprime_inter = interpolate.interp1d(r, bprime, kind='cubic')
    R_inter = interpolate.interp1d(r, R, kind='cubic')
    Sr_inter = interpolate.interp1d(r, Sr, kind='cubic')
    rhothree_inter = interpolate.interp1d(r, rhothree, kind='cubic')
    RHO_inter=interpolate.interp1d(r, RHO, kind='cubic')
    RHOprime_inter=interpolate.interp1d(r, RHOprime, kind='cubic')
    #Ktt0=np.sqrt((16.0*np.pi*gv.G*rhothree[0]-R[0])/6.0)
    #Krr0=(16.0*np.pi*gv.G*rhothree[0]-R[0]-2.0*Ktt0*Ktt0)/(4.0*Ktt0)
    #Ktt0=0.0
    #Krr0=0.0
    
 
    def residual(Ktt_sol, r, b, bprime, Sr, rhothree, R):
        res=np.empty_like(r)
        res=4.0*b*r*Ktt_sol*der.first(Ktt_sol, r)+16.0*np.pi*gv.G*b*r*Ktt_sol*Sr+(b+r*bprime)*(6.0*Ktt_sol*Ktt_sol-16.0*np.pi*gv.G*rhothree+R)
        res[0]=Ktt_sol[0]
        return res

    Ktt0=np.ones_like(r)
    sol = optimize.root(lambda Ktt_sol: residual(Ktt_sol, r, b, bprime, Sr, rhothree, R),Ktt0)
    #print sol.x
    #pl.plot(r, np.exp(sol.x)/Omega)
    #pl.plot(r, sol.x)
    #pl.show()

    Ktt=sol.x
    Krr=np.empty_like(r)
    Krr[1:]=(16.0*np.pi*gv.G*rhothree[1:]-R[1:] -2.0*Ktt[1:]*Ktt[1:])/(4.0*Ktt[1:])
    Krr[0]=0.0
    #print Ktt


    return Ktt, Krr
'''

if __name__ == "__main__":
    r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    b=np.zeros_like(r)+1.0
    a=np.insert(a_1, 0, 1.0)
    NS_radius=1.33478413497
    i_R=np.int(NS_radius/np.diff(r)[0] )
    
    
    ur_mu=NS_radius-0.5*NS_radius
    ur_sigma=gv.ur_sigma
    ur_a=1.0/ur_sigma/np.sqrt(2.0*np.pi)
    ur_b=ur_mu
    ur_c = ur_sigma
    ur=-gv.ur_order*np.ones_like(r)*ur_a*np.exp(-(r-ur_b)*(r-ur_b)/2.0/ur_c/ur_c)

    ur=-np.ones_like(r)*r*gv.ur_order

    W=np.sqrt(1.0+ur*ur/a/a)
    
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    

    aprime=der.first(a,r)
    bprime=der.first(b,r)
    bdoubleprime=der.second(b,r)
    R_1= -(2.0/(r[1:]*r[1:]*a[1:]*a[1:]*a[1:]*b[1:]*b[1:]))*(-2.0*r[1:]*b[1:]*aprime[1:]*(r[1:]*bprime[1:]+b[1:])+a[1:]*(r[1:]*r[1:]*bprime[1:]*bprime[1:] +2.0*r[1:]*b[1:]*(r[1:]*bdoubleprime[1:] +3.0*bprime[1:]) +b[1:]*b[1:]) -a[1:]*a[1:]*a[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*R_1[0]-q20*R_1[1]-q30*R_1[2]-q40*R_1[3]-q50*R_1[4])/q00
    R=np.insert(R_1, 0, first_element)


    ohr=16.0*np.pi*gv.G*rhothree
    R=ohr
    

    Ktt=np.zeros_like(r)
    Krr= 4.0*np.pi*gv.G*r*Sr
    K=Krr+2.0*Ktt

    
    sump=rho(p)+p
    urup=ur/a/a
    
    
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    A0=W
    B0=(rho(p)+p)*ur/W/a/a
    Q0=(rho(p)+p)*W/a/a

    
    
    #C0=np.zeros_like(r)
    #C0[1:]=((sump[1:])/(sqrt_det[1:]))*der.first(sqrt_det[1:]*urup[1:], r[1:]) + der.first(rho(p[1:]), r[1:])*urup[1:] +  (sump[1:])*(-W[1:]*Krr[1:] -2.0*W[1:]*Ktt[1:] + Krr[1:]*ur[1:]*ur[1:]/a[1:]/a[1:]/W[1:])
    #plot.connect(r[1:], C0[1:])
    
    #q00 = -24.0/17.0
    #q10 = 59.0/34.0
    #q20 = -4.0/17.0
    #q30 = -3.0/34.0
    #q40 = 0.0
    #q50 = 0.0
    #first_element= (-q10*C0[1:][0]-q20*C0[1:][1]-q30*C0[1:][2]-q40*C0[1:][3]-q50*C0[1:][4])/q00
    #C0=np.insert(C0[1:], 0, first_element)

    C0=((sump))*der.first(sqrt_det*urup, r) + (sqrt_det)*der.first(rho(p), r)*urup +  (sqrt_det)*(sump)*(-W*Krr -2.0*W*Ktt + Krr*ur*ur/a/a/W)
    #plot.connect(r, B0)
    S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r)*urup*urup/a +der.first(urup, r)*urup)+der.first(p,r)*urup*urup +der.first(p,r)/a/a
    
 

    drhodp= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
    dpdrho=1.0/drhodp
    L0=W*(dpdrho)*ur/a/a
    Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
    Omega=Omega0*(3.0*dpdrho-1.0)
    pprime=der.first(p,r)
    rhoprime=der.first(rho(p), r)
    Kprime=der.first(K,r)
    #pl.plot(r, Omega)
    #pl.plot(r, ur)
    #pl.show()
    
    '''
    def residual(alpha, K, Omega, pprime, rhoprime, Kprime):
        res=np.empty_like(r)
        #res[:i_R]=K[:i_R]*der.first(Omega*alpha,r)[:i_R] - (3.0*pprime[:i_R]-rhoprime[:i_R])*K[:i_R]*K[:i_R]*alpha[:i_R] + alpha[:i_R]*Omega[:i_R]*Kprime[:i_R]
        #res[i_R+1:]=K[i_R+1:]*der.first(Omega*alpha,r)[i_R+1:] - (3.0*pprime[i_R+1:]-rhoprime[i_R+1:])*K[i_R+1:]*K[i_R+1:]*alpha[i_R+1:] + alpha[i_R+1:]*Omega[i_R+1:]*Kprime[i_R+1:]
        #res=K*der.first(Omega*alpha,r) - (3.0*pprime-rhoprime)*K*K*alpha - alpha*Omega*Kprime
        res=K*sqrt_det*der.first(Omega*alpha, r)-K*der.first(sqrt_det,r)*Omega*alpha-sqrt_det*(sqrt_det*(3.0*pprime-rhoprime)*K*K*alpha+ alpha*Omega*Kprime)
        alpha_squared=alpha*alpha
    
       
        #res[-1]=der.first(alpha_squared, r)[-1]- (1.0-alpha_squared[-1] )/r[-1]
        #res= Omega*K*der.first(alpha,r)- (3.0*pprime-rhoprime)*K*K-Omega*Kprime
        res[i_R]=der.first(alpha_squared, r)[i_R]- (1.0-alpha_squared[i_R] )/r[i_R]
        #res[0]= der.first(alpha,r)[0]
        return res
        '''

    
    Omega_inter = interpolate.interp1d(r, Omega, kind='cubic')
    
    sqrt_det_inter=interpolate.interp1d(r,  sqrt_det, kind='cubic')
    K_inter=interpolate.interp1d(r, K, kind='cubic')
    pprime_inter=interpolate.interp1d(r, pprime, kind='cubic')
    rhoprime_inter=interpolate.interp1d(r, rhoprime, kind='cubic')
    Kprime_inter=interpolate.interp1d(r, Kprime, kind='cubic')
    A0_inter=interpolate.interp1d(r, A0, kind='cubic')
    B0_inter=interpolate.interp1d(r, B0, kind='cubic')
    C0_inter=interpolate.interp1d(r, C0, kind='cubic')
    Q0_inter=interpolate.interp1d(r, Q0, kind='cubic')
    L0_inter=interpolate.interp1d(r, L0, kind='cubic')
    S0_inter=interpolate.interp1d(r, S0, kind='cubic')
    dpdrho_inter=interpolate.interp1d(r, dpdrho, kind='cubic')
    ur_inter=interpolate.interp1d(r, ur, kind='cubic')
    Kprime_inter=interpolate.interp1d(r, Kprime, kind='cubic')

    RR, BB=B_calc.giveB()
    srt_det= sqrt_det_inter(RR)
    K=K_inter(RR)
    pprime=pprime_inter(RR)
    rhoprime=rhoprime_inter(RR)
    Krpime=Kprime_inter(RR)
    A0=A0_inter(RR)
    B0=B0_inter(RR)
    C0=C0_inter(RR)
    Q0=Q0_inter(RR)
    S0=S0_inter(RR)
    L0=L0_inter(RR)
    dpdrho=dpdrho_inter(RR)
    ur=ur_inter(RR)
    Kprime=Kprime_inter(RR)
    chi= 3.0*pprime-rhoprime
    mu=sqrt_det*B0*S0-Q0*C0
    nu=A0*Q0-L0*B0

    A_alpha=K*sqrt_det*nu*mu*(3.0*dpdrho-1.0)
    B_alpha=K*sqrt_det*nu*der.first(mu*(3.0*dpdrho-1.0),r) -K*mu*(3.0*dpdrho-1.0)*der.first(sqrt_det*nu,r) -chi*K*K*sqrt_det*sqrt_det*nu*nu -Kprime*mu*sqrt_det*nu*(3.0*dpdrho-1.0)

    A_alpha=1e10*A_alpha
    B_alpha=1e10*B_alpha

    #res=K*sqrt_det*der.first(Omega, r)
#K*sqrt_det*der.first(Omega*BB, r)-K*der.first(sqrt_det,r)*Omega*BB-sqrt_det*(sqrt_det*(3.0*pprime-rhoprime)*K*K*BB+ BB*Omega*Kprime)
    #alpha_squared=alpha*alpha
    
    def residual(alpha, r, A_alpha, B_alpha):
        res=np.empty_like(r)
        #res=A_alpha*der.first(alpha,r)+B_alpha*alpha
        #res=A_alpha*der.first(alpha,r)+B_alpha
        res=np.log10(np.abs(A_alpha))-np.log10(np.abs(B_alpha)) +np.log10(der.first(alpha,r))
        #alpha_squared=alpha*alpha
        #res[-1]=der.first(alpha_squared, r)[-1]- (1.0-alpha_squared[-1] )/r[-1]
        #res[0]= der.first(alpha,r)[0]
        return res

    f=3.0*dpdrho-1.0
    #alpha0=np.ones_like(RR)
    #sol = optimize.root(lambda alpha: residual(alpha, RR, A_alpha, B_alpha), alpha0)
    #pl.plot(RR, sol.x)
    #pl.show()
    #plot.connect(RR, ur)
    #plot.connect(RR, K)
    #plot.connect(RR, ur)
    #pl.plot(RR, np.log10(np.abs(1e10*mu)))
    #pl.plot(RR, np.log10(np.abs(1e10*f)))
    #pl.plot(RR, np.log10(np.abs(B_alpha))-np.log10(np.abs(A_alpha)))
    #pl.plot(RR, np.log10(np.abs(der.first(BB,RR)))-np.log10(np.abs(BB)))
    #pl.plot(RR, -B_alpha/A_alpha)
    #pl.plot(RR, np.log10(np.abs(B_alpha)))
    '''pl.plot(RR, B_alpha)
    pl.plot(RR, K*sqrt_det*nu*der.first(mu*(3.0*dpdrho-1.0),r) )
    pl.plot(RR, -K*mu*(3.0*dpdrho-1.0)*der.first(sqrt_det*nu,r))
    pl.plot(RR,-chi*K*K*sqrt_det*sqrt_det*nu*nu)
    pl.plot(RR,-Kprime*mu*sqrt_det*nu*(3.0*dpdrho-1.0))'''
    pl.show()

    
    '''
    alpha0=np.ones_like(r)
    sol = optimize.root(lambda alpha: residual(alpha, K, Omega, pprime, rhoprime, Kprime),alpha0)

    #plot.connect(r, Omega)
    pl.plot(r, sol.x)
    pl.show()

    #pl.plot(r, sqrt_det*((3.0*pprime-rhoprime)*K*K+ Omega*Kprime)+K*der.first(sqrt_det,r)*Omega)
   
    #pl.show()
    '''
    

    
