import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import K_solver as K_solver
import derivative as der
import B_calc as B_calc
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
import math





if __name__ == "__main__":
    
    r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    #r, p, ptilde, m =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)

    #NS_radius=initial.RNcorrect(gv.p0)
    NS_radius=1.33478413497
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)
    b=np.zeros_like(a)+1    
    ur=np.zeros_like(r)
    urup=ur/a/a
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Ktt, Krr= K_solver.findK(r, p, ptilde, m)
    K=Krr+2.0*Ktt

    sump=rho(p)+p
    sump = sump + gv.delta_rhothree_frac*sump
    #plot.connect(r, a)

    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    A0=W
    B0=(rho(p)+p)*ur/W/a/a
    Q0=(rho(p)+p)*W/a/a
    L0=W*(Gamma-1.0)*ur/a/a
    C0=np.zeros_like(r)
    C0[1:]=((sump[1:])/(sqrt_det[1:]))*der.first(sqrt_det[1:]*urup[1:], r[1:]) + der.first(rho(p[1:]), r[1:])*urup[1:] +  (sump[1:])*(-W[1:]*Krr[1:] -2.0*W[1:]*Ktt[1:] + Krr[1:]*ur[1:]*ur[1:]/a[1:]/a[1:]/W[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*C0[1:][0]-q20*C0[1:][1]-q30*C0[1:][2]-q40*C0[1:][3]-q50*C0[1:][4])/q00
    C0=np.insert(C0[1:], 0, first_element)
    S0=(sump)*(2.0*ur*W*Krr/a/a -2.0*Krr*W*urup+ der.first(a,r)*urup*urup/a +der.first(urup, r)*urup)+der.first(p,r)*urup*urup +der.first(p,r)/a/a
    Omega0=(B0*S0-Q0*C0)/(A0*Q0-L0*B0)
    Omega=Omega0*(3.0*Gamma-4.0)
    

    #plot.connect(r, sqrt_det)
    p_inter= interpolate.interp1d(r, p, kind='cubic')
    lnp=np.log(p_inter(r))
    lnpprime=der.first(lnp, r)
    K_inter = interpolate.interp1d(r, K, kind='cubic')
    Kprime=der.first(K,r)
    Kprime_inter=interpolate.interp1d(r, Kprime, kind='cubic')
    rhoprime=der.first(rho(p), r)
    pprime=der.first(p, r)
    rhoprime_inter=interpolate.interp1d(r, rhoprime, kind='cubic')
    pprime_inter=interpolate.interp1d(r, pprime, kind='cubic')
    Omega_inter=interpolate.interp1d(r, Omega, kind='cubic')

    #pl.plot(r, np.log10(Omega_inter(r)/K_inter(r)))
    #pl.plot(r, np.log10(Omega_inter(r)))
    #pl.plot(r, pprime_inter(r)/p_inter(r))
    #pl.plot(r, lnpprime, 'ro')
    #pl.show()
    #plot.connect(r, K*(3.0*pprime_inter(r)-rhoprime_inter(r))/Omega_inter(r))
    #plot.connect(r, K_inter(r)*(3.0*pprime_inter(r)-rhoprime_inter(r))/Omega_inter(r))
    #pl.plot(r, np.log10(np.abs(K_inter(r)*(3.0*pprime_inter(r)-rhoprime_inter(r)))))
    #pl.plot(r, np.log10(1.0/Omega_inter(r)))
    #pl.plot(r, K_inter(r))
    #pl.plot(r, 3.0*pprime_inter(r)-rhoprime_inter(r))
    #pl.show()


    
    Z0=15.0
    r0=0.0
    state0=np.array([Z0])
    state=state0
    
    def f(radius, state):
        if(radius==0):
            fZ=0.0
        else:
            Z=state[0]
            fZ=(3.0*pprime_inter(radius) -rhoprime_inter(radius))*K_inter(radius)*Z/Omega_inter(radius) + Z*Kprime_inter(radius)/K_inter(radius)
        return np.array([fZ])
    state_int = ode(f)
    state_int.set_integrator('dopri5',max_step=1E-2,first_step=1E-6,dfactor=1E-4)
    state_int.set_initial_value(state0, r0)
    state_soln = np.empty((len(r),len(state0)))
    state_soln[0] = state0
    for i,radius in enumerate(r[1:]):
        state_int.integrate(radius)
        state_soln[i+1] = state_int.y
    Z=state_soln[..., 0]
    
    ALPHA=np.zeros_like(Z)
    ALPHA[1:]=np.sqrt(1.0-2.0*m[1:]/r[1:])
    ALPHA_inter = interpolate.InterpolatedUnivariateSpline(r[1:], ALPHA[1:])
    value_alpha=ALPHA_inter(NS_radius)
    ALPHA=ALPHA_inter(r)
    

    #pl.plot(r, K)
    #pl.plot(r, Z)
   
    alpha=Z/Omega

    new_r, sqrtB=B_calc.giveB()
    #pl.plot(new_r, sqrtB)
    
    factor=alpha[len(r)-1]/value_alpha


    square_alpha= alpha*alpha 
    der_square_alpha=der.first(square_alpha, r)
    alpharight = np.sqrt(np.abs(1.0-r*der_square_alpha))
    factor=alpharight[len(r)-1]/alpha[len(r)-1]
    pl.plot(r[:-137], alpha[:-137]*factor)
    #print alpha/factor
    #pl.plot(r, ALPHA)
    pl.show()
    
    #plot.connect(r[:-137], Z[:-137])
    
    
    

    
    

    
    
