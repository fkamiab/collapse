import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
import math





def findK(r, p, ptilde, m):
    h=np.diff(r)[1]
    def int_array(Radius):
        return int(Radius/h)
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)
    b=np.zeros_like(a)+1    
    aprime=der.first(a,r)
    bprime=der.first(b,r)
    bdoubleprime=der.second(b,r)
    R_1= -(2.0/(r[1:]*r[1:]*a[1:]*a[1:]*a[1:]*b[1:]*b[1:]))*(-2.0*r[1:]*b[1:]*aprime[1:]*(r[1:]*bprime[1:]+b[1:])+a[1:]*(r[1:]*r[1:]*bprime[1:]*bprime[1:] +2.0*r[1:]*b[1:]*(r[1:]*bdoubleprime[1:] +3.0*bprime[1:]) +b[1:]*b[1:]) -a[1:]*a[1:]*a[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*R_1[0]-q20*R_1[1]-q30*R_1[2]-q40*R_1[3]-q50*R_1[4])/q00
    R=np.insert(R_1, 0, first_element)
    ur=np.zeros_like(R)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    R=ohr
    #rhofactor=np.ones_like(r)*gv.delta_rhothree_frac*np.tanh(10.0*r)    
    #rhothree=rhothree+rhofactor*rhothree
    rhothree= rhothree+ gv.delta_rhothree_frac*rhothree

    RHO=16.0*np.pi*gv.G*rhothree-R
    RHOprime=der.first(RHO,r)
    a_inter = interpolate.interp1d(r, a, kind='cubic')
    b_inter = interpolate.interp1d(r, b, kind='cubic')
    bprime_inter = interpolate.interp1d(r, bprime, kind='cubic')
    R_inter = interpolate.interp1d(r, R, kind='cubic')
    Sr_inter = interpolate.interp1d(r, Sr, kind='cubic')
    rhothree_inter = interpolate.interp1d(r, rhothree, kind='cubic')
    RHO_inter=interpolate.interp1d(r, RHO, kind='cubic')
    RHOprime_inter=interpolate.interp1d(r, RHOprime, kind='cubic')
    Ktt0=np.sqrt((16.0*np.pi*gv.G*rhothree[0]-R[0])/6.0)
    Krr0=(16.0*np.pi*gv.G*rhothree[0]-R[0]-2.0*Ktt0*Ktt0)/(4.0*Ktt0)
    #Ktt0=0.0
    #Krr0=0.0
    r0=0.0
    state0=np.array([Ktt0])
    state=state0
    def f(radius, state):
        if(radius==0):
            fKtt=0.0
        else:
            Ktt=state[0]
            Krr=(16.0*np.pi*gv.G*rhothree_inter(radius)-R_inter(radius)-2.0*Ktt*Ktt)/(4.0*Ktt)
            fKtt=-4.0*np.pi*gv.G*Sr_inter(radius)-(1.0/radius +bprime_inter(radius)/b_inter(radius))*(Ktt-Krr)
        return np.array([fKtt])
    state_int = ode(f)
    state_int.set_integrator('dopri5',max_step=1E-2,first_step=1E-6,dfactor=1E-4)
    state_int.set_initial_value(state0, r0)
    state_soln = np.empty((len(r),len(state0)))
    state_soln[0] = state0
    for i,radius in enumerate(r[1:]):
        state_int.integrate(radius)
        state_soln[i+1] = state_int.y
    Ktt=state_soln[..., 0]
    Krr=(16.0*np.pi*gv.G*rhothree-R-2.0*Ktt*Ktt)/(4.0*Ktt)
    return Ktt, Krr


if __name__ == "__main__":
    r, p, ptilde, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    a=np.insert(a_1, 0, 1.0)

    Ktt, Krr=  findK(r, p, ptilde, m)
    ur=np.zeros_like(r)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    factor=np.ones_like(r)*gv.delta_rhothree_frac*np.tanh(10.0*r)    
    rhothree1=rhothree+factor*rhothree
    
    #pl.plot(r, factor)
    #pl.plot(r, np.log10(rhothree))
    #pl.plot(r, np.log10(rhothree1))
    #pl.show()



    pl.plot(r, Ktt)
    pl.plot(r, Krr)
    pl.show()
