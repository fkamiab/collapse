#!/usr/bin/env python2

"""time_integration.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-07-04 18:04:49 (jmiller)>

This file implements a 4th-order explicit Runge-Kutta integrator.
"""

# ======================================================================
# imports
# ======================================================================
import numpy as np
from scipy import integrate
import matplotlib as mpl
from matplotlib import pyplot as plt
# ======================================================================


# ======================================================================
# global variables
# ======================================================================
CFL_FACTOR = 0.1
# ======================================================================


def get_min_dx(x):
    """
    Returns the minimum distance between grid points in the
    array x
    """
    dx = x[1:] - x[:-1]
    return np.min(dx)

    
def get_dt(x):
    """
    Given a grid x, returns the timestep we should use based
    on the global choice of CFL factor.
    """
    return CFL_FACTOR*get_min_dx(x)


def rk2_step(t,state_vector,dt,right_hand_side,
             post_processor=False):
    """
    A time-step for an RK2 integrator.

    Parameters
    ----------
    -- t, the current time
    -- state_vector, the current state of the system
    -- d/dt (state_vector) = right_hand_side(t,state_vector)
    -- post_processor is a function that post-processes the solution at every
       every substep. It should take the state as input.
    """
    k1 = state_vector + 0.5*dt*right_hand_side(t,state_vector)
    if post_processor:
        k1 = post_processor(k1)
    ynew = state_vector + dt*right_hand_side(t+0.5*dt,k1)
    if post_processor:
        ynew = post_processor(ynew)
    tnew = t+dt
    return tnew,ynew


def rk4_step(t,state_vector,dt,
             right_hand_side,
             post_processor=False):
    """
    A time-step for an RK4 integrator.

    Parameters
    ----------
    -- t, the current time
    -- state_vector, the current state of the system
    -- d/dt (state_vector) = right_hand_side(t,state_vector)
    -- post_processor is a function that post-processes the solution at every
       every substep. It should take the state as input.
    """
    k1 = right_hand_side(t,state_vector)
    if post_processor:
        k1 = post_processor(k1)
    k2 = right_hand_side(t+0.5*dt, state_vector + 0.5*k1*dt)
    if post_processor:
        k2 = post_processor(k2)
    k3 = right_hand_side(t+0.5*dt,state_vector + 0.5*k2*dt)
    if post_processor:
        k3 = post_processor(k3)
    k4 = right_hand_side(t+dt,state_vector+k3*dt)
    if post_processor:
        k4 = post_processor(k4)
    ynew = state_vector + (1.0/6)*dt*(k1+2*k2+2*k3+k4)
    if post_processor:
        ynew = post_processor(ynew)
    tnew = t+dt
    return tnew,ynew


class ode:
    """
    The ode class. Jonah's python-native implementation of the
    scipy.integrate.ode class.
    (see: http://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.ode.html)

    One major difference in interfaces is that this implementation
    demands the user specify the timestep to take in some way.

    usage example:
    y0,t0 = [0,1],0
    def f(t,y):
        return np.array([-y[1],y[0]])
    integrator = ode(f)
    integrator.set_initial_value(y0,t0)
    integrator.set_dt(0.1)
    mydt = 0.2
    while integrator.t < 10:
          integrator.integrate(integrator.t + mydt)
          print "{} {}".format(integrator.t, integrator.y)
    """
    def __init__(self,right_hand_side,step=rk2_step):
        """
        Initializes the integrator to solve the equation
        d/dt (state) = right_hand_side(t,state)

        right_hand_side must take a float, and an array of floats
        and return an array of floats of the same size.
        """
        self.__rhs = right_hand_side
        self.__dt = False
        self.t = False
        self.y = False
        self.__post_processor = False
        self.__step = step

    def set_initial_value(self,y0,t0 = 0.):
        """
        Sets the initial state to y0 and time to t0.
        if t0 is not specified, it is assumed to be zero.
        """
        if type(y0) != np.ndarray:
            y0 = np.array(y0)
        self.t = t0
        self.y = y0

    def set_dt(self,par):
        """
        Sets the time step of the integrator. Accepts either a single
        floating point number or a one-dimensional numpy array.

        If a single float is used, it is assumed to be the desired time step.
        
        If a numpy array is used, it is assumed to be the grid of positions, x,
        and the timestep is chosen via the CFL criterion with
        a conservatively chosen CFL factor.
        """
        if type(par) == np.float64:
            self.__dt = par
        elif type(par) == float or type(par) == int:
            dt = float(par)
            self.__dt = dt
        elif type(par) == np.ndarray:
            x_grid = par
            self.__dt = get_dt(x_grid)
        else:
            print "Type of dt = {}".format(type(par))
            raise TypeError("Not a valid type to set dt.")

    def set_post_processor(self,post_processor):
        """
        A post-processor is a function that maps a 1d-array to a 1d-array.
        It is applied to the state vector at each Runge-Kutta substep.
        This does not need to be set at each time step.
        """
        self.__post_processor = post_processor

    def step(self):
        """
        Integrates the current system in time one step. Updates
        the internal state and time. Raises an error if initial value
        and time step have not been set.
        """
        if not self.__dt:
            raise ValueError("Time step not set!")
        if not self.y:
            print self.y
            print type(self.y)
            raise ValueError("Initial system not set!")
        self.t,self.y = self.__step(self.t,self.y,self.__dt,self.__rhs,
                                    self.__post_processor)

    def integrate(self,t_final):
        """
        Integrates forward in time until the internal time is at least
        as large as t_final.
        """
        while self.t < t_final:
            self.step()
        

if __name__ == "__main__":
    a = 1
    b = 1
    t0 = 0.
    tfinal = 5
    nt = 20
    yanalytic = lambda t: t*t + a*t + b
    rhs = lambda t,y: 2*t + a
    ts = np.linspace(t0,tfinal,nt)
    dt = ts[1]-ts[0]
    ys = np.empty_like(ts)
    y0 = yanalytic(t0)
    ys[0] = y0
    mystep = rk2_step
    for i in range(1,len(ts)):
        tnew,ynew=rk2_step(ts[i-1],ys[i-1],dt,rhs)
        ys[i] = ynew
    ytrues = yanalytic(ts)
    plt.plot(ts,ytrues)
    plt.plot(ts,ys,'ro')
    plt.show()
