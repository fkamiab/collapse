#!/usr/bin/env python

"""evolve.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 22:30:47 (jmiller)>

This file is the main evolution loop for the system with hydro
variables only.
"""

import numpy as np
import global_variables as gv
#import derivative as der
import derivative_mirror as der
#import derivative_mirror_4 as der4
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import rhs
from scipy.integrate import ode
import pylab as pl
import matplotlib as mpl
import initial_velocity as velocity
import plot as plot
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
import Paether as PaetherFinder
import initial_K as initial_K
from scipy import interpolate
#import initial_profile as initial
import initial_profile_NOshooting as initial
import Hamiltonian_Constraint as Hamiltonian


def evolve(r, Sr_tilde0, tau_tilde0, a0, b0, K0):
    """
    The main loop. Evolves system from t0 to tfinal with nt steps
    with initial data given by r,rho0,p0,ur0,a,b,K,alpha.
    """
    # initialize output arrays
    t_array = np.linspace(0.0,gv.TIME,gv.RESTIME)
    soln_array = np.empty((len(t_array),gv.NUM_EVOLVED_VARIABLES,gv.RES))

    # initial state
    U0 = np.vstack((Sr_tilde0,tau_tilde0,a0,b0,K0))
    soln_array[0] = U0
 
   
    U0_flat = utilities.flatten(U0)

    # make right-hand-side
    my_rhs = rhs.make_rhs(r)

    integrator = ode(my_rhs)
    #integrator.set_integrator('dopri5' ,max_step=gv.STEP,first_step=gv.STEP)
    integrator.set_integrator('dopri5' , first_step=gv.STEP)
    integrator.set_initial_value(U0_flat,0.0)

    for i, t in enumerate(t_array[1:]):
        integrator.integrate(t)
        if gv.printme==1:
            print "integrated to {} out of {}.".format(t,gv.TIME)
        soln_array[i+1] = utilities.unflatten(integrator.y,
                                              gv.NUM_EVOLVED_VARIABLES)

        
        Sr_tilde_step=soln_array[i+1,0,...]
        tau_tilde_step=soln_array[i+1,1,...]
        a_step=soln_array[i+1,2,...]
        b_step=soln_array[i+1,3,...]
        K_step=soln_array[i+1,4,...]
        p_step, ur_step, a_step ,b_step=variables.primitive_variables(r, Sr_tilde_step, tau_tilde_step, a_step, b_step)
        Ktt_step, Krr_step = K_solver.giveK(r, p_step, K_step, ur_step, a_step, b_step)
        Paether_step=PaetherFinder.get_Paether(r, p_step, ur_step, Ktt_step, Krr_step, a_step, b_step)
        alpha_step=alphafinder.give_alpha_direct(r, p_step, Paether_step)


        filename="./time_outputs/time_step_{}.out".format(i+1)
        np.savetxt(filename, np.c_[r, p_step, Paether_step, ur_step, alpha_step, a_step, b_step, K_step, Sr_tilde_step, tau_tilde_step], delimiter='\t', header='Time={} and Columns are r\tp\tPaether\tur\talpha\ta\tb\tK\tSr_tilde\ttau_tilde'.format(t))

    Sr_tilde_soln=soln_array[..., 0, ...]
    tau_tilde_soln=soln_array[..., 1, ...]
    a_soln=soln_array[..., 2, ...]
    b_soln=soln_array[..., 3, ...]
    K_soln=soln_array[..., 4, ...]
    
    return t_array, Sr_tilde_soln, tau_tilde_soln, a_soln, b_soln, K_soln






if __name__ == "__main__":


  
    '''SETTING INITIAL PRESSURE PROFILE FOR THE STAR'''

    if(gv.build_initial_profile==1):
        r, p0, Paether0, m0, a0, b0, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)   
        quit()
    
    if(gv.build_initial_profile==0):
        r, p0, Paether0, m0, a0, b0 = np.loadtxt('profile_centralpressure0.012_RES{}_Lambda0.25_truncfactor{}_truncradius{}.out'.format(gv.RES, gv.radius_truncation_factor, gv.RN_factor_truncate), skiprows=1, unpack=True)

        NS_radius=1.33177406643

  

    rho0=tmunu.get_rho(p0)
    sqrt_det=geometry.get_gamma_half(r,a0,b0)   
    ur0=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, gv.ur_order)
    
    K0=np.ones_like(r)*gv.ur_order
    Ktt0, Krr0 = K_solver.giveK(r, p0, K0, ur0, a0, b0)
    
    Sr_tilde0, tau_tilde0, a0, b0= variables.hydro_variables(r, p0, ur0, a0, b0)
    alpha0=alphafinder.give_alpha_direct(r, p0, Paether0)
    filename="./time_outputs/time_step_{}.out".format(0)
    np.savetxt(filename, np.c_[r, p0, Paether0, ur0, alpha0, a0, b0, K0, Sr_tilde0, tau_tilde0], delimiter='\t', header='Time={} and Columns are r\tp\tPaether\tur\talpha\ta\tb\tK\tSr_tilde\ttau_tilde'.format(0))               
    t_array, Sr_tilde_soln, tau_tilde_soln, a_soln, b_soln, K_soln = evolve(r, Sr_tilde0, tau_tilde0, a0, b0, K0)



