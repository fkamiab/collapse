import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math


def giveKinitial(r, p, Paether, ur, a, b):
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    drhodp= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
    dpdrho=1.0/drhodp
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    sqrt_detprime=der.first(sqrt_det,r)
    pprime=der.first(p,r)
    rhoprime=der.first(rho(p),r)
    urup=ur/a/a
    ratio=urup[1:]/sqrt_det[1:]
    new_ratio=np.insert(ratio, 0, 0.0)
    A=gv.Lambda*(3.0*dpdrho[1:]-1.0)*(-(rho(p)[1:]+p[1:])*sqrt_detprime[1:]*urup[1:]/sqrt_det[1:]+(pprime[1:]-rhoprime[1:])*urup[1:]-(rho(p)[1:]+p[1:])*der.first(urup,r)[1:])
    B=Paether[1:]-gv.Lambda*(3.0*dpdrho[1:]-1.0)*(rho(p)[1:]+p[1:])
    K=A/B
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*K[0]-q20*K[1]-q30*K[2]-q40*K[3]-q50*K[4])/q00
    new_K=np.insert(K, 0, first_element)
    K=new_K

    D=-4.0*np.pi*gv.G*(rho(p)+p)*ur
    def residual(Ktt, r, D, K):
        res=np.empty_like(r)
        res=r*der.first(Ktt,r)-D*r+(3.0*Ktt-K)
        res[0]=der.first(Ktt,r)[0]
        return res
    Ktt0=np.ones_like(r)
    sol = optimize.root(lambda Ktt: residual(Ktt, r, D, K), Ktt0)
    Ktt=sol.x
    Krr=K-2.0*Ktt
    return Ktt, Krr

if __name__ == "__main__":


    #r, p, Paether, m, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)
    r, p, Paether, m = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25.out', skiprows=1, unpack=True)
    NS_radius=1.33478413497
    
    '''
    p_inter=interpolate.interp1d(r, p, kind='cubic')
    Paether_inter=interpolate.interp1d(r, Paether, kind='cubic')
    m_inter=interpolate.interp1d(r, m, kind='cubic')
    r=np.linspace(0.0, NS_radius, gv.RES)
    p=p_inter(r)
    Paether=Paether_inter(r)
    m=m_inter(r)'''
    
     
    ur_mu=NS_radius*(1.0-0.7)
    ur_sigma=gv.ur_sigma
    ur_a=1.0/ur_sigma/np.sqrt(2.0*np.pi)
    ur_b=ur_mu
    ur_c = ur_sigma
    ur_order=gv.ur_order
    ur=-ur_order*np.ones_like(r)*(ur_a*np.exp(-(r-ur_b)*(r-ur_b)/2.0/ur_c/ur_c))*r*r/NS_radius/NS_radius

    ur=-ur_order*np.ones_like(r)*(ur_a*np.exp(-(r-ur_b)*(r-ur_b)/2.0/ur_c/ur_c))*(np.cos(11.0*np.pi*r/2.0/NS_radius))*r*r/NS_radius/NS_radius

    
 

    a_1= np.sqrt(1.0/(1.0-2.0*m[1:]/r[1:]))
    b=np.zeros_like(r)+1.0
    a=np.insert(a_1, 0, 1.0)




    i_R=np.int(NS_radius/np.diff(r)[0] )
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    drhodp= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
    dpdrho=1.0/drhodp
    Sr=(rho(p)+p)*ur*W
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    aprime=der.first(a,r)
    bprime=der.first(b,r)
    bdoubleprime=der.second(b,r)
    R_1= -(2.0/(r[1:]*r[1:]*a[1:]*a[1:]*a[1:]*b[1:]*b[1:]))*(-2.0*r[1:]*b[1:]*aprime[1:]*(r[1:]*bprime[1:]+b[1:])+a[1:]*(r[1:]*r[1:]*bprime[1:]*bprime[1:] +2.0*r[1:]*b[1:]*(r[1:]*bdoubleprime[1:] +3.0*bprime[1:]) +b[1:]*b[1:]) -a[1:]*a[1:]*a[1:])
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*R_1[0]-q20*R_1[1]-q30*R_1[2]-q40*R_1[3]-q50*R_1[4])/q00
    R=np.insert(R_1, 0, first_element)
    ohr=16.0*np.pi*gv.G*rhothree
    R=ohr
    sump=rho(p)+p
    urup=ur/a/a
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    sqrt_detprime=der.first(sqrt_det,r)
    pprime=der.first(p,r)
    rhoprime=der.first(rho(p),r)
    


    ratio=urup[1:]/sqrt_det[1:]
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*ratio[0]-q20*ratio[1]-q30*ratio[2]-q40*ratio[3]-q50*ratio[4])/q00
    new_ratio=np.insert(ratio, 0, first_element)

    A=gv.Lambda*(3.0*dpdrho-1.0)*(-(rho(p)+p)*sqrt_detprime*new_ratio+(pprime-rhoprime)*urup-(rho(p)+p)*der.first(urup,r))
    B=Paether-gv.Lambda*(3.0*dpdrho-1.0)*(rho(p)+p)
    K=A/B


    D=-4.0*np.pi*gv.G*(rho(p)+p)*ur
    #pl.plot(r, A/B)
    #pl.plot(r, B)
    #pl.show()
    
    def residual(Ktt, r, D, K):
        res=np.empty_like(r)
        res=r*der.first(Ktt,r)-D*r+(3.0*Ktt-K)
        res[0]=der.first(Ktt,r)[0]
        return res
    Ktt0=np.ones_like(r)
    sol = optimize.root(lambda Ktt: residual(Ktt, r, D, K), Ktt0)
    Ktt=sol.x
    Krr=K-2.0*Ktt
    pl.plot(r, Ktt)
    pl.plot(r, Krr)
    pl.show()
    

    
    #pl.plot(r, ur)
    #pl.plot(r,A/(Paether[0]))
    #pl.show()
