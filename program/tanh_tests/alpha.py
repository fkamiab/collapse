import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative_mirror as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import optimize
from scipy import interpolate
import math

LAST_ALPHA=None


def give_alpha_direct(r, p, Paether):
    global LAST_ALPHA
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    pprime=der.first(p, r, der.EVEN)
    rhoprime=der.first(rho(p),r, der.EVEN)
    Paetherprime=der.first(Paether, r, der.EVEN)
    def residual(r, alpha, Paether, Paetherprime, pprime, rhoprime):
        res= der.first(alpha,r, der.EVEN)*Paether +Paetherprime*alpha-gv.Lambda*alpha*(3.0*pprime-rhoprime)
        #res= der.first(alpha*Paether,r)-gv.Lambda*alpha*(3.0*pprime-rhoprime)
        alpha_squared=alpha*alpha
        #res[-1]=der.first(alpha_squared, r)[-1]- (1.0-alpha_squared[-1] )/r[-1]
        #res[-1]=alpha_squared[-1]-(1.0-2.0*0.188869961957/1.33478413497)
        #res[0]= der.first(alpha,r, der.EVEN)[0]
        return res

    if LAST_ALPHA==None:
        alpha0=np.ones_like(r)
    else:
        alpha0=LAST_ALPHA
    
    sol = optimize.root(lambda alpha: residual(r, alpha, Paether, Paetherprime, pprime, rhoprime), alpha0, tol=1e-9)
    alpha=sol.x
    correct_alpha=np.sqrt(1.0-2.0*0.188869961957/1.33478413497)
    alpha=alpha*correct_alpha/alpha[-1]
    LAST_ALPHA=alpha*np.ones_like(alpha)
    #alpha=der.set_to_zero_at_origin(alpha,r)
    return alpha




def give_alpha_integrate(r, p, Paether):
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    pprime=der.first(p, r, der.EVEN)
    rhoprime=der.first(rho(p),r, der.EVEN)
    Paetherprime=der.first(Paether, r, der.EVEN)
    pprime_inter= interpolate.interp1d(r, pprime, kind=6)
    rhoprime_inter= interpolate.interp1d(r, rhoprime, kind=6)
    Paetherprime_inter= interpolate.interp1d(r, Paetherprime, kind=6)
    Paether_inter= interpolate.interp1d(r, Paether, kind=6)   
    
    
    #r0=0.0
    r0=r[0]
    Z0=np.log(0.4)
    state0=np.array([Z0])
    state=state0    
    def f(radius, state):
        #if(radius==0):
            #fZ=0.0
        #else:
        fZ=(gv.Lambda*(3.0*pprime_inter(radius)-rhoprime_inter(radius))-Paetherprime_inter(radius))/Paether_inter(radius)
        
        return np.array([fZ])
    state_int = ode(f)
    state_int.set_integrator('dop853',max_step=0.5*(r[1]-r[0]),
                             first_step=1E-10,dfactor=1E-4)
    state_int.set_initial_value(state0, r0)
    state_soln = np.empty((len(r),len(state0)))
    state_soln[0] = state0
    for i,radius in enumerate(r[1:]):
        state_int.integrate(radius)
        state_soln[i+1] = state_int.y
    Z=state_soln[..., 0]
    alpha=np.exp(Z)
    #correct_alpha=1.0/(np.sqrt(np.abs((2.0*r[-1]/Paether[-1])*(gv.Lambda*(3.0*pprime[-1]-rhoprime[-1])-Paetherprime[-1])+1.0)))
    correct_alpha=np.sqrt(1.0-2.0*0.188869961957/1.33478413497)
    alpha=alpha*correct_alpha/alpha[-1]


    
    return alpha

