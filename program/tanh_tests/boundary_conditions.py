import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math
import initial_velocity as velocity
import initial_K as initial_K
import variable_change as var
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver


def central_derzero(x):
    y=x[1:]    
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    first_element= (-q10*y[0]-q20*y[1]-q30*y[2]-q40*y[3]-q50*y[4])/q00
    x2=np.insert(y, 0, first_element)
    return x2
 
def outer_derzero(x):
    y=x[:-1]    
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    last_element= (q10*y[-1]+q20*y[-2]+q30*y[-3]+q40*y[-4]+q50*y[-5])/(-q00)
    x2=np.append(y, last_element)
    return x2
 
