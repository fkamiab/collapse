#!/usr/bin/env python


import numpy as np
import pylab as pl
from copy import copy
from scipy import interpolate #import KroghInterpolator BarycentricInterpolator



EVEN=True
ODD=False
order = 4
TheInterpolator=interpolate.KroghInterpolator


def first(f,x,parity):
    """
    Calculates the first derivative of a function f with respect to x,
    requires parity p, which must be even or odd.
    """
    dx = x[1]-x[0]
    f_copy = np.empty(len(f)+order)
    f_copy[order/2:-order/2] = f[:]
    if parity == EVEN:
        f_copy[:order/2] = np.array([i for i in reversed(f[0:order/2])])
    else:
        f_copy[:order/2] = np.array([i for i in reversed(-f[0:order/2])])
    # extrapolation B.C. are really really bad.
    my_interp = TheInterpolator(x[-2*order:],f[-2*order:])
    f_copy[-order/2:] = my_interp(np.array([x[-1]+(i+1)*dx for i in range(order/2)]))
    out = np.empty_like(f)
    if order == 2:
        out = (f_copy[2:] - f_copy[:-2])/(2*dx)
    elif order == 4:
        out = (-f_copy[4:] + 8*f_copy[3:-1] - 8*f_copy[1:-3] + f_copy[:-4])/(12*dx)
    else: 
        raise ValueError("Only 2nd order and 4th order derivatives defined.")
    #if order == 4:
    #    out[-2] = (3*f[-1] + 10*f[-2] - 18*f[-3] + 6*f[-4] - f[-5])/(12*dx)
        #out[-1] = out[-2]
    x_extended = np.empty(len(x)+order)
    x_extended[order/2:-order/2] = x[:]
    x_extended[:order/2] = np.array([i for i in reversed([x[0]-(i+1)*dx for i in range(order/2)])])
    x_extended[-order/2:] = np.array([x[-1]+(i+1)*dx for i in range(order/2)])
    #if parity == EVEN:
        #out[0] = 0.
    return out#,x_extended,f_copy



def second(f,x,parity):
    """
    Calculates the second derivative of a function f with respect to x,
    requires parity p, which must be even or odd.
    """
    dx = x[1]-x[0]
    f_copy = np.empty(len(f)+order)
    f_copy[order/2:-order/2] = f[:]
    if parity == EVEN:
        f_copy[:order/2] = np.array([i for i in reversed(f[1:order/2+1])])
    else:
        f_copy[:order/2] = np.array([i for i in reversed(-f[1:order/2+1])])
    # extrapolation B.C. are really really bad.
    my_interp = TheInterpolator(x[-order-1:],f[-order-1:])
    f_copy[-order/2:] = my_interp(np.array([x[-1]+(i+1)*dx for i in range(order/2)]))
    out = np.empty_like(f)
    if order == 4:
        out = (-f_copy[4:] + 16*f_copy[3:-1] - 30*f_copy[2:-2]+ 16*f_copy[1:-3] - f_copy[:-4])/(12*dx*dx)
    else: 
        raise ValueError("Only 4th order derivatives defined.")
    #if order == 4:
    #    out[-2] = (3*f[-1] + 10*f[-2] - 18*f[-3] + 6*f[-4] - f[-5])/(12*dx)
        #out[-1] = out[-2]
    x_extended = np.empty(len(x)+order)
    x_extended[order/2:-order/2] = x[:]
    x_extended[:order/2] = np.array([i for i in reversed([x[0]-(i+1)*dx for i in range(order/2)])])
    x_extended[-order/2:] = np.array([x[-1]+(i+1)*dx for i in range(order/2)])
    return out#,x_extended,f_copy


def set_to_zero_at_origin(f,x):
    """
    Sets the value of an even function at the origin,
    assuming that function to be a 4th-order polynomial.
    """
    out = copy(f)
    x_interp = np.concatenate((np.array([-i for i in reversed(x[1:order/2+2])]),
                               x[1:order/2+2]))
    f_interp = np.concatenate((np.array([i for i in reversed(f[1:order/2+2])]),
                               f[1:order/2+2]))
    my_interp = TheInterpolator(x_interp,f_interp)
    out[0] = my_interp(x[0])
    return out
