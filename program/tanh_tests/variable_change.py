#!/usr/bin/env python

"""variable_change.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-17 14:38:05 (jmiller)>

This file defines variable transformations between conserved and
primitive variables and related quantities.
"""

import pylab as pl
import numpy as np
import global_variables as gv
import derivative_mirror as der
from scipy import optimize
import geometry
import energy_momentum as tmunu
import utilities
import plot


# ======================================================================
# Get conserved from primitive
# ======================================================================
def hydro_variables(r, p, ur, a, b, densitized=True):
    "Finds conserved variables from primitive ones"
    W=tmunu.get_w(ur,a)
    Gamma=gv.Gamma
    sqrt_det = geometry.get_gamma_half(r,a,b)
    rho=tmunu.get_rho(p)
    rho_matter=tmunu.get_rho0(p)
    Sr_tilde=(rho+p)*W*ur
    tau_tilde= (rho+p)*W*W-p-W*rho_matter
    if densitized:
        Sr_tilde *= sqrt_det
        tau_tilde *= sqrt_det
    return Sr_tilde, tau_tilde, a, b
# ======================================================================


# ======================================================================
# Get primitive from conserved
# ======================================================================
def primitive_variables(r, Sr_tilde, tau_tilde, a, b):
    rho = np.empty_like(r)
    p = np.empty_like(r)
    ur = np.empty_like(r)
    p0 = 1e-3
    ur0 = 1e-3
    gamma_half = geometry.get_gamma_half(r,a,b)
    Sr = np.zeros_like(Sr_tilde)
    tau = np.zeros_like(tau_tilde)
    #Sr[1:] = Sr_tilde[1:]/gamma_half[1:]
    #tau[1:] = tau_tilde[1:]/gamma_half[1:]
    #tau[0]=der.second(tau_tilde, r, der.EVEN)[0]/der.second(gamma_half, r, der.EVEN)[0]
    #tau=der.set_to_zero_at_origin(tau,r)
    
    Sr = Sr_tilde/gamma_half
    tau = tau_tilde/gamma_half
    
    
    #pl.plot(r,D)
    #pl.plot(r,Sr)
    #pl.plot(r,Sr,'ro')
    #pl.plot(r,tau)
    #pl.show()
    
    for i in range(1,len(r)+1):
        if i > 1:
            p0 = p[-(i-1)]
            ur0 = ur[-(i-1)]
        def f(data):
            p_value = data[0]
            ur_value = data[1]
            Sr_value,tau_value,abad,bbad \
                = hydro_variables(r[-i],
                               p_value,
                               ur_value,
                               a[-i],b[-i],
                               densitized = False)
            Sr_tilde_residual = 1e2*(Sr[-i] - Sr_value)
            tau_tilde_residual = 1e2*(tau[-i] - tau_value)
            return np.array([Sr_tilde_residual,
                             tau_tilde_residual])
        sol = optimize.root(f,np.array([p0,ur0]),
                            method='lm',
                            tol=1e-10,
                            options={'maxiter':2000})
        if sol.success:
            p_sol,ur_sol=np.split(sol.x,2)
        else:
            print "solver failed!"
            print "Failure at radius {}".format(r[i])
            print "Status = {}".format(sol.status)
            print sol.message
            print sol.fun
            raise ValueError("Solver failed!")
        p[-i] = p_sol
        ur[-i] = ur_sol
    
    #p = der.set_to_zero_at_origin(p)
    #ur[0] = 0
    return p,ur,a,b

