import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
from scipy.optimize import bisect
from scipy.optimize import newton
from scipy.interpolate import UnivariateSpline
from scipy.integrate import ode
import global_variables as gv
import derivative_4_2 as der42
import derivative_mirror as der
from scipy import interpolate
from scipy import optimize




def create_file(filename):
    "Creates a new file with filename"
    with open(filename,'w') as f:
        f.write('# pcentral\tM\tR\n')

def save_row(p, R,M,filename):
    "Appends R and M to a file with filename"
    with open(filename,"a") as f:
        f.write("{} {} {}\n".format(p, R,M))    


def give_ptilde(p):
    if(p==0.012):
        return 0.0130006807815
    else:
        p_array=np.linspace(0.0, 1.5*p, gv.ptilde_RES)
        def rho(p_value):
            return np.power(p_value/gv.K, 1.0/gv.Gamma)+p_value/(gv.Gamma-1.0)
        def residual(p_value, ptilde_value):
            res=np.empty_like(p_value)
            res=der42.first(ptilde_value, p_value)*(rho(p_value)+p_value) - rho(p_value)- 3.0*gv.Lambda*p_value/(1.0-gv.Lambda) - ptilde_value
            res[0]=ptilde_value[0]
            return res
        ptilde_guess=np.zeros_like(p_array)
        sol = optimize.root(lambda ptilde: residual(p_array , ptilde), ptilde_guess)
        ptilde_array=sol.x
        ptilde_inter= interpolate.interp1d(p_array, ptilde_array, kind=1)
        return ptilde_inter(p)
    


def profile(p0, ptilde0, RN, radius_factor, RES):    
    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p
    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            if gv.flag_trunc==0:
                fp= (-(p+rho(p))*tov/r/r)
                fptilde=(-(ptilde+rhotilde(p))*tov/r/r)
            if gv.flag_trunc==1:
                fp= (-(p+rho(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN*gv.RN_factor_truncate*RN-r*r)))
                fptilde=(-(ptilde+rhotilde(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN*gv.RN_factor_truncate*RN-r*r)))
            print 'r={}\t\tp={}'.format(r,p)
            fm=4.0*np.pi*r*r*rhotilde(p)
   
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dop853',first_step=1E-10, max_step=1.5*1e-5, dfactor=1E-5, nsteps=1e8)
    state_int.set_integrator('dop853',first_step=1E-10, max_step=1e-4)
    state_int.set_initial_value(state0, r0)
    #r_array=np.linspace(0.0, radius_factor*RN, RES)
    #RMAX=radius_factor*RN
    RMAX=RN*radius_factor
    r_array=np.linspace(RMAX/(2.0*gv.RES-1.0), RMAX, RES)
    state_soln = np.empty((len(r_array),len(state0)))
    #state_soln[0] = state0
    for i,r in enumerate(r_array):
        if not state_int.successful():
            raise ValueError("Integrator failed.")
        state_int.integrate(r)
        state_soln[i] = state_int.y
    return r_array, state_soln


def pressure(p0, ptilde0, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p
    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_integrator('dop853',first_step=1E-10, max_step=1e-2)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[0]


def give_initial_profile(p0, radius_factor, RES):
    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
    ptilde0_correct= give_ptilde(p0)
    RN_trial=2.0
    if p0==0.012:
        Radius_correct=1.33177406643
    else:
        Radius_correct=bisect(lambda r: pressure(p0,ptilde0_correct, r)-gv.pressure_ratio_truncate*p0, 1, RN_trial) 
    r, state_soln=profile(p0, ptilde0_correct, Radius_correct, radius_factor, RES)
    p=state_soln[..., 0]
    ptilde=state_soln[..., 1]
    m=state_soln[..., 2]
    Paether=(1.0-gv.Lambda)*ptilde-(1.0-3.0*gv.Lambda)*p-gv.Lambda*rho(p)
    a=np.sqrt(1.0/(1.0-2.0*m/r))
    b=np.ones_like(a)
    filename="profile_centralpressure{}_RES{}_Lambda{}_truncfactor{}_truncradius{}.out".format(p0, RES, gv.Lambda, gv.radius_truncation_factor, gv.RN_factor_truncate)
    print "Printing {} \n".format(filename)
    np.savetxt(filename, np.c_[r, p, Paether, m, a, b], delimiter='\t', header='Mass={} and Radius={} and Columns are \tr\tp\tPaether\tm\ta\tb'.format(state_soln[len(r)-1, 2], Radius_correct))     
    return r, p, Paether, m, a, b, Radius_correct
 







if __name__ == "__main__":
    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))

    ptilde0_correct= give_ptilde(gv.p0)
    RN_trial=2.0
    if gv.p0==0.012:
        Radius_correct=1.33177406643
    else:
        Radius_correct=bisect(lambda r: pressure(gv.p0,ptilde0_correct, r)-gv.pressure_ratio_truncate*gv.p0, 1, RN_trial) 
    r, state_soln=profile(gv.p0, ptilde0_correct, Radius_correct, gv.radius_factor, gv.RES)
    p=state_soln[..., 0]
    ptilde=state_soln[..., 1]
    m=state_soln[..., 2]
    Paether=(1.0-gv.Lambda)*ptilde-(1.0-3.0*gv.Lambda)*p-gv.Lambda*rho(p)

    a=np.sqrt(1.0/(1.0-2.0*m/r))
    R= -(2.0/(r*r*a*a*a))*(-2.0*r*der.first(a,r, der.EVEN) + a- a*a*a)

    W=1.0
    Gamma=gv.Gamma


    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree

    

    pl.plot(r, np.log10(p))
    pl.plot(r, np.log10(ptilde))
    pl.show()
        

    pl.plot(r, R)
    pl.plot(r, ohr)
    pl.show()

    pl.plot(r, (R-ohr)/ohr)
    pl.show()
   
