import pylab as pl
import numpy as np
import plot
import global_variables as gv
import initial_profile as initial
import derivative_mirror as der
from scipy.optimize import bisect
from scipy.integrate import ode
from scipy import interpolate
from scipy import optimize
import math
import initial_velocity as vel
import initial_K as Kfirst


def giveHamiltonian(r, a, b, p, ur, Ktt, Krr):
    aprime=der.first(a,r, der.EVEN)
    bprime=der.first(b,r, der.EVEN)
    bdoubleprime=der.second(b,r, der.EVEN)
    R= -(2.0/(r*r*a*a*a*b*b))*(-2.0*r*b*aprime*(r*bprime+b)+a*(r*r*bprime*bprime +2.0*r*b*(r*bdoubleprime +3.0*bprime) +b*b) -a*a*a)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    hamilton=ohr-R-4.0*Krr*Ktt-2.0*Ktt*Ktt
    hamilton1=-4.0*Krr*Ktt-2.0*Ktt*Ktt
    hamilton2=ohr-R
    return np.abs(hamilton2)/ohr, np.abs(hamilton)/ohr


#if __name__ == "__main__":



    
