#!/usr/bin/env python

"""
derivative.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 21:14:49 (jmiller)>

This program defines the derivative operators used in the neutron star
collapse code. We use fourth-order finite differences with summation
by parts, as defined here:
http://dx.doi.org/10.1007/s10915-006-9123-7
"""

import pylab as pl
import numpy as np
from copy import copy

# define the global stencil constants
q00 = -2.09329763466349871588733
q10 = 4.0398572053206615302160
q20 = -3.0597858079809922953240
q30 = 1.37319053865399486354933
q40 = -0.25996430133016538255400
q50 = 0.0
q60= 0.0

q01 = -0.31641585285940445272297
q11 = -0.53930788973980422327388
q21 = 0.98517732028644343383297
q31 = -0.05264665989297578146709
q41 = -0.113807251750624235013258
q51 = 0.039879767889849911803103
q61= -0.0028794339334846531588787

q02 = 0.13026916185021164524452
q12 = -0.87966858995059249256890
q22 =  0.38609640961100070000134
q32 = 0.31358369072435588745988
q42 = 0.085318941913678384633511
q52 = -0.039046615792734640274641
q62 = 0.0034470016440805155042908

q03 = -0.01724512193824647912172
q13 = 0.16272288227127504381134
q23 = -0.81349810248648813029217
q33 = 0.13833269266479833215645
q43 = 0.59743854328548053399616
q53 = -0.066026434346299887619324
q63 = -0.0017244594505194129307249

q04 = -0.00883569468552192965061
q14 = 0.03056074759203203857284
q24 = 0.05021168274530854232278
q34 = -0.66307364652444929534068
q44 = 0.014878787464005191116088
q54 = 0.65882706381707471953820
q64 = -0.082568940408449266558615

def first(f, r):
    """
    Take the first derivative of array f with respect to array r.
    """
    h=np.diff(r)[1]
    der_f=np.zeros(len(r))   
    Q=f[2:]-f[:-2]
    Q=np.lib.pad(Q, (1,1), 'edge')
    W=f[4:]-f[:-4]
    W=np.lib.pad(W, (2,2), 'edge')
    der_f=(1.0/h)*((2.0/3.0)*Q - (1.0/12.0)*W)
    
    der_f[0]=(1.0/h)*(q00*f[0]+q10*f[1]+q20*f[2]+q30*f[3]+q40*f[4]+q50*f[5] +q60*f[6])
    der_f[len(r)-1]=(1.0/h)*(-q00*f[len(r)-1]-q10*f[len(r)-2]-q20*f[len(r)-3]
                              -q30*f[len(r)-4]-q40*f[len(r)-5]-q50*f[len(r)-6]
                              -q60*f[len(r)-7])

    der_f[1]=(1.0/h)*(q01*f[0]+q11*f[1]+q21*f[2]+q31*f[3]+q41*f[4]+q51*f[5]+q61*f[6])
    der_f[len(r)-2]=(1.0/h)*(-q01*f[len(r)-1]-q11*f[len(r)-2]-q21*f[len(r)-3]
                             -q31*f[len(r)-4]-q41*f[len(r)-5]-q51*f[len(r)-6]-q61*f[len(r)-7])

    der_f[2]=(1.0/h)*(q02*f[0]+q12*f[1]+q22*f[2]+q32*f[3]+q42*f[4]+q52*f[5]+q62*f[6])
    der_f[len(r)-3]=(1.0/h)*(-q02*f[len(r)-1]-q12*f[len(r)-2]-q22*f[len(r)-3]
                             -q32*f[len(r)-4]-q42*f[len(r)-5]-q52*f[len(r)-6]-q62*f[len(r)-7])

    der_f[3]=(1.0/h)*(q03*f[0]+q13*f[1]+q23*f[2]+q33*f[3]+q43*f[4]+q53*f[5]
                      +q63*f[6])
    der_f[len(r)-4]=(1.0/h)*(-q03*f[len(r)-1]-q13*f[len(r)-2]-q23*f[len(r)-3]
                             -q33*f[len(r)-4]-q43*f[len(r)-5]-q53*f[len(r)-6]
                              -q63*f[len(r)-7])

    der_f[4]=(1.0/h)*(q04*f[0]+q14*f[1]+q24*f[2]+q34*f[3]+q44*f[4]+q54*f[5]
                      +q64*f[6])
    der_f[len(r)-5]=(1.0/h)*(-q04*f[len(r)-1]-q14*f[len(r)-2]-q24*f[len(r)-3]
                             -q34*f[len(r)-4]-q44*f[len(r)-5]-q54*f[len(r)-6]
                              -q64*f[len(r)-7])
  



    return der_f

def second(f, r):
    """
    Take the second derivative of array f with respect to array r.
    """
    dr = r[1]-r[0]
    d2f = np.zeros_like(r)
    d2f[1:-1] = f[0:-2] - 2*f[1:-1] + f[2:]
    d2f[0] = f[0] - 2*f[1] + f[2]
    d2f[-1] = f[-1] - 2*f[-2] + f[-3]
    d2f /= (dr*dr)
    return d2f

def set_to_zero_at_origin(a):
    """
    Set the zeroth element of array a so that a[0] has
    vanishing derivative.

    Not in-place. Returns b, which is a copy of a
    but with the c
    """
    b = copy(a)
    b[0] = (-q10*a[1+0]-q20*a[1+1]-q30*a[1+2]-q40*a[1+3]-q50*a[1+4]-q60*a[1+5])/q00
    return b
    
if __name__ == "__main__":
    RES=10
    r=np.linspace(-10, 10, RES)
    def f(r):
        return r*r
    def g(r):
        return 2.0*r
    def k(r):
        return 2.0*np.ones_like(r)

    pl.plot(r, first(f(r),r))
    pl.plot(r, g(r), 'bo')
    pl.show()
    print der(f,r) - g(r)
    pl.semilogy(r, first(f,r) - g(r))
    pl.show()



