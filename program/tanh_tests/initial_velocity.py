import pylab as pl
import numpy as np
import plot
import global_variables as gv
import derivative_mirror as der
import derivative as der0
from scipy import interpolate
from scipy.integrate import ode
from scipy import optimize


def velocity(r, p, Paether, a, b, NS_radius, ur_order):
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    sqrt_detprime=der.first(sqrt_det,r, der.EVEN)
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    drhodp= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
    dpdrho=1.0/drhodp
    pprime=der.first(p,r, der.EVEN)
    rhoprime=der.first(rho(p), r, der.EVEN)
    xx=ur_order*(Paether-gv.Lambda*(3.0*dpdrho-1.0)*(rho(p)+p))/(gv.Lambda*(3.0*dpdrho-1.0))

    pprime_inter= interpolate.interp1d(r, pprime, kind=6)
    rhoprime_inter= interpolate.interp1d(r, rhoprime, kind=6)
    p_inter= interpolate.interp1d(r, p, kind=6)
    sqrt_det_inter= interpolate.interp1d(r, sqrt_det, kind=6)
    sqrt_detprime_inter= interpolate.interp1d(r, sqrt_detprime, kind=6)
    xx_inter= interpolate.interp1d(r, xx, kind=6)

    #Z is the densitized version of the velocity (urup*sqrt_det)
    Z0=0.0
    #Z0=(-xx[0]*sqrt_det[0])*np.diff(r)[0]/(rho(p[0])+p[0])
    #Z0=-r[0]*sqrt_det[0]/a[0]/a[0]
    #Z0=0.0
    state0=np.array([Z0])
    state=state0    
    #r0=0.0
    r0=r[0]
    print r0, Z0
    def f(radius, state):
        #if(radius==0):
            #fZ=0.0
        #else:
        Z=state[0]
        fZ=((pprime_inter(radius)-rhoprime_inter(radius))*Z-xx_inter(radius)*sqrt_det_inter(radius))/(rho(p_inter(radius))+p_inter(radius))
        return np.array([fZ])
    state_int = ode(f)
    state_int.set_integrator('dop853',max_step=0.5*(r[1]-r[0]),first_step=1E-7)
    state_int.set_initial_value(state0, r0)
    state_soln = np.empty((len(r),len(state0)))
    state_soln[0] = state0
    for i,radius in enumerate(r[1:]):
        state_int.integrate(radius)
        state_soln[i+1] = state_int.y
    Z=state_soln[..., 0]
    #ratio=Z[1:]/sqrt_det[1:]
    #new_ratio=np.insert(ratio, 0, 0.0)
    #ur=new_ratio*a*a

    ur=Z*a*a/sqrt_det


    #plot.connectpoints(r, der.first((pprime_inter(r)-rhoprime_inter(r))/(rho(p_inter(r))+p_inter(r)),r))
    #plot.connectpoints(r, der.second((pprime-rhoprime)/(rho(p)+p),r))
    #plot.connectpoints(r, der.second((pprime-rhoprime),r))

    '''tck=interpolate.splrep(r[4:], ur[4:], s=0)
    ur2=interpolate.splev(r, tck)
    pl.plot(r, der.second(ur2,r))
    pl.plot(r, der.second(ur,r), 'ro')
    pl.show()'''
    #pl.plot(r, der.second((pprime),r), 'ro')
    #pl.plot(r, der3.first((pprime),r))
    #pl.plot(r, der0.second((pprime),r))
    
    #pl.show()


    #plot.connectpoints(r, der.second((xx_inter(r)*sqrt_det_inter(r))/(rho(p_inter(r))+p_inter(r)),r))
    #quit()

    '''pl.plot(r, der.second(ur,r), 'ro')
    
    tck=interpolate.splrep(np.insert(r[4:-4], 0,0.0), np.insert(ur[4:-4],0, 0,0), k=5)
    ur=interpolate.splev(r, tck)
    
    pl.plot(r, der.second(ur,r))
    pl.show()'''

    #plot.connectpoints(r, ur)
    #plot.connectpoints(r, der.first(ur, r, der.ODD))
    #plot.connectpoints(r, der.second(ur, r, der.ODD))
    

    #pl.plot(r, der.first(der.second(ur, r, der.ODD), r, der.ODD))
    #pl.plot(r, der0.first(der0.second(ur,r),r), 'ro')
    #pl.show()
    #quit()
    
   
    return ur




def giveur_direct(r, p, Paether, a, b, NS_radius, ur_order):
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    sqrt_detprime=der.first(sqrt_det,r, der.EVEN)
    ratio=sqrt_detprime/sqrt_det
    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    drhodp= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
    dpdrho=1.0/drhodp
    pprime=der.first(p,r, der.EVEN)
    rhoprime=der.first(rho(p), r, der.EVEN)
    xx=ur_order*(Paether-gv.Lambda*(3.0*dpdrho-1.0)*(rho(p)+p))/(gv.Lambda*(3.0*dpdrho-1.0))
    

    def residual(Z, pprime, rhoprime, xx, sqrt_det, p):
        res=np.empty_like(r)
        res= der.first(Z,r, der.ODD)-((pprime-rhoprime)*Z-xx)/(rho(p)+p)  + ratio*Z      
        return res
    Z0=np.ones_like(r)
    sol = optimize.root(lambda Z: residual(Z, pprime, rhoprime, xx, sqrt_det, p), Z0)
    Z=sol.x
    #ur=Z*a*a/sqrt_det
    ur=Z*a*a
    
    
    return ur






if __name__ == "__main__":
    r=np.linspace(0, 4.0, 200)
    NS_radius=1.1
    #plot.connect(r, velocity(r, NS_radius))
    #print velocity(r, NS_radius)
    
