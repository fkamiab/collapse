import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
from scipy.optimize import bisect
from scipy.optimize import newton
from scipy.interpolate import UnivariateSpline
from scipy.integrate import ode
import global_variables as gv
import derivative_mirror as der
from scipy import interpolate

flag_plot=gv.flag_plot
flag_trunc=gv.flag_trunc


def create_file(filename):
    "Creates a new file with filename"
    with open(filename,'w') as f:
        f.write('# pcentral\tM\tR\n')

def save_row(p, R,M,filename):
    "Appends R and M to a file with filename"
    with open(filename,"a") as f:
        f.write("{} {} {}\n".format(p, R,M))


def PTILDE(p0, ptilde0, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            p=np.abs(p)
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
            #print 'r={} p={} ptilde={}\n'.format(r, p, ptilde)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_integrator('dop853',first_step=1E-10, max_step=1e-2, nsteps=1e8)
    ###state_int.set_initial_value(state0, r0)


    state_int.set_initial_value(state0, r0)
    #r_array=np.linspace(0.0, radius_factor*RN, gv.RES)
    RMAX=RN
    RES=gv.RES
    r_array=np.linspace(RMAX/(2.0*RES-1.0), RMAX, RES)
    state_soln = np.empty((len(r_array),len(state0)))
    #state_soln[0] = state0
    for i,r in enumerate(r_array):
        if not state_int.successful():
            raise ValueError("Integrator failed.")
        state_int.integrate(r)
        state_soln[i] = state_int.y
   
    p=state_soln[..., 0]
    ptilde=state_soln[..., 1]
    m=state_soln[..., 2]


    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
    r=r_array
    term1=der.second(der.second(ptilde, r, der.EVEN)[:60], r[:60], der.EVEN)[:60]
    term2=der.second(der.second(p, r, der.EVEN)[:60], r[:60], der.EVEN)[:60]
    term3=der.second(der.second(rho(p), r, der.EVEN)[:60], r[:60], der.EVEN)[:60]
    term4=der.second(der.second((1.0-gv.Lambda)*ptilde-(1.0-3.0*gv.Lambda)*p-gv.Lambda*rho(p), r, der.EVEN)[:60], r[:60], der.EVEN)[:60]
    pl.plot(r, np.abs(p))
    pl.plot(r, np.abs(ptilde))
    pl.show()
    #plot.connectpoints(r[:60], der.first(term1, r[:60], der.EVEN))
    #plot.connectpoints(r[:60], der.first(term2, r[:60], der.EVEN))
    #plot.connectpoints(r[:60], der.first(term3, r[:60], der.EVEN))
    #plot.connectpoints(r[:60], der.first(term4, r[:60], der.EVEN))

    


    #last_r=RN
    #state_int.integrate(last_r)
    #return state_int.y[1]
    return ptilde[len(r)-1]







def pressure(p0, ptilde0, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_integrator('dop853',first_step=1E-10, max_step=1e-2)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[0]







def mass(p0, ptilde0, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[2]





def profile(p0, ptilde0, RN, radius_factor, RES):    
    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            if flag_trunc==0:
                fp= (-(p+rho(p))*tov/r/r)
                fptilde=(-(ptilde+rhotilde(p))*tov/r/r)
            if flag_trunc==1:
                fp= (-(p+rho(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN*gv.RN_factor_truncate*RN-r*r)))
                fptilde=(-(ptilde+rhotilde(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN*gv.RN_factor_truncate*RN-r*r)))
            print 'r={}\t\tp={}'.format(r,p)
            fm=4.0*np.pi*r*r*rhotilde(p)
   
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dop853',first_step=1E-10, max_step=1.5*1e-5, dfactor=1E-5, nsteps=1e8)
    state_int.set_integrator('dop853',first_step=1E-10, max_step=1e-4)
    state_int.set_initial_value(state0, r0)
    #r_array=np.linspace(0.0, radius_factor*RN, RES)
    #RMAX=radius_factor*RN
    RMAX=RN*radius_factor

    r_array=np.linspace(RMAX/(2.0*gv.RES-1.0), RMAX, RES)
    state_soln = np.empty((len(r_array),len(state0)))
    #state_soln[0] = state0
    for i,r in enumerate(r_array):
        if not state_int.successful():
            raise ValueError("Integrator failed.")
        state_int.integrate(r)
        state_soln[i] = state_int.y
    return state_soln



def profile2(p0, ptilde0, RN, radius_factor, RES):    
    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            if flag_trunc==0:
                fp= (-(p+rho(p))*tov/r/r)
                fptilde=(-(ptilde+rhotilde(p))*tov/r/r)
            if flag_trunc==1:
                fp= (-(p+rho(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN*gv.RN_factor_truncate*RN-r*r)))
                fptilde=(-(ptilde+rhotilde(p))*tov/r/r)*(0.5+0.5*np.tanh(gv.radius_truncation_factor*(gv.RN_factor_truncate*RN*gv.RN_factor_truncate*RN-r*r)))
            #print 'r={}\t\tp={}'.format(r,p)
            fm=4.0*np.pi*r*r*rhotilde(p)
   
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dop853',first_step=1E-10, max_step=1.5*1e-5, dfactor=1E-5, nsteps=1e8)
    state_int.set_integrator('dop853',first_step=1E-10, max_step=1e-4)
    state_int.set_initial_value(state0, r0)
    #r_array=np.linspace(0.0, radius_factor*RN, RES)
    #RMAX=radius_factor*RN
    RMAX=RN*radius_factor

    r_array=np.linspace(0.0, RMAX, RES)
    state_soln = np.empty((len(r_array),len(state0)))
    #state_soln[0] = state0
    for i,r in enumerate(r_array):
        if not state_int.successful():
            raise ValueError("Integrator failed.")
        state_int.integrate(r)
        state_soln[i] = state_int.y
    return state_soln





def RNcorrect(p0):
    RN=2.
    if gv.profile_method==1:
        
        pressure_ratio_truncate=gv.pressure_ratio_truncate
        
        ptilde0=0.0130006502247
        x=PTILDE(p0,ptilde0, RN)
        quit()
        
        def f(ptilde):
            Radius_p=bisect(lambda r: pressure(p0,ptilde, r)-pressure_ratio_truncate*p0, 1, RN)
            Radius_ptilde=bisect(lambda r: PTILDE(p0,ptilde, r), 0.8, RN)
            return Radius_p-Radius_ptilde
        
    #ptilde_correct=bisect(lambda ptilde: f(ptilde), p0, p0+p0/6)
        ptilde_correct=newton(lambda ptilde: f(ptilde), p0)
        print "ptilde={}".format(ptilde_correct)
        print "R1={}".format(bisect(lambda r: pressure(p0,ptilde_correct, r)-pressure_ratio_truncate*p0, 1, RN))
        print "R2={}".format(bisect(lambda r: PTILDE(p0,ptilde_correct, r)-pressure_ratio_truncate*p0, 0.8, RN))
        quit()
        
        
        ptilde0_correct=bisect(lambda ptilde0: PTILDE(p0,ptilde0, RN), p0, p0+p0/6)
        
        
        quit()
        Radius_correct=bisect(lambda r: pressure(p0,ptilde0_correct, r)-pressure_ratio_truncate*p0, 1, RN)


    if gv.profile_method==0:
        ptilde0_guess=p0
        Radius_correct=bisect(lambda r: pressure(p0,ptilde0_guess, r)-gv.pressure_ratio_truncate*p0, 1, RN) 
        ptilde0_correct=newton(lambda ptilde0: PTILDE(p0,ptilde0, Radius_correct), p0)
        #print ptilde0_correct
        #quit()
        
        
    return Radius_correct, ptilde0_correct
    

def give_initial_profile(p0, radius_factor, RES):
    #RN=6.0
    #Radius_correct, ptilde0_correct=RNcorrect(p0)
    #ptilde0_correct=bisect(lambda ptilde0: PTILDE(p0,ptilde0, RN), p0, p0+p0/6)
    if gv.profile_method==1:
        Radius_correct, ptilde0_correct=1.32904795128, 0.0130006502247
        R2=1.33623199426
    if gv.profile_method==0:
        Radius_correct, ptilde0_correct=RNcorrect(p0)
    if gv.profile_method==3:
        def shoot_f(ptilde0_guess):
            #ptilde0_guess=gv.p0
            RMAX=0.1
            radius_factor=1.0
            RES=200
            state_soln=profile2(p0, ptilde0_guess, RMAX, radius_factor, RES)  
            r=np.linspace(0.0, RMAX, RES)
            p=state_soln[..., 0]
            ptilde=state_soln[..., 1]
            m=state_soln[..., 2]
            def rho(p):
                return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
            def rhotilde(p):
                return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p
            x3=8.0*np.pi*rhotilde(p)[0]
            der3m=der.first(der.second(m, r, der.ODD), r, der.ODD)[0]
            x2=(8.0*np.pi*r*rhotilde(p))[0]
            der2m=der.second(m, r, der.ODD)[0]
            der1m=der.first(m,r,der.ODD)[0]
            x1=(4.0*np.pi*r*r*rhotilde(p))[0]
            print '{}\t{}\t{}'.format(ptilde0_guess, x3, der3m)
            return x1
        #print shoot_f(gv.p0)
        for i in range(0,10):
            ptilde0_guess=gv.p0+i*gv.p0/2.0
            x=shoot_f(ptilde0_guess)
        

        #ptilde0_correct=bisect(lambda ptilde0: shoot_f(ptilde0), gv.p0, gv.p0-gv.p0/6)
        #print 'ptilde is equal to {}.'.format(ptilde0_correct)
        quit()
        


        
    #quit()
    Mass_correct=mass(p0,ptilde0_correct, Radius_correct)
    #state_soln=profile(p0, ptilde0_correct, Radius_correct, radius_factor, RES)
    state_soln=profile(p0, ptilde0_correct, 1.33515583879, radius_factor, gv.RES)
    #r=np.linspace(0.0, gv.radius_factor*Radius_correct, gv.RES)
    RMAX=radius_factor*Radius_correct
    r=np.linspace(RMAX/(2.0*gv.RES-1.0), RMAX, gv.RES)
    p=state_soln[..., 0]
    ptilde=state_soln[..., 1]
    m=state_soln[..., 2]


    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1.0-gv.Lambda))*p

    


    if(flag_plot==1):
        N_plot=15
        pl.plot(r, p)
        pl.plot(r, ptilde)
        pl.show()
        term1=der.second(der.second(ptilde, r, der.EVEN)[:N_plot], r[:N_plot], der.EVEN)[:N_plot]
        term2=der.second(der.second(p, r, der.EVEN)[:N_plot], r[:N_plot], der.EVEN)[:N_plot]
        term3=der.second(der.second(rho(p), r, der.EVEN)[:N_plot], r[:N_plot], der.EVEN)[:N_plot]
        term4=der.second(der.second((1.0-gv.Lambda)*ptilde-(1.0-3.0*gv.Lambda)*p-gv.Lambda*rho(p), r, der.EVEN)[:N_plot], r[:N_plot], der.EVEN)[:N_plot]
        plot.connectpoints(r[:N_plot], der.first(term1, r[:N_plot], der.EVEN))
        plot.connectpoints(r[:N_plot], der.first(term2, r[:N_plot], der.EVEN))
        plot.connectpoints(r[:N_plot], der.first(term3, r[:N_plot], der.EVEN))
        plot.connectpoints(r[:N_plot], der.first(term4, r[:N_plot], der.EVEN))
    

    Paether=(1.0-gv.Lambda)*ptilde-(1.0-3.0*gv.Lambda)*p-gv.Lambda*rho(p)
    if gv.flag_evolve==0:
        quit()
    filename="profile_centralpressure{}_RES{}_Lambda{}.out".format(p0, RES, gv.Lambda)
    print "Printing {} \n".format(filename)
    np.savetxt(filename, np.c_[r, p, Paether, m], delimiter='\t', header='Mass={} and Radius={} and Columns are \tr\tp\tPaether\tm'.format(state_soln[len(r)-1, 2], Radius_correct))     
    return r, p, Paether, m, Radius_correct
     

   









    
if __name__ == "__main__":
    


    r=np.linspace(0, 4, 500)
    RN=1.32904795128
    term_EVEN=(0.5+0.5*np.tanh(3.5*(gv.RN_factor_truncate*RN*gv.RN_factor_truncate*RN-r*r)))
    term=(0.5+0.5*np.tanh(5.*(gv.RN_factor_truncate*RN-r)))

    pl.plot(r, term)
    pl.plot(r, term_EVEN)
    pl.show()
    '''
    def rho(p):
        return (np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0))

    def dpdrho(p):
        drhodp_value= (1.0/gv.K)*(1.0/gv.Gamma)*((p/gv.K)**(1.0/gv.Gamma -1.0))+1.0/(gv.Gamma-1.0)
        dpdrho_value=1.0/drhodp_value
        return dpdrho_value


    p=np.logspace(-5, 2.0, 200)
    plot.connect(np.log10(p), np.log10(rho(p)))
    plot.connect(np.log10(p), dpdrho(p)-1.0)'''


