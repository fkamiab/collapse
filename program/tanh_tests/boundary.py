#!/usr/bin/env python

"""
boundary.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-15 15:20:29 (jmiller)>

This program defines the boundary conditions used in the neutron star
collapse code. 
"""

import numpy as np
import derivative as deriv
from copy import copy

EVEN = 'even'
ODD = 'odd'

def even_parity_symmetry(a):
    """
    Imposes even-parity symmetry conditions on array a.

    Not in-place. Returns a new array that satisfies the appropriate
    boundary data.
    """
    return deriv.set_to_zero_at_origin(a)

def odd_parity_symmetry(a):
    """
    Imposes odd-parity symmetry conditions on array a.

    Not in-place. Returns a new array that satisfies the appropriate
    boundary data.
    """
    b = copy(a)
    b[0] = 0
    return b

def impose_symmetry(a,parity):
    """
    Imposes even- or odd-parity symmetry conditions on a.

    Not in-place. Returns a new array that satisfies the appropriate
    boundary data.
    """
    if parity == EVEN:
        return even_parity_symmetry(a)
    elif parity == ODD:
        return odd_parity_symmetry(a)
    else:
        raise ValueError("Parity must be even or odd.")
