#!/usr/bin/env python

"""evolve.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 22:30:47 (jmiller)>

This file is the main evolution loop for the system with hydro
variables only.
"""

import numpy as np
import global_variables as gv
#import derivative as der
import derivative_mirror as der
#import derivative_mirror_4 as der4
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import rhs
from scipy.integrate import ode
import pylab as pl
import matplotlib as mpl
import initial_velocity as velocity
import plot as plot
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
import Paether as PaetherFinder
import initial_K as initial_K
from scipy import interpolate
#import initial_profile as initial
import initial_profile_NOshooting as initial
import Hamiltonian_Constraint as Hamiltonian


def evolve(r, Sr_tilde0, tau_tilde0, a0, b0, K0):
    """
    The main loop. Evolves system from t0 to tfinal with nt steps
    with initial data given by r,rho0,p0,ur0,a,b,K,alpha.
    """
    # initialize output arrays
    t_array = np.linspace(0.0,gv.TIME,gv.RESTIME)
    soln_array = np.empty((len(t_array),gv.NUM_EVOLVED_VARIABLES,gv.RES))

    # initial state
    U0 = np.vstack((Sr_tilde0,tau_tilde0,a0,b0,K0))
    soln_array[0] = U0
 
   
    U0_flat = utilities.flatten(U0)

    # make right-hand-side
    my_rhs = rhs.make_rhs(r)

    integrator = ode(my_rhs)
    #integrator.set_integrator('dopri5' ,max_step=gv.STEP,first_step=gv.STEP)
    integrator.set_integrator('dopri5' , first_step=gv.STEP)
    integrator.set_initial_value(U0_flat,0.0)

    for i, t in enumerate(t_array[1:]):
        integrator.integrate(t)
        if gv.printme==1:
            print "integrated to {} out of {}.".format(t,gv.TIME)
        soln_array[i+1] = utilities.unflatten(integrator.y,
                                              gv.NUM_EVOLVED_VARIABLES)

        
        Sr_tilde_step=soln_array[i+1,0,...]
        tau_tilde_step=soln_array[i+1,1,...]
        a_step=soln_array[i+1,2,...]
        b_step=soln_array[i+1,3,...]
        K_step=soln_array[i+1,4,...]
        p_step, ur_step, a_step ,b_step=variables.primitive_variables(r, Sr_tilde_step, tau_tilde_step, a_step, b_step)
        Ktt_step, Krr_step = K_solver.giveK(r, p_step, K_step, ur_step, a_step, b_step)
        Paether_step=PaetherFinder.get_Paether(r, p_step, ur_step, Ktt_step, Krr_step, a_step, b_step)
        alpha_step=alphafinder.give_alpha_direct(r, p_step, Paether_step)


        filename="./time_outputs/time_step_{}.out".format(i+1)
        np.savetxt(filename, np.c_[r, p_step, Paether_step, ur_step, alpha_step, a_step, b_step, K_step, Sr_tilde_step, tau_tilde_step], delimiter='\t', header='Time={} and Columns are r\tp\tPaether\tur\talpha\ta\tb\tK\tSr_tilde\ttau_tilde'.format(t))

    Sr_tilde_soln=soln_array[..., 0, ...]
    tau_tilde_soln=soln_array[..., 1, ...]
    a_soln=soln_array[..., 2, ...]
    b_soln=soln_array[..., 3, ...]
    K_soln=soln_array[..., 4, ...]
    
    return t_array, Sr_tilde_soln, tau_tilde_soln, a_soln, b_soln, K_soln






if __name__ == "__main__":


  
    '''SETTING INITIAL PRESSURE PROFILE FOR THE STAR'''

    if(gv.build_initial_profile==1):
        r, p0, Paether0, m0, a0, b0, NS_radius =initial.give_initial_profile(gv.p0, gv.radius_factor, gv.RES)   
        quit()
    
    if(gv.build_initial_profile==0):
        r, p0, Paether0, m0, a0, b0 = np.loadtxt('profile_centralpressure0.012_RES{}_Lambda0.25_trunc{}.out'.format(gv.RES, gv.radius_truncation_factor), skiprows=1, unpack=True)
        r, p0, Paether0, m01, a0, b0 = np.loadtxt('profile_centralpressure0.012_RES{}_Lambda0.25_trunc10.0.out'.format(gv.RES), skiprows=1, unpack=True)
        r, p0, Paether0, m02, a0, b0 = np.loadtxt('profile_centralpressure0.012_RES{}_Lambda0.25_trunc2.0.out'.format(gv.RES), skiprows=1, unpack=True)
      
        r100, p100, Paether100, m100, a100, b100 = np.loadtxt('profile_centralpressure0.012_RES100_Lambda0.25_trunc{}.out'.format(gv.radius_truncation_factor), skiprows=1, unpack=True)
        r200, p200, Paether200, m200, a200, b200 = np.loadtxt('profile_centralpressure0.012_RES200_Lambda0.25_trunc{}.out'.format(gv.radius_truncation_factor), skiprows=1, unpack=True)
        r400, p400, Paether400, m400, a400, b400 = np.loadtxt('profile_centralpressure0.012_RES400_Lambda0.25_trunc{}.out'.format(gv.radius_truncation_factor), skiprows=1, unpack=True)
        NS_radius=1.33177406643

    rho0=tmunu.get_rho(p0)
    sqrt_det=geometry.get_gamma_half(r,a0,b0)   
    #plot.connect(r, np.log10(p0))
    #plot.connect(r, np.log10(np.abs(Paether0))) 
    '''mpl.rcParams.update({'font.size':15})

    pl.plot(r, m01, lw=3, label=r'Truncation Slope=10')
    pl.plot(r, m02, lw=3, label=r'Truncation Slope=2')
    pl.legend(loc=2)
    pl.xlabel(r'$r$', fontsize=20)
    #pl.ylabel(r'$\log(p)_{(t)}-\log(p)_{(t=0)}$', fontsize=20)
    pl.ylabel(r'$M$', fontsize=20)
    pl.savefig('Mass.pdf'.format(gv.radius_truncation_factor), bbox_inches='tight')
    pl.show()
    quit()'''
    
   

  
    ur0=velocity.giveur_direct(r, p0, Paether0, a0, b0, NS_radius, gv.ur_order)
    ur100=velocity.giveur_direct(r100, p100, Paether100, a100, b100, NS_radius, gv.ur_order)
    ur200=velocity.giveur_direct(r200, p200, Paether200, a200, b200, NS_radius, gv.ur_order)
    ur400=velocity.giveur_direct(r400, p400, Paether400, a400, b400, NS_radius, gv.ur_order)
    

 
    #plot.connect(r, ur0)
    
    
         
    K0=np.ones_like(r)*gv.ur_order
    Ktt0, Krr0 = K_solver.giveK(r, p0, K0, ur0, a0, b0)




    K100=np.ones_like(r100)*gv.ur_order
    Ktt100, Krr100 = K_solver.giveK(r100, p100, K100, ur100, a100, b100)
    
    


    K200=np.ones_like(r200)*gv.ur_order
    Ktt200, Krr200 = K_solver.giveK(r200, p200, K200, ur200, a200, b200)  
    
    K400=np.ones_like(r400)*gv.ur_order
    Ktt400, Krr400 = K_solver.giveK(r400, p400, K400, ur400, a400, b400)



    
    Sr_tilde0, tau_tilde0, a0, b0= variables.hydro_variables(r, p0, ur0, a0, b0)
 
    alpha0=alphafinder.give_alpha_direct(r, p0, Paether0)
    
    #ur_test=np.zeros_like(K0)
    #hamilton, hamilton1, hamilton2=Hamiltonian.giveHamiltonian(r, a0, b0, p0, ur_test, Ktt0, Krr0)

    ur_test100=np.zeros_like(r100)
    hamilton100_2, hamilton100=Hamiltonian.giveHamiltonian(r100, a100, b100, p100, ur100, Ktt100, Krr100)

    ur_test200=np.zeros_like(r200)
    hamilton200_2, hamilton200=Hamiltonian.giveHamiltonian(r200, a200, b200, p200, ur200, Ktt200, Krr200)

    ur_test400=np.zeros_like(r400)
    hamilton400_2, hamilton400=Hamiltonian.giveHamiltonian(r400, a400, b400, p400, ur400, Ktt400, Krr400)


    #lineK=np.ones_like(K0)*(-gv.ur_order**2)
    #pl.plot(r, hamilton)
    #pl.plot(r, hamilton1)
    mpl.rcParams.update({'font.size':15})

    pl.plot(r100, np.log10(np.abs(hamilton100)), lw=3, label=r'$RES=100$, with $K$={}'.format(gv.ur_order))
    
    pl.plot(r200, np.log10(np.abs(hamilton200)), lw=3, label=r'$RES=200$, with $K$={}'.format(gv.ur_order))
 
    pl.plot(r400, np.log10(np.abs(hamilton400)), lw=3, label=r'$RES=400$, with $K$={}'.format(gv.ur_order))


    pl.plot(r100, np.log10(np.abs(hamilton100_2)), lw=3, label=r'$RES=100$, without $K$')
    pl.plot(r200, np.log10(np.abs(hamilton200_2)), lw=3, label=r'$RES=200$, without $K$')
    pl.plot(r400, np.log10(np.abs(hamilton400_2)), lw=3, label=r'$RES=400$, without $K$')
    #pl.plot(r, lineK)
    pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    #pl.legend(loc=5)
    pl.xlabel(r'$r$', fontsize=20)
    #pl.ylabel(r'$\log(p)_{(t)}-\log(p)_{(t=0)}$', fontsize=20)
    pl.ylabel(r'$|H|/(16\pi G^\prime \rho_3)$', fontsize=20)
    pl.savefig('H_threeRES_trunc{}_K{}.pdf'.format(gv.radius_truncation_factor, gv.ur_order), bbox_inches='tight')
    pl.show()
    quit()

    filename="./time_outputs/time_step_{}.out".format(0)
    np.savetxt(filename, np.c_[r, p0, Paether0, ur0, alpha0, a0, b0, K0, Sr_tilde0, tau_tilde0], delimiter='\t', header='Time={} and Columns are r\tp\tPaether\tur\talpha\ta\tb\tK\tSr_tilde\ttau_tilde'.format(0))               
               

    t_array, Sr_tilde_soln, tau_tilde_soln, a_soln, b_soln, K_soln = evolve(r, Sr_tilde0, tau_tilde0, a0, b0, K0)



