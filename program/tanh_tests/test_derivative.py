#!/usr/bin/env python

"""evolve.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 22:30:47 (jmiller)>

This file is the main evolution loop for the system with hydro
variables only.
"""

import numpy as np
import global_variables as gv
import derivative_4_3 as der43
import derivative_4_2 as der42
import derivative_spectral as derspectral
import derivative_4_3_extrapolated as derextra
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import rhs
from scipy.integrate import ode
import pylab as pl
import initial_velocity as velocity
import plot as plot
import alpha as alphafinder
import B_calc as B_calc
import K_solver as K_solver
import Paether as PaetherFinder
import initial_K as initial_K
from scipy import optimize



def givey(x, ydoubleprime):

    def residual(x, y, ydoubleprime):
        res=np.empty_like(x)
        res=der.second(y,x)-ydoubleprime
        res[0]=y[0]
        res[len(x)-1]=y[len(x)-1]-1.0
        return res
    y0=np.ones_like(x)
    sol = optimize.root(lambda y: residual(x, y, ydoubleprime), y0)
    y=sol.x
    return y

if __name__ == "__main__":

    import derivative_spectral
    import derivative_spectral
    x= derspectral.r# np.linspace(0.0, 1.0, 500)
    y=x**3 
    
    #y=der.set_to_zero_at_origin(y)
    yprime= 3.0*x**2 
    #yprime=der.first(y,x)
    #ydoubleprime=6.0*x 
    ydoubleprime=6.0*x

    
    #pl.plot(x, y)
    #pl.plot(x, y3, 'ro')
    #pl.plot(x, der.second(y,x), 'bo')
    #pl.show()

    

    #y3= givey(x, ydoubleprime)
    
    print derspectral.first(y,x)[0]

    y2=derspectral.set_to_zero_at_origin(y)

    print derspectral.first(y2,x)[0]

    pl.plot(x, yprime)
    pl.plot(x, derspectral.first(y,x), 'ro')
    #pl.plot(x, der.second(y, x), 'bo')
    pl.show()


    pl.plot(x, ydoubleprime)
    pl.plot(x, derspectral.second(y,x), 'ro')
    #pl.plot(x, der.second(y, x), 'bo')
    pl.show()
