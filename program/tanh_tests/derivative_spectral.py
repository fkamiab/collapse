#!/usr/bin/env python

"""
derivative.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 21:50:33 (jmiller)>

This program defines the derivative operators used in the neutron star
collapse code. As a test we use a pseudospectral derivative.
"""

import pylab as pl
import numpy as np
from numpy import polynomial
from numpy import linalg
from scipy import integrate
from scipy import optimize
from copy import copy
import global_variables as gv

# ======================================================================
# Global constants
# ======================================================================
LOCAL_XMIN = -1 # Maximum and min values of reference cell
LOCAL_XMAX = 1
POLY = polynomial.chebyshev.Chebyshev  # A class for orthogonal Polynomials
WEIGHT_FUNC = polynomial.chebyshev.chebweight # Quadrature weight
#WEIGHT_FUNC = lambda x: 1.0/np.sqrt(1-x**2) # Quadrature weight
ORDER = gv.RES-1
RN_DEFAULT = 1.33478413497 # radius of neutron star
R_MAX = RN_DEFAULT*gv.radius_factor
DEFAULT_RESOLUTION=100
# ======================================================================


# ======================================================================
# Nodal and Modal Details
# ======================================================================
def continuous_inner_product(foo,bar):
    """"
    Takes the continuous inner product in the POLY norm between
    the functions foo and bar.
    """
    return INTEGRATOR(lambda x: foo(x)*bar(x)*WEIGHT_FUNC(x),
                      LOCAL_XMIN,LOCAL_XMAX,
                      epsrel=1E-2/ORDER)[0]

def get_quadrature_points(order):
    """
    Returns the quadrature points for Gauss-Lobatto quadrature
    as a function of the order of the polynomial we want to
    represent.
    See: https://en.wikipedia.org/wiki/Gaussian_quadrature
    """
    return np.sort(np.concatenate((np.array([-1,1]),
                                   POLY.basis(order).deriv().roots())))

def get_vandermonde_matrices(order,nodes=False):
    """
    Returns the Vandermonde fast-Fourier transform matrices s2c and c2s,
    which convert spectral coefficients to configuration space coefficients
    and vice-versa respectively. Requires the order of the element/method
    as input.
    """
    if np.any(nodes == False):
        nodes = get_quadrature_points(order)
    s2c = np.zeros((order+1,order+1),dtype=float)
    for i in range(order+1):
        for j in range(order+1):
            s2c[i,j] = POLY.basis(j)(nodes[i])
    c2s = linalg.inv(s2c)
    return s2c,c2s

def get_weights(order,c2s = False, nodes=False):
    """
    Returns the integration weights for Gauss-Lobatto quadrature
    as a function of the order of the polynomial we want to
    represent.
    See: https://en.wikipedia.org/wiki/Gaussian_quadrature
    See: arXive:gr-qc/0609020v1
    """
    if POLY == polynomial.chebyshev.Chebyshev:
        weights = np.empty((order+1))
        weights[1:-1] = np.pi/order
        weights[0] = np.pi/(2*order)
        weights[-1] = weights[0]
        return weights
    elif POLY == polynomial.legendre.Legendre:
        if np.all(nodes == False):
            nodes=get_quadrature_points(order)
        interior_weights = 2/((order+1)*order*POLY.basis(order)(nodes[1:-1])**2)
        boundary_weights = np.array([1-0.5*np.sum(interior_weights)])
        weights = np.concatenate((boundary_weights,
                                  interior_weights,
                                  boundary_weights))
        return weights
    else:
        raise ValueError("Not a known polynomial type.")
        return False
    
def get_modal_differentiation_matrix(order):
    """
    Returns the differentiation matrix for the first derivative in the
    modal basis.
    """
    out = np.zeros((order+1,order+1))
    for i in range(order+1):
        out[:i,i] = POLY.basis(i).deriv().coef
    return out

def get_nodal_differentiation_matrix(order,
                                     s2c=False,c2s=False,
                                     Dmodal=False):
    """
    Returns the differentiation matrix for the first derivative
    in the nodal basis

    It goes without saying that this differentiation matrix is for the
    reference cell.
    """
    if np.any(Dmodal == False):
        Dmodal = get_modal_differentiation_matrix(order)
    if np.any(s2c == False) or np.any(c2s == False):
        s2c,c2s = get_vandermonde_matrices(order)
    return np.dot(s2c,np.dot(Dmodal,c2s))
# ======================================================================


# ======================================================================
# Operators outside reference cell
# ======================================================================
def get_colocation_points(order,xmin=LOCAL_XMIN,xmax=LOCAL_XMAX,
                          quad_points=False):
    """
    Generates order+1 colocation points on the domain [xmin,xmax]
    """
    if np.any(quad_points == False):
        quad_points = get_quadrature_points(order)
    scale_factor = (xmax-float(xmin))/(LOCAL_XMAX-float(LOCAL_XMIN))
    shift_factor = xmin-float(LOCAL_XMIN)
    return scale_factor*(shift_factor + quad_points)

def get_global_differentiation_matrix(order,
                                      xmin=LOCAL_XMIN,
                                      xmax=LOCAL_XMAX,
                                      s2c=False,
                                      c2s=False,
                                      Dmodal=False):
    """
    Returns the differentiation matrix in the nodal basis
    for the global coordinates (outside the reference cell)

    Takes the Jacobian into effect.
    """
    scale_factor = (xmax-float(xmin))/(LOCAL_XMAX-float(LOCAL_XMIN))
    LD = get_nodal_differentiation_matrix(order,s2c,c2s,Dmodal)
    PD = LD/scale_factor
    return PD
# ======================================================================


# ======================================================================
# Reconstruct Global Solution
# ======================================================================
def get_continuous_object(grid_func,
                          xmin=LOCAL_XMIN,xmax=LOCAL_XMAX,
                          c2s=False):
    """
    Maps the grid function grid_func, which is any field defined
    on the colocation points to a continuous function that can
    be evaluated.

    Parameters
    ----------
    xmin -- the minimum value of the domain
    xmax -- the maximum value of the domain
    c2s  -- The Vandermonde matrix that maps the colocation representation
            to the spectral representation

    Returns
    -------
    An numpy polynomial object which can be called to be evaluated
    """
    order = len(grid_func)-1
    if np.any(c2s == False):
        s2c,c2s = get_vandermonde_matrices(order)
    spec_func = np.dot(c2s,grid_func)
    my_interp = POLY(spec_func,domain=[xmin,xmax])
    return my_interp
# ======================================================================


# ======================================================================
# A convenience class that generates everything and can be called
# ======================================================================
class PseudoSpectralStencil:
    """
    A convenience class. Given an order, and a domain [xmin,xmax]
    defines internally all structures and methods the user needs.
    """
    def __init__(self,order,xmin,xmax):
        """
        Constructor. Needs the order of the method and the domain
        [xmin,xmax].
        """
        self.order = order
        self.xmin = xmin
        self.xmax = xmax
        self.quads = get_quadrature_points(self.order)
        self.weights = get_weights(self.order,self.quads)
        self.s2c,self.c2s = get_vandermonde_matrices(self.order,self.quads)
        self.Dmodal = get_modal_differentiation_matrix(self.order)
        self.Dnodal = get_nodal_differentiation_matrix(self.order,
                                                       self.s2c,self.c2s,
                                                       self.Dmodal)
        self.colocation_points = get_colocation_points(self.order,
                                                       self.xmin,self.xmax,
                                                       self.quads)
        self.PD = get_global_differentiation_matrix(self.order,
                                                    self.xmin,self.xmax,
                                                    self.s2c,self.c2s,
                                                    self.Dmodal)

    def get_x(self):
        """
        Returns the colocation points
        """
        return self.colocation_points


    def differentiate(self,grid_func,order=1):
        """
        Given a grid function defined on the colocation points,
        returns its derivative of the appropriate order
        """
        assert type(order) == int
        assert order >= 0
        if order == 0:
            return grid_func
        else:
            return self.differentiate(np.dot(self.PD,grid_func),order-1)

    def to_continuum(self,grid_func):
        """
        Given a grid function defined on the colocation points, returns a
        numpy polynomial object that can be evaluated.
        """
        return get_continuous_object(grid_func,self.xmin,self.xmax,self.c2s)

    def make_plottable(self,grid_func,resolution=DEFAULT_RESOLUTION):
        """
        Given a grid function, returns evenly spaced x and y data 
        """
        return self.to_continuum(grid_func).linspace(resolution)

    def __call__(self,grid_func):
        "Convenience call to first-order differentiation."
        return self.differentiate(grid_func)
# ======================================================================

stencil = PseudoSpectralStencil(gv.RES-1,0,R_MAX)
r = stencil.get_x()

def first(f, r):
    """
    Take the first derivative of array f with respect to array r.
    """
    return stencil.differentiate(f)

def second(f, r):
    """
    Take the second derivative of array f with respect to array r.
    """
    return stencil.differentiate(f,2)

def set_to_zero_at_origin(a):
    """
    Set the zeroth element of array a so that a[0] has
    vanishing derivative.

    Not in-place. Returns b, which is a copy of a
    but with the c
    """
    def f(x):
        b = copy(a)
        b[0] = x
        out = stencil.differentiate(b)[0]
        print out
        return out
    soln = optimize.root(f,a[1])
    print soln.success
    b = copy(a)
    b[0] = soln.x
    return b

