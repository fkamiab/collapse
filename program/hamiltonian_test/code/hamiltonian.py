import numpy as np
import pylab as pl
import matplotlib as mpl
import plot as plot
import read_time_steps as read
import Hamil as Hamil
import K_solver as K_solver



if __name__ == "__main__":

    '''Constants'''
    number_variables=10
    time_res=368
    final_time=0.0368007360147

 

    #####################################################
    '''Reading variables and plotting evolution of hamiltonian violation over time'''
 
   ######################## RES 100 ######################
    space_res=100
    r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde, time= read.read_variables(number_variables, space_res, time_res, final_time)
    Ktt=np.empty_like(K)
    Krr=np.empty_like(K)
    hamilton=np.empty_like(time)
    for x in range(0, number_variables):
        Ktt[x], Krr[x]=K_solver.giveK(r[x], p[x], K[x], ur[x], a[x], b[x])    
        hamilton[x]=Hamil.giveHamiltonian(r[x], a[x], b[x], p[x], ur[x], Ktt[x], Krr[x])
        mpl.rcParams.update({'font.size':15})
    pl.plot(time, hamilton, lw=3, label=r'RES=100')
    ######################## RES 200 ######################
    space_res=200
    r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde, time= read.read_variables(number_variables, space_res, time_res, final_time)
    Ktt=np.empty_like(K)
    Krr=np.empty_like(K)
    hamilton=np.empty_like(time)
    for x in range(0, number_variables):
        Ktt[x], Krr[x]=K_solver.giveK(r[x], p[x], K[x], ur[x], a[x], b[x])    
        hamilton[x]=Hamil.giveHamiltonian(r[x], a[x], b[x], p[x], ur[x], Ktt[x], Krr[x])
    pl.plot(time, hamilton, lw=3, label=r'RES=200')   
    ######################## RES 400 ######################
    space_res=400
    r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde, time= read.read_variables(number_variables, space_res, time_res, final_time)
    Ktt=np.empty_like(K)
    Krr=np.empty_like(K)
    hamilton=np.empty_like(time)
    for x in range(0, number_variables):
        Ktt[x], Krr[x]=K_solver.giveK(r[x], p[x], K[x], ur[x], a[x], b[x])    
        hamilton[x]=Hamil.giveHamiltonian(r[x], a[x], b[x], p[x], ur[x], Ktt[x], Krr[x])
    pl.plot(time, hamilton, lw=3, label=r'RES=400') 
   


    #pl.show()





    #####################################################
    '''Reading variables and plotting evolution of hamiltonian violation over space for time zero'''
 
   ######################## RES 100 ######################
    '''space_res=100
    r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde, time= read.read_variables(number_variables, space_res, time_res, final_time)
    Ktt=np.empty_like(K)
    Krr=np.empty_like(K)
    hamilton=np.empty_like(K)
    Ktt[19], Krr[19]=K_solver.giveK(r[19], p[19], K[19], ur[19], a[19], b[19])    
    hamilton[19]=Hamil.giveHamiltonian(r[19], a[19], b[19], p[19], ur[19], Ktt[19], Krr[19])
    mpl.rcParams.update({'font.size':15})
    pl.plot(r[19], hamilton[19], lw=3, label=r'RES=100')

   ######################## RES 200 ######################
    space_res=200
    r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde, time= read.read_variables(number_variables, space_res, time_res, final_time)
    Ktt=np.empty_like(K)
    Krr=np.empty_like(K)
    hamilton=np.empty_like(K)
    Ktt[19], Krr[19]=K_solver.giveK(r[19], p[19], K[19], ur[19], a[19], b[19])    
    hamilton[19]=Hamil.giveHamiltonian(r[19], a[19], b[19], p[19], ur[19], Ktt[19], Krr[19])
    mpl.rcParams.update({'font.size':15})
    pl.plot(r[19], hamilton[19], lw=3, label=r'RES=200')


   ######################## RES 400 ######################
    space_res=400
    r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde, time= read.read_variables(number_variables, space_res, time_res, final_time)
    Ktt=np.empty_like(K)
    Krr=np.empty_like(K)
    hamilton=np.empty_like(K)
    Ktt[19], Krr[19]=K_solver.giveK(r[19], p[19], K[19], ur[19], a[19], b[19])    
    hamilton[19]=Hamil.giveHamiltonian(r[19], a[19], b[19], p[19], ur[19], Ktt[19], Krr[19])
    mpl.rcParams.update({'font.size':15})
    pl.plot(r[19], hamilton[19], lw=3, label=r'RES=400')'''





    
    #pl.plot(time, hamilton, lw=3)
    pl.xlabel(r'$t$', fontsize=20)
    #pl.ylabel(r'$H^2  \ / \ [ \ 2\pi \  \int  \ \ \rho \  r^2 \  dr \ ] $', fontsize=20)
    pl.legend(loc=4, bbox_to_anchor=(1, 0.5))
    pl.savefig('Htime.pdf', bbox_inches='tight')
    pl.show()

 
    quit()
 
    '''mpl.rcParams.update({'font.size':15})
    pl.plot(r[x], ur[x], lw=3, label=r't={}'.format(time[x]))
    pl.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    pl.xlabel(r'$r$', fontsize=20)
    pl.ylabel(r'$K$', fontsize=20)
    pl.savefig('K.pdf', bbox_inches='tight')
    pl.show()'''

 
