import numpy as np
import pylab as pl
import matplotlib as mpl
import plot as plot
import read_time_steps as read
from scipy import interpolate


def interpolate_arrays(interp_n, output_res, rmin, rmax, r, p, Paether, ur, alpha, a, b, K, Sr_tilde, tau_tilde):

    
    r_inter=np.empty([len(r[:, 1]), output_res])
    p_inter=np.empty_like(r_inter)
    Paether_inter=np.empty_like(r_inter)
    ur_inter=np.empty_like(r_inter)
    alpha_inter=np.empty_like(r_inter) 
    a_inter=np.empty_like(r_inter) 
    b_inter=np.empty_like(r_inter) 
    K_inter=np.empty_like(r_inter) 
    Sr_tilde_inter=np.empty_like(r_inter) 
    tau_tilde_inter=np.empty_like(r_inter)

    for x in range(len(r[:, 1])-1, len(r[:, 1])):
     
        
   
        p_interfunction= interpolate.interp1d(r[x], p[x], kind=interp_n)
        Paether_interfunction =interpolate.interp1d(r[x], Paether[x], kind=interp_n)
        ur_interfunction= interpolate.interp1d(r[x], ur[x], kind=interp_n)
        alpha_interfunction= interpolate.interp1d(r[x], alpha[x], kind=interp_n)
        a_interfunction= interpolate.interp1d(r[x], a[x], kind=interp_n)
        b_interfunction= interpolate.interp1d(r[x], b[x], kind=interp_n)
        K_interfunction= interpolate.interp1d(r[x], K[x], kind=interp_n)
        Sr_tilde_interfunction= interpolate.interp1d(r[x], Sr_tilde[x], kind=interp_n)
        tau_tilde_interfunction= interpolate.interp1d(r[x], tau_tilde[x], kind=interp_n)
            
        r_inter[x]=np.linspace(rmin, rmax, output_res)
        
        p_inter[x]=p_interfunction(r_inter[x])
        Paether_inter[x]=Paether_interfunction(r_inter[x])
        ur_inter[x]=ur_interfunction(r_inter[x])
        alpha_inter[x]=alpha_interfunction(r_inter[x])
        a_inter[x]=a_interfunction(r_inter[x])
        b_inter[x]=b_interfunction(r_inter[x])
        K_inter[x]=K_interfunction(r_inter[x])
        Sr_tilde_inter[x]=Sr_tilde_interfunction(r_inter[x])
        tau_tilde_inter[x]=tau_tilde_interfunction(r_inter[x])

    return r_inter, p_inter, Paether_inter, ur_inter, alpha_inter, a_inter, b_inter, K_inter, Sr_tilde_inter, tau_tilde_inter




 





