import numpy as np
import pylab as pl
import matplotlib as mpl
import plot as plot
import read_time_steps as read
from scipy import interpolate
import interpolation as interpol


if __name__ == "__main__":

    '''Constants'''
    number_variables=11
    time_res=300
    final_time=0.030000600012
    interpolation_space_res=100
    interp_n=4
    R_id_convergence=10
    T_id_convergence=10
    #####################################################
    '''Reading variables'''
    space_res=100
    r100, p100, Paether100, ur100, alpha100, a100, b100, K100, Sr_tilde100, tau_tilde100, time100= read.read_variables(number_variables, space_res, time_res, final_time)

    space_res=200
    r200, p200, Paether200, ur200, alpha200, a200, b200, K200, Sr_tilde200, tau_tilde200, time200= read.read_variables(number_variables, space_res, time_res, final_time)

    space_res=400
    r400, p400, Paether400, ur400, alpha400, a400, b400, K400, Sr_tilde400, tau_tilde400, time400= read.read_variables(number_variables, space_res, time_res, final_time)




    #####################################################
    '''Interpolation radius bounds'''
    rmin= r100[1][0]
    rmax= r100[1][len(r100[1])-1]
    #####################################################

    '''Interpolation'''
    r100_inter, p100_inter, Paether100_inter, ur100_inter, alpha100_inter, a100_inter, b100_inter, K100_inter, Sr_tilde100_inter, tau_tilde100_inter=interpol.interpolate_arrays(interp_n, interpolation_space_res, rmin, rmax, r100, p100, Paether100, ur100, alpha100, a100, b100, K100, Sr_tilde100, tau_tilde100)

 
    r200_inter, p200_inter, Paether200_inter, ur200_inter, alpha200_inter, a200_inter, b200_inter, K200_inter, Sr_tilde200_inter, tau_tilde200_inter=interpol.interpolate_arrays(interp_n, interpolation_space_res, rmin, rmax, r200, p200, Paether200, ur200, alpha200, a200, b200, K200, Sr_tilde200, tau_tilde200)


    r400_inter, p400_inter, Paether400_inter, ur400_inter, alpha400_inter, a400_inter, b400_inter, K400_inter, Sr_tilde400_inter, tau_tilde400_inter=interpol.interpolate_arrays(interp_n, interpolation_space_res, rmin, rmax, r400, p400, Paether400, ur400, alpha400, a400, b400, K400, Sr_tilde400, tau_tilde400)
    #####################################################



    K_converge=np.empty_like(K400_inter)
    K_diff = [np.empty_like(K400_inter) for i in range(2)]
    line_one=np.ones_like(K400_inter)/9.0
    
   
    K_converge[T_id_convergence]=np.abs((K400_inter[T_id_convergence]   - K200_inter[T_id_convergence])/(K400_inter[T_id_convergence]   - K100_inter[T_id_convergence]))

    K_diff[0][T_id_convergence] = np.abs(K400_inter[T_id_convergence]   - K100_inter[T_id_convergence])
    K_diff[1][T_id_convergence] = np.abs(K400_inter[T_id_convergence]   - K200_inter[T_id_convergence])

    pl.plot(r400_inter[T_id_convergence], np.log10(K_diff[0][T_id_convergence]))
    pl.plot(r400_inter[T_id_convergence], np.log10(K_diff[1][T_id_convergence]))
    
    #pl.plot(r400_inter[T_id_convergence], (3**(2.5))*K_diff[1][T_id_convergence])
    #pl.xlabel('r')
    #pl.ylabel(r'$\Delta K$')
    #pl.legend([r'$K_{400}-K_{100}$',r'$3^{(2.5)} (K_{400}-K_{200})$']) 
  #  print r400_inter[T_id_convergence][:4]
    pl.show()
    
#    pl.plot(r400_inter[T_id_convergence], K_converge[T_id_convergence])
#    pl.plot(r400_inter[T_id_convergence], line_one[T_id_convergence])
#    pl.show()

    '''pl.plot(r400_inter[1], p400_inter[1], 'ro')
    pl.plot(r100[1], p100[1])
    pl.show()'''
    
    
  
