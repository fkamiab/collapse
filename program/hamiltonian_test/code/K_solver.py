import pylab as pl
import numpy as np
import plot
import global_variables as gv
import derivative_mirror as der
from scipy.optimize import bisect
from scipy import optimize
import math




def giveK(r, p, K, ur, a, b):
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    D=-4.0*np.pi*gv.G*(rho(p)+p)*ur
    def residual(Ktt, r, D, K):
        res=np.empty_like(r)
        res=r*der.first(Ktt,r, der.EVEN)-D*r+(3.0*Ktt-K)
        #res[0]=der.first(Ktt,r, der.EVEN)[0]
        return res
    Ktt0=np.ones_like(r)
    sol = optimize.root(lambda Ktt: residual(Ktt, r, D, K), Ktt0)
    Ktt=sol.x
    Krr=K-2.0*Ktt
    return Ktt, Krr
