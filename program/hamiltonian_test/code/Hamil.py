import pylab as pl
import numpy as np
import plot
import global_variables as gv
import derivative_mirror as der
import math



def giveHamiltonian(r, a, b, p, ur, Ktt, Krr):
    ndontcount=3
 
    aprime=der.first(a,r, der.EVEN)
    bprime=der.first(b,r, der.EVEN)
    bdoubleprime=der.second(b,r, der.EVEN)
    R= -(2.0/(r*r*a*a*a*b*b))*(-2.0*r*b*aprime*(r*bprime+b)+a*(r*r*bprime*bprime +2.0*r*b*(r*bdoubleprime +3.0*bprime) +b*b) -a*a*a)
    W=np.sqrt(1.0+ur*ur/a/a)
    Gamma=gv.Gamma
    def rho(p):
        return np.power(p/gv.K, 1.0/Gamma)+p/(Gamma-1.0)
    rhothree=rho(p)*(W*W-gv.Lambda) +p*(W*W+3.0*gv.Lambda-1.0)
    ohr=16.0*np.pi*gv.G*rhothree
    hamilton=ohr-R-2.0*Ktt*Ktt-4.0*Ktt*Krr
    dr=r[2]-r[1]
    sum_hamilton=np.sum((hamilton*hamilton*r*r*dr)[:-ndontcount])
    sum_density= 2.0*np.pi*np.sum(rho(p)*r*r*dr)
    #sum_hamilton=np.sum(hamilton)
    #print sum_density
    return sum_hamilton/sum_density
#np.log10(hamilton*hamilton/sum_density/sum_density)






    
