#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <limits.h> 
#define PI (3.141592653589793)
#define mp (1.672621e-24)
#define clight (2.99792458*1e10)
#define npart (1000)

double PEOS[317], RHOEOS[317];
void readEOS(void);
double frho(double fpress);
double feps(double matterdensity);
double fpress(double rrrho);

int main(int argc, char *argv[])
{


  int i;
  int j;
  int jj;
  int q;
  int w;
  double dP;
  double *A, *B, *C, *p, *P, *sump, *rho, *Mass, *pmax;
  double r;
  double dr;  double G;
  double VA, VB, VC, Vp, VP, Vrho;
  double fA, fB, fC, fA2, fB2, fC2; 
  double RN, MRN, MRNprevious;
  double K;
  double M;
  double dMRN;
  double diff, diff2, diffprevious;
  double MRNrecord;
  double dRN;
  double Mmax;
  double Minitial;
  double *H, *KK;
  double diffrecord;
  double minMRN, maxMRN;
  double dimL, dimT, dimM;
  double PF;
 

  double kA1, kA2, kA3, kA4; 
  double kB1, kB2, kB3, kB4; 
  double kC1, kC2, kC3, kC4; 
  double kp1, kp2, kp3, kp4; 
  double smasses, Pcgs, rhocgs, Gamma; 

  double P1, P2, P3, rho0;
  int MRNstep;
  double PK, rhoK, dPK;
 
  A =  calloc((npart), sizeof(double));
  B =  calloc((npart), sizeof(double));
  C =  calloc((npart), sizeof(double));
  p =  calloc((npart), sizeof(double));
  pmax =  calloc((npart), sizeof(double));
  P =  calloc((npart), sizeof(double));
  sump =  calloc((npart), sizeof(double));
  rho =  calloc((npart), sizeof(double));
  Mass =  calloc((npart), sizeof(double)); 
  H=calloc((3), sizeof(double));
  KK=calloc((3), sizeof(double));

  
  readEOS();

  dimL=1e7;
  dimT=dimL/(2.99792458*1e10);
  dimM=dimL*2.99792458*2.99792458*1e20/6.674e-8;

 
  RN= 10e5/dimL;
  G=1.0;

  MRNstep=2000;
  smasses=1.989e33;
  q=0;
  

   
   FILE *file;
  file = fopen("RMEOSAether2.txt", "w");
  fprintf(file,"#R\tM \n");


  if(0){  PF=1e-2;
  dP=1e38/1000.0;
  for(q = 0; q < 1000; q++){
    fprintf(file, "%le\t%le \n", PF, frho(PF));
    PF=PF+dP;
    
  }

  }
  

  // if(0){


  dRN=(20.0e5-RN*dimL)/dimL/50.0;
  for(q = 0; q < 50; q++)			
  {
  
      
      
    
 
    
  /*     FILE *file2;
  file2 = fopen("MRN.txt", "w");
  fprintf(file2,"MRN\tp \n");*/
    
      j=0;
      
        
      maxMRN=RN/2.5;
 
      minMRN=RN/100000.0;
      //   maxMRN=2*smasses/dimM;
      MRN=maxMRN;
      dMRN=-(maxMRN-minMRN)/MRNstep;
      
   diff2=0.0;
   w=0;
   
   for(j = 0; j<MRNstep; j++)			
   {
       
  /* MRN=1.3246*smasses/dimM;*/
     A[0]=1.0/(1.0-2.0*MRN/RN);
       B[0]=1.0-2.0*MRN/RN;
       C[0]=2.0*MRN/RN/RN;
       p[0]=1e-20*dimT*dimT*dimL/dimM;     
       P[0]=1e-20*dimT*dimT*dimL/dimM;
       rho[0]=(feps(frho(1e-20)))*dimL*dimT*dimT/dimM;
  
       sump[0] = p[0] + rho[0];
       
       
       r=RN;
       dr=-RN/npart;
       
       
       /*         FILE *file5;
	     file5 = fopen("odeGR.txt", "w");
	     fprintf(file5,"#Radius\tA\tB\tp\trho\tP \n");*/
       
       M=0.0;
       for(i = 0; i < npart; i++)			
	 {
	   
	   
	   fA=-2.0*A[i]*A[i]/r + C[i]*A[i]/B[i] + 2.0*A[i]/r + 8.0*PI*G*A[i]*A[i]*r*(0.5*(rho[i]+p[i])-P[i]);
	   fB=C[i];
	   fC=-8.0*PI*G*A[i]*B[i]*(0.5*(rho[i]+p[i])-P[i])+0.5*C[i]*(fA/A[i]+C[i]/B[i])+2.0*fA*B[i]/r/A[i];
	   Vp=-(fB/B[i])*(p[i] + rho[i])/2.0;
	   Vrho= Vp*(feps(frho((p[i]+Vp*dr)/(dimT*dimT*dimL/dimM)))*(dimL*dimT*dimT/dimM)-feps(frho((p[i])/(dimT*dimT*dimL/dimM)))*(dimL*dimT*dimT/dimM))/(Vp*dr);
	   VP=-(fB/B[i])*(p[i]+rho[i]+P[i])/2.0-(1.0/4.0)*(Vp+Vrho);
	   
	   
	   
	   A[i+1]=A[i]+fA*dr;
	   B[i+1]=B[i]+fB*dr;
	   C[i+1]=C[i]+fC*dr;
	   p[i+1]=p[i]+Vp*dr;
	   rho[i+1]=feps(frho(p[i+1]/(dimT*dimT*dimL/dimM)))*(dimL*dimT*dimT/dimM);
	   P[i+1]=P[i]+VP*dr;
	   sump[i+1]= p[i+1] + rho[i+1];
	   
	   if(rho[i]+p[i]>0){
	     
	     M=-4.0*PI*r*r*(rho[i]+p[i])*dr+M;
      }
	   
	   /*	   fprintf(file5, "%le\t%le\t%le\t%le\t%le\t%le \n", log(r*100.0)/log(10), log(A[i])/log(10), log(B[i])/log(10), log(p[i]/(dimT*dimT*dimL/dimM))/log(10), log((sump[i]-p[i])/(dimL*dimL*dimL/dimM))/log(10), P[i]);*/
	   
	   r=r+dr;
	 }
       
       
       
       i=0;
       
       
       /*  printf("%le\t%le\t%le \n", RN, MRNrecord, M);*/
  /*
    H[2-j]=sqrt((M-MRN)*(M-MRN));
    KK[2-j]=MRN;*/
       
       
  /* printf("H[2-%i]=%le \n", j, H[2-j]);*/
       
  /* MRN=MRN+dMRN;*/
       
       



  
 
       

  
  
       diff=MRN-M;
       if(diff<0){
	 diff=-diff;
       }
       
       
                  if((diff2>0)&&(diff2<diff)){
	 MRNrecord=MRNprevious;
	 j=MRNstep+100;
	 diffrecord=diffprevious;
	 
	 }
       
       MRNprevious=MRN;
       diffprevious=diff;
       
       diff2=diff;
       
       
       
       
       /*  if(diffprevious<1e10){
 
          fprintf(file2, "%le\t%le\t%le \n", MRNprevious, diffprevious, M);
       
	  	  }*/
       
	     if(RN>2.5*minMRN){
	       MRN=(minMRN)*pow(10.0, (MRNstep-2-j)*log(maxMRN/minMRN)/log(10)/(MRNstep-1));
	 	 }
	     /*	 else{
		 	 MRN=MRN+dMRN;
			 }*/

       
       
       
       
       

       
       
       
       

       
       
       
          }
   
   /***********************************/
   
   
   
 




 


















  
   	fprintf(file, "%le\t%le\t%le \n", RN, MRNrecord, diffrecord/MRNrecord);
    

  j=0;
  /* for(j = 1; j < 1000; j++)			
    { 
      if((pmax[j]<pmax[j-1])&&(pmax[j]<pmax[j+1])){
	Mmax=Minitial+dMRN*j;
	jj=j;
	j=1000000;
	}
   

	}*/

 /*fprintf(file, "%le %le \n", RN, Mmax);*/

 /*  if(0){
  printf("%le \n", M);

  i=0;
  for(i = 0; i < npart; i++)			
    {

      rho[npart-1-i]=sump[i]-p[i];

    }


  r=0.0;
  dr=RN/npart;
  Mass[0]=0.0;

  for(i = 1; i < npart; i++)			
    {

      Mass[i]=4.0*PI*r*r*rho[i]*dr+Mass[i-1];
      r=r+dr;

    }


  r=0.0;
  dr=RN/npart;
  Mass[0]=0.0;

  for(i = 0; i < npart; i++)			
    {

       fprintf(file, "%le\t%le \n", r, Mass[i]);
       r=r+dr;
    }
  }

 */
  
    RN=RN+dRN;

   /* RN=exp((100-2-q)*log(5)/(100-1));*/
  
   


 

       }


	 fclose(file);

  free(A);
  free(B);
  free(C);
  free(p);
  free(P);
  free(sump);  
  free(rho);
  free(Mass);
  free(KK);
  free(H);


  	 
  return 0;
}



//}



void readEOS(void){

  char filename[50];
  char line[1000];
  int i;
  double n, rho, p;
 
 
 
 /******Reading the EOS file**************/
  sprintf(filename, "SLY.txt");
  FILE *eos;
  eos = fopen(filename, "r");
 
  if (eos == NULL) {
    printf("Can't open input file 4!\n");
    exit(1);
  }
 
  for(i = 0; i < 317; i++)
    {
      fgets(line, sizeof(line), eos);
      sscanf(line, "%le %le %le \n", &n, &rho, &p);
      RHOEOS[i]=log(mp)/log(10.0)    + n;
      PEOS[i]=p;
    }

  fclose(eos);
  /****************************************/
  


}





double fpress(double rrrho)
{
  char filename[50];
  char line[1000];
  int i;
  double n, rho, p;
   double K, Gamma;
  double LOGrho;
  double press;
  double rhofid, lrhofid, lpfid, P1, P2, P3, rho1, rho2, rho3;

   



 /***FPS*****/
   P1=34.283;
  P2=35.142;
  P3=35.925;
  /**********/

  /****GS1*****/
  /* P1=34.504;
  P2=34.884;
  P3=35.613;*/
  /************/

  /*****AP3*****/
  /*   P1=34.392;
  P2=35.464;
  P3= 36.452;*/
  /*************/


  rho1= log(1.85*2.7e14)/log(10.0);
  rho2= log(2.0*1.85*2.7e14)/log(10.0);
  rho3=log(4.0*1.85*2.7e14)/log(10.0);

  i=0;
 
 /****FPS*****/
    lrhofid=14.30;
      lpfid=log(1.580486e+33)/log(10.0);
  /*************/

 /****GS1*****/
  /*    lrhofid=14.85;
	lpfid=log(4.956768e+34)/log(10.0);*/
   /*************/

 /****AP3*****/ 
 /* lrhofid=14.30;
    lpfid=log(1.580486e+33)/log(10.0);*/
  /*************/

  LOGrho=log(rrrho)/log(10.0);

  if(LOGrho<RHOEOS[0]){
    Gamma=(PEOS[1]-PEOS[0])/(RHOEOS[1]-RHOEOS[0]);
    K = pow(10.0, PEOS[0]-Gamma*RHOEOS[0]);
    press=K*pow(rrrho, Gamma);
  }


  if((LOGrho>RHOEOS[0])&&(LOGrho<RHOEOS[10])&&(LOGrho<lrhofid)){
    for(i = 0; i < 10; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


  if((LOGrho>RHOEOS[10])&&(LOGrho<RHOEOS[20])&&(LOGrho<lrhofid)){
    for(i = 10; i < 20; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

  if((LOGrho>RHOEOS[20])&&(LOGrho<RHOEOS[30])&&(LOGrho<lrhofid)){
    for(i = 20; i < 30; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


 if((LOGrho>RHOEOS[30])&&(LOGrho<RHOEOS[40])&&(LOGrho<lrhofid)){
    for(i = 30; i < 40; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


 if((LOGrho>RHOEOS[40])&&(LOGrho<RHOEOS[50])&&(LOGrho<lrhofid)){
    for(i = 40; i < 50; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


if((LOGrho>RHOEOS[50])&&(LOGrho<RHOEOS[60])&&(LOGrho<lrhofid)){
    for(i = 50; i < 60; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


 if((LOGrho>RHOEOS[60])&&(LOGrho<RHOEOS[70])&&(LOGrho<lrhofid)){
    for(i = 60; i < 70; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

 if((LOGrho>RHOEOS[70])&&(LOGrho<RHOEOS[80])&&(LOGrho<lrhofid)){
    for(i = 60; i < 70; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

 if((LOGrho>RHOEOS[80])&&(LOGrho<RHOEOS[90])&&(LOGrho<lrhofid)){
    for(i = 80; i < 90; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


 if((LOGrho>RHOEOS[90])&&(LOGrho<RHOEOS[100])&&(LOGrho<lrhofid)){
    for(i = 90; i < 100; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[100])&&(LOGrho<RHOEOS[110])&&(LOGrho<lrhofid)){
    for(i = 100; i < 110; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[110])&&(LOGrho<RHOEOS[120])&&(LOGrho<lrhofid)){
    for(i = 110; i < 120; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[120])&&(LOGrho<RHOEOS[130])&&(LOGrho<lrhofid)){
    for(i = 120; i < 130; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[130])&&(LOGrho<RHOEOS[140])&&(LOGrho<lrhofid)){
    for(i = 130; i < 140; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


if((LOGrho>RHOEOS[140])&&(LOGrho<RHOEOS[150])&&(LOGrho<lrhofid)){
    for(i = 140; i < 150; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[150])&&(LOGrho<RHOEOS[170])&&(LOGrho<lrhofid)){
    for(i = 150; i < 170; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[170])&&(LOGrho<RHOEOS[190])&&(LOGrho<lrhofid)){
    for(i = 170; i < 190; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


if((LOGrho>RHOEOS[190])&&(LOGrho<RHOEOS[210])&&(LOGrho<lrhofid)){
    for(i = 190; i < 210; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


if((LOGrho>RHOEOS[210])&&(LOGrho<RHOEOS[230])&&(LOGrho<lrhofid)){
    for(i = 210; i < 230; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


if((LOGrho>RHOEOS[230])&&(LOGrho<RHOEOS[250])&&(LOGrho<lrhofid)){
    for(i = 230; i < 250; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[250])&&(LOGrho<RHOEOS[270])&&(LOGrho<lrhofid)){
    for(i = 250; i < 270; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[270])&&(LOGrho<RHOEOS[290])&&(LOGrho<lrhofid)){
    for(i = 270; i < 290; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }

if((LOGrho>RHOEOS[290])&&(LOGrho<RHOEOS[310])&&(LOGrho<lrhofid)){
    for(i = 290; i < 310; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }


if((LOGrho>RHOEOS[310])&&(LOGrho<RHOEOS[317])&&(LOGrho<lrhofid)){
    for(i = 310; i < 317; i++)
      {
	if((LOGrho>RHOEOS[i])&&(LOGrho<RHOEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  press=K*pow(rrrho, Gamma);
	  i=1000;
	}
      }
  }





  if((LOGrho>lrhofid)&&(LOGrho<rho1)){
    Gamma=(P1-lpfid)/(rho1-lrhofid);
    K = pow(10.0, P1-Gamma*rho1);
    press=K*pow(rrrho, Gamma);
  }

  if((LOGrho>rho1)&&(LOGrho<rho2)){
    Gamma=(P2-P1)/(rho2-rho1);
    K = pow(10.0, P1-Gamma*rho1);
    press=K*pow(rrrho, Gamma);
  }


  if((LOGrho>rho2)){
    Gamma=(P3-P2)/(rho3-rho2);
    K = pow(10.0, P2-Gamma*rho2);
    press=K*pow(rrrho, Gamma);
  }





  return press;


}
 

double frho(double fpress)
{
  char filename[50];
  char line[1000];
  int i;
  double n, rho, p;
  
  double K, Gamma, Gamma1, Gamma2, Gamma3;
  double LOGp;
  double rhooo;
  double rhofid, lrhofid, lpfid, P1, P2, P3, rho1, rho2, rho3;
  double a1, a2, a3;
  
  
  i=0;

  

  

  /***FPS*****/
   P1=34.283;
  P2=35.142;
  P3=35.925;
  /**********/

  /****GS1*****/
  /*P1=34.504;
  P2=34.884;
  P3=35.613;*/
  /************/

  /*****AP3*****/
  /*  P1=34.392;
  P2=35.464;
  P3= 36.452;*/
  /*************/


  rho1= log(1.85*2.7e14)/log(10.0);
  rho2= log(2.0*1.85*2.7e14)/log(10.0);
  rho3=log(4.0*1.85*2.7e14)/log(10.0);

  i=0;

 /****FPS*****/
  lrhofid=14.30;
    lpfid=log(1.580486e+33)/log(10.0);
  /*************/

 /****GS1*****/
  /*lrhofid=14.85;
    lpfid=log(4.956768e+34)/log(10.0);*/
  /*************/

 /****AP3*****/ 
 /* lrhofid=14.30;
    lpfid=log(1.580486e+33)/log(10.0);*/
  /*************/



    LOGp=log(fpress)/log(10.0);
   Gamma1=(P1-lpfid)/(rho1-lrhofid);
   Gamma2=(P2-P1)/(rho2-rho1);
   Gamma3=(P3-P2)/(rho3-rho2);
 
 
 
   //   a1=-1.0*pow(10.0, P1)*pow((pow(10.0,lrhofid))/(pow(10.0, rho1)), Gamma1)/(Gamma1-1.0)/pow(10.0, lrhofid);

   //   a2=a1+   pow(10.0, P1)/(Gamma1-1.0)/pow(10.0, rho1) - pow(10.0, P1)/(Gamma2-1.0)/pow(10.0, rho1);

   //a3= a2+pow(10.0, P1)*pow((pow(10.0,rho2))/(pow(10.0, rho1)), Gamma2)/(Gamma2-1.0)/pow(10.0, rho2) - pow(10.0, P2)/(Gamma3-1.0)/pow(10.0, rho2);
  


  if(LOGp<PEOS[0]){
    Gamma=(PEOS[1]-PEOS[0])/(RHOEOS[1]-RHOEOS[0]);
    K = pow(10.0, PEOS[0]-Gamma*RHOEOS[0]);
    rhooo=pow(fpress/K, 1.0/Gamma);
  }


  if((LOGp>PEOS[0])&&((LOGp<PEOS[10])&&(LOGp<lpfid))){
    for(i = 0; i < 10; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }


  if((LOGp>PEOS[10])&&((LOGp<PEOS[20])&&(LOGp<lpfid))){
    for(i = 10; i < 20; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

  if((LOGp>PEOS[20])&&((LOGp<PEOS[30])&&(LOGp<lpfid))){
    for(i = 20; i < 30; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

  if((LOGp>PEOS[30])&&((LOGp<PEOS[40])&&(LOGp<lpfid))){
    for(i = 30; i < 40; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

  if((LOGp>PEOS[40])&&((LOGp<PEOS[50])&&(LOGp<lpfid))){
    for(i = 40; i < 50; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

  if((LOGp>PEOS[50])&&((LOGp<PEOS[60])&&(LOGp<lpfid))){
    for(i = 50; i < 60; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

  if((LOGp>PEOS[60])&&((LOGp<PEOS[70])&&(LOGp<lpfid))){
    for(i = 60; i < 70; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }


 if((LOGp>PEOS[70])&&((LOGp<PEOS[80])&&(LOGp<lpfid))){
    for(i = 70; i < 80; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

 if((LOGp>PEOS[80])&&((LOGp<PEOS[90])&&(LOGp<lpfid))){
    for(i = 80; i < 90; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

 if((LOGp>PEOS[90])&&((LOGp<PEOS[100])&&(LOGp<lpfid))){
    for(i = 90; i < 100; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }


 if((LOGp>PEOS[100])&&((LOGp<PEOS[150])&&(LOGp<lpfid))){
    for(i = 100; i < 150; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

 if((LOGp>PEOS[150])&&((LOGp<PEOS[200])&&(LOGp<lpfid))){
    for(i = 150; i < 200; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

 if((LOGp>PEOS[200])&&((LOGp<PEOS[250])&&(LOGp<lpfid))){
    for(i = 200; i < 250; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

if((LOGp>PEOS[250])&&((LOGp<PEOS[300])&&(LOGp<lpfid))){
    for(i = 250; i < 300; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }

if((LOGp>PEOS[300])&&((LOGp<PEOS[316])&&(LOGp<lpfid))){
    for(i = 300; i < 316; i++)
      {
	if((LOGp>PEOS[i])&&(LOGp<PEOS[i+1])){
	  Gamma=(PEOS[i+1]-PEOS[i])/(RHOEOS[i+1]-RHOEOS[i]);
	  K = pow(10.0, PEOS[i]-Gamma*RHOEOS[i]);
	  rhooo=pow(fpress/K, 1.0/Gamma);
	  i=1000;
	}
      }
  }


  if((LOGp>lpfid)&&(LOGp<P1)){
    Gamma=(P1-lpfid)/(rho1-lrhofid);
    K = pow(10.0, P1-Gamma*rho1);

  
    rhooo=pow(fpress/K, 1.0/Gamma);
  
    //   rhooo=(1.0+a1)*rhooo+pow(10.0, P1)*pow(pow(10.0, rhooo)/pow(10.0, rho1), Gamma)/(Gamma-1.0);
  }

  if((LOGp>P1)&&(LOGp<P2)){
    Gamma=(P2-P1)/(rho2-rho1);
    K = pow(10.0, P1-Gamma*rho1);
    rhooo=pow(fpress/K, 1.0/Gamma);
 
    //    rhooo=(1.0+a2)*rhooo+pow(10.0, P1)*pow(pow(10.0, rhooo)/pow(10.0, rho1), Gamma)/(Gamma-1.0);
 
  }


  if((LOGp>P2)){
    Gamma=(P3-P2)/(rho3-rho2);
    K = pow(10.0, P2-Gamma*rho2);
    rhooo=pow(fpress/K, 1.0/Gamma);

    //   rhooo=(1.0+a3)*rhooo+pow(10.0, P2)*pow(pow(10.0, rhooo)/pow(10.0, rho2), Gamma)/(Gamma-1.0);
 
  }


  



  return rhooo;


  
  
 
}
 





/*******Testing the reading of the EOS *********/

 /*  FILE *fileEOS;
  fileEOS = fopen("Equation.txt", "w");
  fprintf(fileEOS,"#density\tpressure \n");

  PK=pow(10.0, 9);
  dPK=(1e40-PK)/1000;
  for(q = 0; q < 1000; q++)			
    {
      fprintf(fileEOS, "%le\t%le \n", log(frho(PK))/log(10.0), log(PK)/log(10.0));
      PK=pow(10.0, 9)*exp((q+1)*log(1e30)/1000);

    }
    fclose(fileEOS);*/

/**************************************************/



double feps(double matterdensity)
{
  double eps, sum;
  double rho, rho2;
  double drho;

  int j;

  j=0;

  sum=0.0;
  rho=1e-1;
  drho = matterdensity/600;

  for(j = 0; j < 600; j++)
    {
      sum=fpress(rho)*drho/rho/rho+sum;
      rho=1e-1*exp((j+1)*log(10.0*matterdensity)/600);
      drho=rho-rho2;
      rho2=rho;
     
    }

      eps=matterdensity*clight*clight + matterdensity*sum;

  return eps;


}


  
