#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <limits.h> 
#define PI (3.141592653589793)
#define mp (1.672621e-24)
#define clight (2.99792458*1e10)
#define npart (100000)
#define radiuspoints (20)
#define MRNstep (2000)



int main(int argc, char *argv[])
{


  int i;
  int j;
  int oddity, oddlabel;
  int jj;
  int q;
  int w;
  long long indice;
  double dP;
  double *A, *B, *C, *p, *P, *sump, *rho, *Mass, *pmax;
  double floor (      double sdf );

  double r;
  double dr;  double G;
  double VA, VB, VC, Vp, VP, Vrho;
  double fA, fB, fC, fA2, fB2, fC2; 
  double RN, MRN, MRNprevious;
  double K;
  double M;
  double dMRN;
  double diff, diff2, diffprevious;
  double MRNrecord;
  double dRN;
  double Mmax;
  double Minitial;
  double *H, *KK;
  double diffrecord;
  double minMRN, maxMRN;
  double dimL, dimT, dimM;
  double PF;
 

  double kA1, kA2, kA3, kA4; 
  double kB1, kB2, kB3, kB4; 
  double kC1, kC2, kC3, kC4; 
  double kp1, kp2, kp3, kp4; 
  double smasses, Pcgs, rhocgs, Gamma; 
  double dereps, derrho;
  double *DEREPS;

  double P1, P2, P3, rho0;
   double PK, rhoK, dPK;

  double Parray, RHOarray, EPSarray;
  double *PEOS, *EPSEOS;
  long long row, row1, row2;
  char line[1000];

  
 
  A =  calloc((npart), sizeof(double));
  B =  calloc((npart), sizeof(double));
  C =  calloc((npart), sizeof(double));
  p =  calloc((npart), sizeof(double));
  pmax =  calloc((npart), sizeof(double));
  P =  calloc((npart), sizeof(double));
  sump =  calloc((npart), sizeof(double));
  rho =  calloc((npart), sizeof(double));
  Mass =  calloc((npart), sizeof(double)); 
  H=calloc((3), sizeof(double));
  KK=calloc((3), sizeof(double));

  PEOS =  calloc((10000), sizeof(double));

  EPSEOS =  calloc((10000), sizeof(double));
  DEREPS =  calloc((10000), sizeof(double));
 

  row=0;
  i=0;
  /*  FILE *eos;
  eos = fopen("EOS.txt", "r");
 
  if (eos == NULL) {
    printf("Can't open input file 4!\n");
    exit(1);
  }
 
  for(i = 0; i < 10000; i++)
    {
      fgets(line, sizeof(line), eos);
      sscanf(line, "%lli %le %le %le %le %le \n", &indice, &Parray, &RHOarray, &EPSarray, &dereps, &derrho);
      PEOS[indice]=Parray;
      EPSEOS[indice]=EPSarray;
      DEREPS[indice]=dereps;
    }

    fclose(eos);*/
  
  i=0;
  row=0;
  dimL=5e5;
  dimT=dimL/(2.99792458*1e10);
  dimM=dimL*2.99792458*2.99792458*1e20/6.674e-8;

  oddity =0;
  // RN= 8e5/dimL;
  //RN=1.0;  
  //RN= 1.175000e+00;
  RN=2.0;
G=4.0/3.0;

  smasses=1.989e33;
  q=0;
  K=1.0;

   
  /*   FILE *file;
  file = fopen("RMPolytropicAetherRealK.txt", "w");
  fprintf(file,"#R\tM \n");


 

  // if(0){


  //    dRN=(25.0e5-RN*dimL)/dimL/50.0;
   dRN=(1.1-RN)/radiuspoints;
  for(q = 0; q < radiuspoints; q++)			
  {*/
  
      
      
    
 
    
  /*   FILE *file2;
  file2 = fopen("MRN.txt", "w");
  fprintf(file2,"MRN\tp \n");*/
    
      j=0;
      
        
      maxMRN=RN/2.5;
  
      //  maxMRN=RN;
 
      minMRN=RN/1000.0;
      //  minMRN=1e-10;

      if(RN>15.0e5/dimL){
	minMRN=RN/1000000.0;
      }
 


      //   maxMRN=2*smasses/dimM;
      //MRN=maxMRN;
 


     MRN=1.037e-01;
     MRN=maxMRN;
      //MRN=1.823455e-01;
      // MRN=1.8175905e-01;
      //MRN=maxMRN;
      //MRN=1.821660e-01;
      dMRN=-(maxMRN-minMRN)/MRNstep;
      
   diff2=0.0;
   w=0;
   
     for(j = 0; j<MRNstep; j++)			
       {
   
       //MRN=1.3246*smasses/dimM;
     G=4.0/3.0;
     A[0]=1.0/(1.0-2.0*MRN/RN);
     B[0]=1.0-2.0*MRN/RN;
     C[0]=2.0*MRN/RN/RN;
     p[0]=1e-20;
     P[0]=1e-20;

     //     p[0]=1e-20*dimT*dimT*dimL/dimM;
     //P[0]=-1e-8*dimT*dimT*dimL/dimM;
     //   p[0]=1e-20;
     //  row=((25.0-20.0)*1e2);
     //    rho[0]=(pow(10.0, EPSEOS[row]))*clight*clight*dimL*dimT*dimT/dimM;
    

 
              rho[0] = pow(p[0]/K, 5.0/9.0)+p[0]/(9.0/5.0-1.0);
 
	      //      P[0]=-1e-20;

     
   
  
       sump[0] = p[0] + rho[0];
       
       
       r=RN;
       dr=-RN/npart;
       
       /*             
              FILE *file5;
	     file5 = fopen("odeAether.txt", "w");
	     fprintf(file5,"#Radius\tA\tB\tp\trho\tP \n");*/
	   
       
       M=0.0;
       for(i = 0; i < npart; i++)			
	 {
	   
	   // printf("%i %lli %le \n" , i, row, sump[i]); 

	   fA=-2.0*A[i]*A[i]/r + C[i]*A[i]/B[i] + 2.0*A[i]/r + 8.0*PI*G*A[i]*A[i]*r*(0.5*(rho[i]+p[i])-P[i]);
	   fB=C[i];
	   fC=-8.0*PI*G*A[i]*B[i]*(0.5*(rho[i]+p[i])-P[i])+0.5*C[i]*(fA/A[i]+C[i]/B[i])+2.0*fA*B[i]/r/A[i];
	   Vp=-(fB/B[i])*(p[i] + rho[i])/2.0;

	   // 	      row1=floor(((log((p[i]+Vp*dr)/(dimT*dimT*dimL/dimM))/log(10.0))+25.0)*1e2);
	   if(0){	   row2=floor(((log((p[i])/(dimT*dimT*dimL/dimM))/log(10.0))+25.0)*1e2);

	    //	   Vrho= (pow(10.0, EPSEOS[row1] + log(clight*clight*(dimL*dimT*dimT/dimM))/log(10.0))-pow(10.0, EPSEOS[row2] + log(clight*clight*(dimL*dimT*dimT/dimM))/log(10.0)))/(dr);

	     Vrho=pow(10.0, DEREPS[row2])*Vp;}

	   //	   Vrho=(dimL*dimL/dimT/dimT/(9.0/5.0))*pow(p[i+1]*dimM/dimT/dimT/dimL, (5.0/9.0)-1.0)*Vp+Vp/(9.0/5.0-1.0);

	    Vrho=(5.0/9.0)*pow(p[i], -4.0/9.0)*Vp+Vp/(9.0/5.0-1.0);
   


	   VP=-(fB/B[i])*(p[i]+rho[i]+P[i])/2.0-(1.0/4.0)*(Vp+Vrho);
 
	   
	   
	   A[i+1]=A[i]+fA*dr;
	   B[i+1]=B[i]+fB*dr;
	   C[i+1]=C[i]+fC*dr;
	   p[i+1]=p[i]+Vp*dr;
	   P[i+1]=P[i]+VP*dr;
   

	   //	   row=floor(((log(p[i+1]/(dimT*dimT*dimL/dimM))/log(10.0))+25.0)*1e2);
	   
	   
	   //    rho[i+1]=pow(10.0, EPSEOS[row] + log(clight*clight*(dimL*dimT*dimT/dimM))/log(10.0));




	   	 rho[i+1]= pow(p[i+1]/K, 5.0/9.0)+p[i+1]/(9.0/5.0-1.0);


	 
	   sump[i+1]= p[i+1] + rho[i+1];
	   
	   if(rho[i]+p[i]>0){
	     
	     M=-4.0*PI*r*r*(rho[i]+p[i])*dr+M;
	   }


	   //if(oddity==20000){
	   //     fprintf(file5, "%le\t%le\t%le\t%le\t%le\t%le \n", r, A[i], B[i], p[i], rho[i], P[i]);
	     //oddity=0;
	   //}

	     // oddity=oddity+1;
	   
	   
	   r=r+dr;
	 }
       
       
       
       i=0;
       
       
       /*  printf("%le\t%le\t%le \n", RN, MRNrecord, M);*/
  /*
    H[2-j]=sqrt((M-MRN)*(M-MRN));
    KK[2-j]=MRN;*/
       
       
  /* printf("H[2-%i]=%le \n", j, H[2-j]);*/
       
  /* MRN=MRN+dMRN;*/
       
       



  
 
       

  
  
       diff=MRN-M;
       if(diff<0){
	 diff=-diff;
       }
       
       
         if((RN>11e5/dimL)&&(RN<12.5e5/dimL)){
       if((diff2>0)&&(diff2<diff)&&(diff2<1e-4)){
	 MRNrecord=MRNprevious;
	 j=MRNstep+100;
	 diffrecord=diffprevious;
	 //   printf("%le \n", MRNprevious*dimM/smasses);
	 }
       }
       
       else{
	 if((diff2>0)&&(diff2<diff)){
	 MRNrecord=MRNprevious;
	 j=MRNstep+100;
	 diffrecord=diffprevious;
	 //   printf("%le \n", MRNprevious*dimM/smasses);
	 }
       }
       
       MRNprevious=MRN;
       diffprevious=diff;
       
       diff2=diff;
       
       
       
       
       //   if(diffprevious<1e1){
 
       //   fprintf(file2, "%le\t%le\t%le \n", MRNprevious, diffprevious, M);
       
	    //  }
       
	

       if(RN>2.5){
	 MRN=(minMRN)*exp((MRNstep-2-j)*log(maxMRN/minMRN)/(MRNstep-1));
       }
       else{
	 MRN=MRN+dMRN;
       }

	 

       
       
       
       
       

       
       
       
       

       
       
       
        }
   
   /***********************************/
   
   
   
 




 
















       	printf("%le\t%le\t%le \n", RN, MRNrecord, diffrecord/MRNrecord);

       //    fprintf(file, "%le\t%le\t%le \n", RN, MRNrecord, diffrecord/MRNrecord);
    
     //   fprintf(file, "%le\t%le\t%le \n", RN*dimL/1e5, MRNrecord*dimM/smasses, diffrecord);
    

       //j=0;
  /* for(j = 1; j < 1000; j++)			
    { 
      if((pmax[j]<pmax[j-1])&&(pmax[j]<pmax[j+1])){
	Mmax=Minitial+dMRN*j;
	jj=j;
	j=1000000;
	}
   

	}*/

 /*fprintf(file, "%le %le \n", RN, Mmax);*/

 /*  if(0){
  printf("%le \n", M);

  i=0;
  for(i = 0; i < npart; i++)			
    {

      rho[npart-1-i]=sump[i]-p[i];

    }


  r=0.0;
  dr=RN/npart;
  Mass[0]=0.0;

  for(i = 1; i < npart; i++)			
    {

      Mass[i]=4.0*PI*r*r*rho[i]*dr+Mass[i-1];
      r=r+dr;

    }


  r=0.0;
  dr=RN/npart;
  Mass[0]=0.0;

  for(i = 0; i < npart; i++)			
    {

       fprintf(file, "%le\t%le \n", r, Mass[i]);
       r=r+dr;
    }
  }

 */
  
       // RN=RN+dRN;

   /* RN=exp((100-2-q)*log(5)/(100-1));*/
  
   


 

   //  }


  	// fclose(file5);

  free(A);
  free(B);
  free(C);
  free(p);
  free(P);
  free(sump);  
  free(rho);
  free(Mass);
  free(KK);
  free(H);
  free(EPSEOS);
  free(PEOS);
  free(DEREPS);
  


  	 
  return 0;
}



//}

