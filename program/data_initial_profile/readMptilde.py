import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
import global_variables_initial_profile as gv

SKIP_ROWS=30

ptilde0, ptilde = np.loadtxt('ptilde0versusptilde.out', skiprows=1, unpack=True)


def rho(p):
    return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
def rhotilde(p):
    return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

plot.connect(ptilde0,ptilde)
