import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
from scipy.optimize import bisect
from scipy.interpolate import UnivariateSpline
from scipy.integrate import ode
import global_variables_initial_profile as gv




def create_file(filename):
    "Creates a new file with filename"
    with open(filename,'w') as f:
        f.write('# pcentral\tM\tR\n')

def save_row(p, R,M,filename):
    "Appends R and M to a file with filename"
    with open(filename,"a") as f:
        f.write("{} {} {}\n".format(p, R,M))


def PTILDE(p0, ptilde0, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[1]








def pressure(p0, ptilde0, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[0]







def mass(p0, ptilde0, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= -(p+rho(p))*tov/r/r
            fptilde=-(ptilde+rhotilde(p))*tov/r/r
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    last_r=RN
    state_int.integrate(last_r)
    return state_int.y[2]





def profile(p0, ptilde0, RN, radius_factor, RES):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

    r0=0.0
    m0=0.0
    state0= np.array([p0, ptilde0, m0])
    state=state0
    def f(r, state):
        if(r==0):
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            p=state[0]
            ptilde=state[1]
            m=state[2]
            tov=(m+4.0*np.pi*r*r*r*ptilde)/(1-2.0*m/r)
            fp= (-(p+rho(p))*tov/r/r)*(0.5+0.5*np.tanh(10*(RN-r)))
            fptilde=(-(ptilde+rhotilde(p))*tov/r/r)*(0.5+0.5*np.tanh(10.0*(RN-r)))
            fm=(4.0*np.pi*r*r*rhotilde(p))*(0.5+0.5*np.tanh(10.0*(RN-r)))
   
        return np.array([fp, fptilde, fm])
    state_int = ode(f)
    state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, r0)
    r_array=np.linspace(0.0, radius_factor*RN, RES)
    state_soln = np.empty((len(r_array),len(state0)))
    state_soln[0] = state0
    for i,r in enumerate(r_array[1:]):
        state_int.integrate(r)
        state_soln[i+1] = state_int.y
    filename="profile_centralpressure{}_RES{}_Lambda{}.out".format(p0, RES, gv.Lambda)
    print "Printing {} \n".format(filename)
    np.savetxt(filename, np.c_[r_array, state_soln[..., 0], state_soln[..., 1],state_soln[..., 2]], delimiter='\t', header='Mass={} and Radius={} and Columns are \tr\tp\tptilde\tm'.format(state_soln[len(r_array)-1, 2], RN))     











    
if __name__ == "__main__":

    RES=200
    p0=0.012
    RN=6
    pressure_ratio_truncate=1e-5
    ptilde0_correct=bisect(lambda ptilde0: PTILDE(p0,ptilde0, RN), p0, p0+p0/6)
    Radius_correct=bisect(lambda r: pressure(p0,ptilde0_correct, r)-pressure_ratio_truncate*p0, 1, RN)
    Mass_correct=mass(p0,ptilde0_correct, Radius_correct)
    radius_factor=6.0
    profile(p0, ptilde0_correct, Radius_correct, radius_factor, RES)
    

    '''RES=200
    p_array=np.logspace(np.log10(0.000000005), np.log10(0.04), RES)
    filename='MR_TOV.out'
    create_file(filename)

    for x in range(0, RES):
        RN=6
        ptilde0_correct=bisect(lambda ptilde0: PTILDE(p_array[x],ptilde0, RN), p_array[x], p_array[x]+p_array[x]/6)
        Radius_correct=bisect(lambda r: pressure(p_array[x],ptilde0_correct, r)-(1e-10)*p_array[x], 1, RN)
        Mass_correct=mass(p_array[x],ptilde0_correct, Radius_correct)
        save_row(p_array[x], Radius_correct, Mass_correct,filename)'''


    '''p0=0.032
    RN=6
    ptilde0_correct=bisect(lambda ptilde0: PTILDE(p0,ptilde0, RN), p0-0.1, p0+0.1)
    Radius_correct=bisect(lambda r: pressure(p0,ptilde0_correct, r)-(1e-10)*p0, 1, RN)'''
    #profile(p0, ptilde0_correct, Radius_correct, RES)
    
     
        
