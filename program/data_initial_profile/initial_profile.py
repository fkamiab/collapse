import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
from scipy.optimize import fmin
from scipy.interpolate import UnivariateSpline
from scipy.integrate import ode
import global_variables_initial_profile as gv




def create_file(filename):
    "Creates a new file with filename"
    with open(filename,'w') as f:
        f.write('# R M \n')

def save_row(R,M,filename):
    "Appends R and M to a file with filename"
    with open(filename,"a") as f:
        f.write("{} {}\n".format(R,M))

'''For a given radius and mass the diffM(MRNi, RN) function integrates the equations inward and finds an integrated mass inside and returns the log10 of the abolute value of the difference between the the integrated mass and MRNi.'''
def diffM(MRNi, RN):    
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p
    A0=1.0/(1.0-2.0*MRNi/RN)
    B0=1.0-2.0*MRNi/RN
    C0=-2.0*MRNi/RN/RN
    p0=1e-10
    ptilde0=(1.0/3.0)*rho(p0)
    m0=0
    def get_r(tau):
        return RN-tau
    state0= np.array([A0, B0, C0, p0, ptilde0, m0])
    tau0=0.0
    state=state0
    def f(tau, state):
        r = get_r(tau)
        A = state[0]
        B = state[1]
        C = state[2]
        p = state[3]
        ptilde = state[4]
        m = state[5]
        fA=2.0*A*A/r + C*A/B - 2.0*A/r - 8.0*np.pi*A*A*r*(rhotilde(p)-ptilde)
        fB=C
        fC=-8.0*np.pi*A*B*(rhotilde(p)-ptilde)+0.5*C*(fA/A+C/B)-2.0*fA*B/r/A
        fp=-(C/B)*(p + rho(p))/2.0
        fptilde=-(C/B)*(ptilde + rhotilde(p))/2.0
        fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fA,fB,fC, fp, fptilde, fm])
    state_int = ode(f)
    #state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, tau0)
    last_tau=RN-0.0001
    state_int.integrate(last_tau)
    return np.log10(np.abs((MRNi-state_int.y[5])/MRNi))



'''This function returns a text file containing the mass radius relationship from Radius minR to maxR with resolution Nradius '''
def MRfinder(minR, maxR, Nradius):
    R_array=np.linspace(minR, maxR, Nradius)
    M_array=np.empty([Nradius])
    filename="MR_from_R{}_to_R{}_with_RES{}.out".format(minR, maxR, Nradius)
    create_file(filename)
    for j in range(0, Nradius):
        RN=R_array[j]
        if j==0:
            M_array[j]=fmin(lambda M: diffM(M,RN), RN/2.5)
        else:
            M_array[j]=fmin(lambda M: diffM(M,RN), M_array[j-1])
        save_row(R_array[j],M_array[j],filename)
    print "\n"
    print "Printed file {}. \n".format(filename)




'''Prints out profile of neutron star (with resolution Nintegration) with radius RN'''
def profilefinder(RN, Nintegration):
    MRNi=fmin(lambda M: diffM(M,RN), RN/2.5)
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p
    A0=1.0/(1.0-2.0*MRNi/RN)
    B0=1.0-2.0*MRNi/RN
    C0=-2.0*MRNi/RN/RN
    p0=1e-10
    ptilde0=(1.0/3.0)*rho(p0)
    m0=0
    def get_r(tau):
        return RN-tau
    state0= np.array([A0, B0, C0, p0, ptilde0, m0])
    tau0=0.0
    state=state0
    def f(tau, state):
        r = get_r(tau)
        A = state[0]
        B = state[1]
        C = state[2]
        p = state[3]
        ptilde = state[4]
        m = state[5]
        fA=2.0*A*A/r + C*A/B - 2.0*A/r - 8.0*np.pi*A*A*r*(rhotilde(p)-ptilde)
        fB=C
        fC=-8.0*np.pi*A*B*(rhotilde(p)-ptilde)+0.5*C*(fA/A+C/B)-2.0*fA*B/r/A
        fp=-(C/B)*(p + rho(p))/2.0
        fptilde=-(C/B)*(ptilde + rhotilde(p))/2.0
        fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fA,fB,fC, fp, fptilde, fm])
    state_int = ode(f)
    state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, tau0)
    tau_array=np.linspace(0.0, RN-0.0001, Nintegration)
    state_soln = np.empty((len(tau_array),len(state0)))
    state_soln[0] = state0
    for i,tau in enumerate(tau_array[1:]):
        state_int.integrate(tau)
        state_soln[i+1] = state_int.y
    filename="profile_RN{}_RES{}.out".format(RN, Nintegration)
    np.savetxt(filename, np.c_[get_r(tau_array), state_soln[..., 0], state_soln[..., 1],state_soln[..., 2],state_soln[..., 3],state_soln[..., 4],state_soln[..., 5]], delimiter='\t', header='MRN is {}\tColumns are\tr\tA\tB\tC\tp\tptilde\tm'.format(MRNi[0]))    
    print "\n"
    print "The mass of this neutron star is {}. \n".format(MRNi[0])
    print "Printed file {}. \n".format(filename)
 

def betterprofilefinder(RN, Nintegration):

    ###########################################################################
    '''Find profiles by integrating outside in'''
    MRNi=fmin(lambda M: diffM(M,RN), RN/2.5)
    def rho(p):
        return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
    def rhotilde(p):
        return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p
    A0=1.0/(1.0-2.0*MRNi/RN)
    B0=1.0-2.0*MRNi/RN
    C0=-2.0*MRNi/RN/RN
    p0=1e-10
    ptilde0=(1.0/3.0)*rho(p0)
    m0=0
    def get_r(tau):
        return RN-tau
    state0= np.array([A0, B0, C0, p0, ptilde0, m0])
    tau0=0.0
    state=state0
    def f(tau, state):
        r = get_r(tau)
        A = state[0]
        B = state[1]
        C = state[2]
        p = state[3]
        ptilde = state[4]
        m = state[5]
        fA=2.0*A*A/r + C*A/B - 2.0*A/r - 8.0*np.pi*A*A*r*(rhotilde(p)-ptilde)
        fB=C
        fC=-8.0*np.pi*A*B*(rhotilde(p)-ptilde)+0.5*C*(fA/A+C/B)-2.0*fA*B/r/A
        fp=-(C/B)*(p + rho(p))/2.0
        fptilde=-(C/B)*(ptilde + rhotilde(p))/2.0
        fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fA,fB,fC, fp, fptilde, fm])
    state_int = ode(f)
    state_int.set_integrator('dopri5',max_step=1E-4)
    state_int.set_initial_value(state0, tau0)
    tau_array=np.linspace(0.0, RN-0.0001, Nintegration)
    state_soln = np.empty((len(tau_array),len(state0)))
    state_soln[0] = state0
    for i,tau in enumerate(tau_array[1:]):
        state_int.integrate(tau)
        state_soln[i+1] = state_int.y
    ###########################################################################

        
    '''Initial Conditions'''
    NSKIPS=30
    A0=1.0
    B0=state_soln[len(tau_array)-NSKIPS, 1]
    C0=0.0
    p0=state_soln[len(tau_array)-NSKIPS, 3]
    ptilde0=state_soln[len(tau_array)-NSKIPS, 4]
    m0=0.0
    r0=0.0
    r=r0
    State0= np.array([A0, B0, C0, p0, ptilde0, m0])
    State=State0
    def f(r, State):
        if r==0.0:
            A0 = State0[0]
            B0 = State0[1]
            C0 = State0[2]
            p0 = State0[3]
            ptilde0 = State0[4]
            m0 = State0[5]
            fA=0.0
            fB=0.0
            #Aprime=2.0*(1.0-state_soln[len(tau_array)-80, 0])/get_r(tau_array[len(tau_array)-80])
            SKIPNEAR=230
            Rnear=get_r(tau_array[len(tau_array)-SKIPNEAR])
            Anear=state_soln[len(tau_array)-SKIPNEAR, 0]
            Bnear=state_soln[len(tau_array)-SKIPNEAR, 1]
            Cnear=-state_soln[len(tau_array)-SKIPNEAR, 2]
            pnear=state_soln[len(tau_array)-SKIPNEAR, 3]
            ptildenear=state_soln[len(tau_array)-SKIPNEAR, 4]
            Aprime=8.0*np.pi*Rnear*Anear*Anear*(rhotilde(pnear)-ptildenear) -2.0*Anear*Anear/Rnear+2.0*Anear/Rnear +Anear*Cnear/Bnear
            fC=2.0*Bnear*Aprime/Rnear/Anear -8.0*np.pi*(rhotilde(pnear)-ptildenear)*Bnear*Anear +(Cnear/2.0)*(Aprime/Anear +Cnear/Bnear)
            #fC=fC/3.0
            fC=0.335938
            fC=0.336
            fC=0.33605
            fp=0.0
            fptilde=0.0
            fm=0.0
        else:
            A = State[0]
            B = State[1]
            C = State[2]
            p = State[3]
            ptilde = State[4]
            m = State[5]
            fA=-2.0*A*A/r + C*A/B + 2.0*A/r + 8.0*np.pi*A*A*r*(rhotilde(p)-ptilde)
            fB=C
            fC=-8.0*np.pi*A*B*(rhotilde(p)-ptilde)+0.5*C*(fA/A+C/B)+2.0*fA*B/r/A
            fp=-(C/B)*(p + rho(p))/2.0
            fptilde=-(C/B)*(ptilde + rhotilde(p))/2.0
            fm=4.0*np.pi*r*r*rhotilde(p)
        return np.array([fA,fB,fC, fp, fptilde, fm])
    
    State_int = ode(f)
    #State_int.set_integrator('dopri5',max_step=1E-4)
    State_int.set_initial_value(State0, r0)
    r_array=np.linspace(0.0, RN, Nintegration)
    State_soln = np.empty((len(r_array),len(State0)))
    State_soln[0] = State0
    for i,r in enumerate(r_array[1:]):
        State_int.integrate(r)
        State_soln[i+1] = State_int.y

    filename="profile_better_RN{}_RES{}.out".format(RN, Nintegration)
    np.savetxt(filename, np.c_[r_array, State_soln[..., 0], State_soln[..., 1],State_soln[..., 2],State_soln[..., 3],State_soln[..., 4],State_soln[..., 5]], delimiter='\t', header='MRN is {}\tColumns are\tr\tA\tB\tC\tp\tptilde\tm'.format(MRNi[0]))    
    #print "Aprime is {} \n".format((2.0*(1.0-state_soln[len(tau_array)-80, 0])/get_r(tau_array[len(tau_array)-80]))/get_r(tau_array[len(tau_array)-80]))
    #print "\n"
    print "The mass of this neutron star is {}. \n".format(MRNi[0])
    print "Printed file {}. \n".format(filename)
    
    '''   Rnear=get_r(tau_array[len(tau_array)-80])
    Anear=state_soln[len(tau_array)-80, 0]
    Bnear=state_soln[len(tau_array)-80, 1]
    Cnear=-state_soln[len(tau_array)-80, 2]
    pnear=state_soln[len(tau_array)-80, 3]
    ptildenear=state_soln[len(tau_array)-80, 4]
    Aprime=8.0*np.pi*Rnear*Anear*Anear*(rhotilde(pnear)-ptildenear) -2.0*Anear*Anear/Rnear+2.0*Anear/Rnear +Anear*Cnear/Bnear
    fC=2.0*Bnear*Aprime/Rnear -8.0*np.pi*(rhotilde(pnear)-ptildenear)*Bnear
    print "fC={} \n".format(fC)'''
    
if __name__ == "__main__":
    betterprofilefinder(1.5, 2000)
