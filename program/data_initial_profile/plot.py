import pylab as pl
import numpy as np

def connect(a,b):
    pl.plot(a,b)
    pl.show()
     
def connectlim(a,b, c, d, e, f):
    pl.plot(a,b)
    pl.xlim(c,d)
    pl.ylim(e,f)
    pl.show()

def points(a,b):
    pl.plot(a,b, 'bo')
    pl.show()

def connectpoints(a,b):
    pl.plot(a,b) 
    pl.plot(a,b, 'ro')
    pl.show()
     
def pointslim(a,b, c, d, e, f):
    pl.plot(a,b, 'bo')
    pl.xlim(c,d)
    pl.ylim(e,f)
    pl.show()

def connectpointslim(a,b, c, d, e, f):
    pl.plot(a,b)
    pl.plot(a,b, 'ro')
    pl.xlim(c,d)
    pl.ylim(e,f)
    pl.show()

def pdf(a,b):
    pl.plot(a,b)
    pl.savefig('plot.pdf')
    pl.show()

def pdflimits(a,b, c, d, e, f):
    pl.plot(a,b)
    pl.xlim(c,d)
    pl.ylim(e,f)
    pl.savefig('plot.pdf')
    pl.show()
