import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d

RN=1.0
r=np.linspace(0, 6*RN,200)
f=0.5+0.5*np.tanh(10*(RN-r))
plot.connect(r, f)
