import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
import global_variables_initial_profile as gv

SKIP_ROWS=30

#R, A, B, C, p, ptilde, m = np.loadtxt('profile_better_RN1.5_RES2000.out', skiprows=1, unpack=True)
#R, A, B, C, p, ptilde, m = np.loadtxt('profile.out', skiprows=1, unpack=True)
R, A, B, C, p, ptilde, m = np.loadtxt('profile_RN1.5_RES2000.out', skiprows=1, unpack=True)

#plot.connect(R, B)
def rho(p):
    return np.power(p/gv.K, 1.0/gv.Gamma)+p/(gv.Gamma-1.0)
def rhotilde(p):
    return rho(p) + (3.0*gv.Lambda/(1-gv.Lambda))*p

Aprime=8.0*np.pi*R*A*A*(rhotilde(p)-ptilde) -2.0*A*A/R+2.0*A/R -A*C/B
#Aprime=-2.0*A*A/R+2.0*A/R -A*C/B
fC=2.0*B*Aprime/R/A -8.0*np.pi*(rhotilde(p)-ptilde)*B*A -(C/2)*(Aprime/A -C/B)

plot.connect(R[:-SKIP_ROWS], p[:-SKIP_ROWS])
#dA=np.diff(A)/np.diff(R)
dC=-np.diff(C)/np.diff(R)
#plot.connect(R[:-SKIP_ROWS-1], 2*(1-A[:-SKIP_ROWS-1])/R[:-SKIP_ROWS-1])
#pl.plot(R[:-SKIP_ROWS-1], dC[:-SKIP_ROWS])
#pl.plot(R[:-SKIP_ROWS], fC[:-SKIP_ROWS])
#pl.show()

#plot.connect(R[:-SKIP_ROWS-1], dC[:-SKIP_ROWS])
#ddA=np.diff(dA)/np.diff(R)[:-1]
#plot.connect(R[:-SKIP_ROWS-2], ddA[:-SKIP_ROWS])
#plot.connect(R[:-6], dp[:-5])
#plot.connect(R, p)

'''
r1=np.delete(r, 0)
derp=np.diff(p)/np.diff(r)
dera=np.diff(a)/np.diff(r)
derrho=np.diff(rho)/np.diff(r)

pl.plot(r,a)
#pl.plot(r1, np.log10(dera))
#pl.ylim(2,100)
#pl.xlim(0, 0.02)
pl.show()
'''
