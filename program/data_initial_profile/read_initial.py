import pylab as pl
import numpy as np
import plot
from scipy.interpolate import interp1d
from scipy.interpolate import UnivariateSpline



r, A, B, p, rho, P = np.loadtxt('odeAether.txt', skiprows=1, unpack=True)
#r, A, B, p, rho, P = np.loadtxt('odeGRresubmit.txt', skiprows=1, unpack=True)
pl.plot(r, p)
#pl.ylim(-0.1,0.1)
pl.show()
