#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <limits.h> 
#include "collapseheader.h" 



double* firstder(double arr[]) {

   int i;
   
   der1  =  calloc((NGRID), sizeof(double));

   i=1;
   for (i = 1; i < NGRID-1; i++) {
     der1[i] = (arr[i+1]-arr[i-1])/(r[i+1]-r[i-1]);
   }
   i=0;
   der1[i]=(4.0*arr[i+1]-arr[i+2]-3.0*arr[i])/(2.0*(r[i+1]-r[i]));
   i=NGRID-1;
   der1[i]=(3.0*arr[i]-4.0*arr[i-1]+arr[i-2])/(2.0*(r[i]-r[i-1]));

   return der1;
}


double* secder(double arr[]) {

   int i;
   
   der2  =  calloc((NGRID), sizeof(double));

   i=1;
   for (i = 1; i < NGRID-1; i++) {
     der2[i] = (arr[i+1]+arr[i-1]-2.0*arr[i])/(r[i+1]-r[i])/(r[i+1]-r[i]);
   }

   i=0;
   der2[i]=(arr[i]+arr[i+2]-2.0*arr[i+1])/(r[i+1]-r[i])/(r[i+1]-r[i]);
   i=NGRID-1;
   der2[i]=(arr[i]+arr[i-2]-2.0*arr[i-1])/(r[i]-r[i-1])/(r[i]-r[i-1]);

   return der2;
}

