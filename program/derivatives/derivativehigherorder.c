#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <limits.h> 
#include "collapseheader.h"



double* DER(double arr[]) {

  int i;   
  double d1, d2, d00, d01, d02, d03, d10, d12, d20, d21, d23, d24, d30, d32, d34, d35;
  double dr;
    
  i=45;
  dr=r[i+1]-r[i];
  
  d1 = 0.6666666666666666666666;
  d2 = -0.083333333333333333333;
    
  d00 = -1.4117647058823529411764705882353;
  d01 = 1.7352941176470588235294117647059;
  d02 = -0.23529411764705882352941176470588;
  d03 = -0.088235294117647058823529411764706;

  d10 = -0.50000000000000000000000000000000;
  d12 = 0.50000000000000000000000000000000;

  d20 = 0.093023255813953488372093023255814;
  d21 = -0.68604651162790697674418604651163;
  d23 = 0.68604651162790697674418604651163;
  d24 = -0.093023255813953488372093023255814;

  d30 = 0.030612244897959183673469387755102;
  d32 = -0.60204081632653061224489795918367;
  d34 = 0.65306122448979591836734693877551;
  d35 = -0.081632653061224489795918367346939;

  der[0] = (d00*arr[0]+d01*arr[1]+d02*arr[2]+d03*arr[3])/dr;
  der[1] = (d10*arr[0]+d12*arr[2])/dr;
  der[2] = (d20*arr[0]+d21*arr[1]+d23*arr[3]+d24*arr[4])/dr;
  der[3] = (d30*arr[0]+d32*arr[2]+d34*arr[4]+d35*arr[5])/dr;

  /* der[0] = 0.0;
  der[1] = 0.0;
  der[2] = 0.0;
  der[3] = 0.0;*/


  i=4;
  for (i=4; i<=NGRID-5; i++){
    der[i] = (-d2*arr[i-2]-d1*arr[i-1]+d1*arr[i+1]+d2*arr[i+2])/dr;
  }


  der[NGRID-1] = -(d00*arr[NGRID-1]+d01*arr[NGRID-2]+d02*arr[NGRID-3]+d03*arr[NGRID-4])/dr;
  der[NGRID-2] = -(d10*arr[NGRID-1]+d12*arr[NGRID-3])/dr;
  der[NGRID-3] = -(d20*arr[NGRID-1]+d21*arr[NGRID-2]+d23*arr[NGRID-4]+d24*arr[NGRID-5])/dr;
  der[NGRID-4] = -(d30*arr[NGRID-1]+d32*arr[NGRID-3]+d34*arr[NGRID-5]+d35*arr[NGRID-6])/dr;

  /* der[NGRID-1] = 0.0;
  der[NGRID-2] = 0.0;
  der[NGRID-3] = 0.0;
  der[NGRID-4] = 0.0;*/

 
   return der;
}



