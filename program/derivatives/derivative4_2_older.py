import pylab as pl
import numpy as np
import plot



def der(f, r):
    h=np.diff(r)[1]
    der_f=np.zeros(len(r))   
    for i in range(4, len(r)-4):
        der_f[i]= (1.0/h)*((2.0/3.0)*(f(r)[i+1]-f(r)[i-1]) - (1.0/12.0)*(f(r)[i+2]-f(r)[i-2]))
    
    q00 = -24.0/17.0
    q10 = 59.0/34.0
    q20 = -4.0/17.0
    q30 = -3.0/34.0
    q40 = 0.0
    q50 = 0.0
    der_f[0]=(1.0/h)*(q00*f(r)[0]+q10*f(r)[1]+q20*f(r)[2]+q30*f(r)[3]+q40*f(r)[4]+q50*f(r)[5])
    der_f[len(r)-1]=(1.0/h)*(-q00*f(r)[len(r)-1]-q10*f(r)[len(r)-2]-q20*f(r)[len(r)-3]-q30*f(r)[len(r)-4]-q40*f(r)[len(r)-5]-q50*f(r)[len(r)-6])

    q01 = -1.0/2.0
    q11 = 0.0
    q21 = 1.0/2.0
    q31 = 0.0
    q41 = 0.0
    q51 = 0.0
    der_f[1]=(1.0/h)*(q01*f(r)[0]+q11*f(r)[1]+q21*f(r)[2]+q31*f(r)[3]+q41*f(r)[4]+q51*f(r)[5])
    der_f[len(r)-2]=(1.0/h)*(-q01*f(r)[len(r)-1]-q11*f(r)[len(r)-2]-q21*f(r)[len(r)-3]-q31*f(r)[len(r)-4]-q41*f(r)[len(r)-5]-q51*f(r)[len(r)-6])


    q02 = 4.0/43.0
    q12 = -59.0/86.0
    q22 = 0.0
    q32 = 59.0/86.0
    q42 = -4.0/43.0
    q52 = 0.0
    der_f[2]=(1.0/h)*(q02*f(r)[0]+q12*f(r)[1]+q22*f(r)[2]+q32*f(r)[3]+q42*f(r)[4]+q52*f(r)[5])
    der_f[len(r)-3]=(1.0/h)*(-q02*f(r)[len(r)-1]-q12*f(r)[len(r)-2]-q22*f(r)[len(r)-3]-q32*f(r)[len(r)-4]-q42*f(r)[len(r)-5]-q52*f(r)[len(r)-6])

    q03 = 3.0/98.0
    q13 = 0.0
    q23 = -59.0/98.0
    q33 = 0.0
    q43 = 32.0/49.0
    q53 = -4.0/49.0
    der_f[3]=(1.0/h)*(q03*f(r)[0]+q13*f(r)[1]+q23*f(r)[2]+q33*f(r)[3]+q43*f(r)[4]+q53*f(r)[5])
    der_f[len(r)-4]=(1.0/h)*(-q03*f(r)[len(r)-1]-q13*f(r)[len(r)-2]-q23*f(r)[len(r)-3]-q33*f(r)[len(r)-4]-q43*f(r)[len(r)-5]-q53*f(r)[len(r)-6])


    return der_f


def secder(f, r):
    def der_f(r):
        return der(f,r)
    return der(der_f,r)
    



if __name__ == "__main__":
    RES=10
    r=np.linspace(-10, 10, RES)
    def f(r):
        return r*r
    def g(r):
        return 2.0*r
    def k(r):
        return 2.0*np.ones_like(r)

    pl.plot(r, secder(f,r))
    pl.plot(r, k(r), 'bo')
    pl.show()
    '''print der(f,r) - g(r)
    pl.semilogy(r, der(f,r) - g(r))
    pl.show()'''



