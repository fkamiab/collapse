#!/usr/bin/env python

"""energy_momentum.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-17 17:27:57 (jmiller)>

This file defines properties of the energy-momentum tensor and the
equation of state.
"""

import geometry
import numpy as np
import derivative as der
import global_variables as gv

def get_w(ur,a):
    """
    Returns the Lorentz factor between normal and fluid observers.
    """
    return np.sqrt(1.0 + (ur*ur)/(a*a))

def get_rho0(p):
    """
    Equation of state. Get rest-mass density from pressure
    """
    return np.power(p/gv.K, 1.0/gv.Gamma)

def get_rho(p):
    """
    Equation of state. Gets density from pressure.
    """
    return get_rho0(p) + p/(gv.Gamma-1.0)

def get_vr(ur,alpha,a):
    """
    Calculates the fluid 3-velocity vr as a function of:
    -- the four-velocity (with index down) ur 
    -- the lapse alpha
    -- the (1,1)-component of the metric, a
    """
    return (alpha*ur)/(get_w(ur,a)*a*a)

def get_T_tu_rd(rho,p,ur,alpha,a):
    """
    Get the component of the stress-energy tensor T^t_r as a function
    of the pressure, radial component of the four-velocity, lapse, and
    (x,x)-component of the metric.
    """
    out = ((rho + p)*get_w(ur,a)*ur)/alpha
    #out[0] = 0
    return out

def get_T_tu_tu(rho,p,ur,alpha,a):
    """
    Get the component of the stress-energy tensor T^{tt} as a function
    of the pressure, radial component of the four-velocity, lapse, and
    (x,x)-component of the metric.
    """
    W= get_w(ur,a)
    out = ((rho + p)*W*W - p)/(alpha*alpha)
    #out = der.set_to_zero_at_origin(out)
    return out

def get_T_ru_rd_0(rho,p,ur,alpha,a):
    """
    Get the component of the stress-energy tensor T^r_r as a function
    of the pressure, radial component of the four-velocity, lapse, and
    (x,x)-component of the metric.

    This version is missing p. Useful for calculating rhs
    """
    W = get_w(ur,a)
    vr = get_vr(ur,alpha,a)
    out = ((rho + p)*((vr*vr*W*W*a*a)/(alpha*alpha)))
    #out = der.set_to_zero_at_origin(out)
    return out

def get_T_ru_rd(rho,p,ur,alpha,a):
    """
    Get the component of the stress-energy tensor T^r_r as a function
    of the pressure, radial component of the four-velocity, lapse, and
    (x,x)-component of the metric.

    This version is missing p. Useful for calculating rhs
    """
    return get_T_ru_rd_0(rho,p,ur,alpha,a)+p

def get_T_tu_ru(rho,p,ur,alpha,a):
    """
    Get the component of the stress-energy tensor T^{tr} as a function
    of the pressure, radial component of the four-velocity, lapse, and
    (x,x)-component of the metric.
    """
    W = get_w(ur,a)
    vr = get_vr(ur,alpha,a)
    out = ((rho + p)*W*W*vr)/(alpha*alpha)
    #out[0] = 0
    return out

def get_T_ru_ru(rho, p,ur,alpha,a):
    """
    Get the component of the stress-energy tensor T^{rr} as a function
    of the pressure, radial component of the four-velocity, lapse, and
    (x,x)-component of the metric.

    WARNING. May become singular.
    """
    vr = get_vr(ur,alpha,a)
    out = get_T_tu_ru(rho, p,ur,alpha,a)*vr + p/(a*a)
    #out = der.set_to_zero_at_origin(out)
    return out

def get_T_thetau_thetau(p,ur,alpha,a,b,r):
    """
    Get the component of the stress-energy tensor T^{theta theta} as a
    function of the pressure, radial component of the four-velocity,
    lapse, and (x,x)-component of the metric.

    WARNING. May become singular.
    """
    return p/(b*b*r*r)

def get_T_thetau_thetau_densitized(p,ur,alpha,a,b,r):
    """
    Get the component of the stress-energy tensor, T^{theta theta},
    multiplied by the square root of the determinant of the metric, as
    a function of the pressure, radial component of the four-velocity,
    lapse, and (x,x)-component of the metric.
    """
    return np.abs(a)*p
