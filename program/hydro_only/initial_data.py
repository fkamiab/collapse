#!/usr/bin/env python

"""initial_data.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-16 21:37:01 (jmiller)>

Initial data for a flat spacetime.
"""

import pylab as pl
import numpy as np
from scipy import optimize
import geometry
import global_variables as gv
import energy_momentum as tmunu

RN_DEFAULT = 1.4 # radius of neutron star
R_MAX = RN_DEFAULT*gv.radius_factor

def gaussian_packet(amplitude = 1, width = 1, r=None):
    """
    Returns r, rho, p, ur, a, b, alpha for a Gaussian packet of gas
    centred at the origin.
    """
    if r == None:
        r = np.linspace(0,R_MAX,gv.RES)
    p = amplitude*np.exp(-r*r/(width**2))
    rho = tmunu.get_rho(p)
    ur = np.zeros_like(r)
    a = np.ones_like(r)*geometry.FLAT_A
    b = np.ones_like(r)*geometry.FLAT_B
    K = np.zeros_like(r)
    alpha = np.ones_like(r)*geometry.FLAT_ALPHA
    return r, rho, p, ur, a, b, K, alpha

def stellar_surface(height_max = 1,
                    height_min = gv.pressure_ratio_truncate**2,
                    width = RN_DEFAULT,
                    r=None):
    """
    A step function in radius to mimic a stellar surface. Returns
    r, rho, p, ur, a, b, alpha
    """
    if r == None:
        r = np.linspace(0,R_MAX,gv.RES)
    p = np.array([height_max if ri <= width else height_min for ri in r])
    rho = tmunu.get_rho(p)
    ur = np.zeros_like(r)
    a = np.ones_like(r)*geometry.FLAT_A
    b = np.ones_like(r)*geometry.FLAT_B
    K = np.zeros_like(r)
    alpha = np.ones_like(r)*geometry.FLAT_ALPHA
    return r, rho, p, ur, a, b, K, alpha

    
