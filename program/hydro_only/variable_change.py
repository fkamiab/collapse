#!/usr/bin/env python

"""variable_change.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-17 14:38:05 (jmiller)>

This file defines variable transformations between conserved and
primitive variables and related quantities.
"""

import pylab as pl
import numpy as np
import global_variables as gv
import derivative as der
from scipy import optimize
import geometry
import energy_momentum as tmunu
import utilities


# ======================================================================
# Get conserved from primitive
# ======================================================================
def to_conserved(r, rho, p, ur, a, b, alpha,densitized=True):
    "Finds conserved variables from primitive ones"
    W=tmunu.get_w(ur,a)
    Gamma=gv.Gamma
    sqrt_det = geometry.get_gamma_half(r,a,b)
    if densitized:
        D_tilde = sqrt_det*W*tmunu.get_rho0(p)
        Sr_tilde=sqrt_det*alpha*tmunu.get_T_tu_rd(rho,p,ur,alpha,a)
        tau_tilde= (alpha*alpha*sqrt_det*tmunu.get_T_tu_tu(rho,p,ur,alpha,a)
                    - D_tilde)
    else:
        D_tilde = W*tmunu.get_rho0(p)
        Sr_tilde= alpha*tmunu.get_T_tu_rd(rho,p,ur,alpha,a)
        tau_tilde= (alpha*alpha*tmunu.get_T_tu_tu(rho,p,ur,alpha,a)
                    - D_tilde)
    return D_tilde, Sr_tilde, tau_tilde
# ======================================================================


# ======================================================================
# Get primitive from conserved
# ======================================================================
def to_primitive(r,D_tilde,Sr_tilde,tau_tilde,a,b,alpha):
    rho = np.empty_like(r)
    p = np.empty_like(r)
    ur = np.empty_like(r)
    p0 = 1e-3
    rho0 = tmunu.get_rho(p0)
    ur0 = 1e-3
    gamma_half = geometry.get_gamma_half(r,a,b)
    D = np.zeros_like(D_tilde)
    Sr = np.zeros_like(Sr_tilde)
    tau = np.zeros_like(tau_tilde)
    D[1:] = D_tilde[1:]/gamma_half[1:]
    Sr[1:] = Sr_tilde[1:]/gamma_half[1:]
    tau[1:] = tau_tilde[1:]/gamma_half[1:]
    D = der.set_to_zero_at_origin(D)
    tau = der.set_to_zero_at_origin(tau)
    
    #pl.plot(r,D)
    #pl.plot(r,Sr)
    #pl.plot(r,Sr,'ro')
    #pl.plot(r,tau)
    #pl.show()
    
    for i in range(1,len(r)):
        if i > 1:
            p0 = p[-(i-1)]
            rho0 = rho[-(i-1)]
            ur0 = ur[-(i-1)]
        def f(data):
            rho_value = data[0]
            p_value = data[1]
            ur_value = data[2]
            D_value,Sr_value,tau_value \
                = to_conserved(r[-i],
                               rho_value,
                               p_value,
                               ur_value,
                               a[-i],b[-i],
                               alpha[-i],
                               densitized = False)
            D_tilde_residual = 1e2*(D[-i] - D_value)
            #D_tilde_residual = 1e2*(tmunu.get_rho(p_value) - rho_value)
            Sr_tilde_residual = 1e2*(Sr[-i] - Sr_value)
            tau_tilde_residual = 1e2*(tau[-i] - tau_value)
            return np.array([D_tilde_residual,
                             Sr_tilde_residual,
                             tau_tilde_residual])
        sol = optimize.root(f,np.array([rho0,p0,ur0]),
                            method='lm',
                            tol=1e-10,
                            options={'maxiter':2000})
        if sol.success:
            rho_sol,p_sol,ur_sol=np.split(sol.x,3)
        else:
            print "solver failed!"
            print "Failure at radius {}".format(r[i])
            print "Status = {}".format(sol.status)
            print sol.message
            print sol.fun
            raise ValueError("Solver failed!")
        rho[-i] = rho_sol
        p[-i] = p_sol
        ur[-i] = ur_sol
    rho = der.set_to_zero_at_origin(rho)
    p = der.set_to_zero_at_origin(p)
    ur[0] = 0
    return rho,p,ur

# def to_primitive(r, D_tilde, Sr_tilde, tau_tilde, a, b, alpha):
#     '''Finds primitive variables from hydro ones'''
#     def F(data):
#         data_unflattened = utilities.unflatten(data,3)
#         rho_value = data_unflattened[0]
#         p_value = data_unflattened[1]
#         ur_value = data_unflattened[2]
#         D_tilde_value, Sr_tilde_value,tau_tilde_value \
#             = to_conserved(r,
#                            rho_value,p_value, ur_value,
#                            a, b, alpha)
#         # rho and p must satisfy EOS
#         D_tilde_residual = tmunu.get_rho(p_value) - rho_value
#         #D_tilde_residual = D_tilde - D_tilde_value
#         # everything else must satisfy definition of conserved variables
#         Sr_tilde_residual = Sr_tilde - Sr_tilde_value
#         tau_tilde_residual = tau_tilde - tau_tilde_value
#         D_tilde_residual[0] = der.first(rho_value,r)[0] # drho/dr = 0 at r = 0
#         Sr_tilde_residual[0] = der.first(p_value,r)[0] # dp/dr = 0 at r = 0 
#         tau_tilde_residual[0] = ur_value[0] # ur = 0 at r = 0
# 
#         #D_tilde_residual[1] = der.first(rho_value,r)[1] # drho/dr = 0 at r = 0
#         #Sr_tilde_residual[1] = der.first(p_value,r)[1] # dp/dr = 0 at r = 0
#         #D_tilde_residual[2] = der.first(rho_value,r)[2] # drho/dr = 0 at r = 0
#         #Sr_tilde_residual[2] = der.first(p_value,r)[2] # dp/dr = 0 at r = 0 
#         #tau_tilde_residual[1] = r[1] # ur = 0 at r = 0
# 
#         array_F=utilities.flatten(np.vstack((D_tilde_residual,
#                                              Sr_tilde_residual,
#                                              tau_tilde_residual)))
#         return array_F
#     p0=np.ones_like(r)
#     rho0 = tmunu.get_rho(p0)
#     ur0=np.zeros_like(r)
#     x0=utilities.flatten(np.vstack((rho0, p0, ur0)))
#     sol=optimize.root(F, x0,
#                       method='lm',
#                       options={'maxiter':2000})
#     if sol.success:
#         rho_sol,p_sol,ur_sol=np.split(sol.x,3)
#     else:
#         print "solver failed!"
#         print "Status = {}".format(sol.status)
#         print sol.message
#         print sol.fun
#         print sol.nit
#         raise ValueError("Solver failed!")
#     #rho_sol = der.set_to_zero_at_origin(rho_sol)
#     #p_sol = der.set_to_zero_at_origin(p_sol)
#     #ur_sol[0] = 0
#     return rho_sol, p_sol, ur_sol
# # ======================================================================
    
