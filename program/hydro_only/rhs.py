#!/usr/bin/env python

"""
rhs.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-06-17 17:29:16 (jmiller)>

This file defines the right-hand-side of the pde system
(d/dt) U = f(t,U)

More precisely, the time evolution of the system U is is given by
(d/dt) U = - (d/dr) FLUX(t,U) + M(t,U)

we caluclate everything on the right-hand-side here.

U = [D_tilde,Sr_tilde,tau_tilde,a,b,K]

where a and b are metric components and
where K is the trace of the extrinsic curvature.
"""

import pylab as pl
import numpy as np
import global_variables as gv
import derivative as der
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities

NUM_VARIABLES = gv.NUM_EVOLVED_VARIABLES
CONVERSION_MESSAGE="conserved -> primitive failed!"

def flux(r,rho,p,ur,D_tilde,Sr_tilde,tau_tilde,a,b,K,alpha):
    """
    Calculates the flux term for the conserved quantities.
    Defined as FLUX(t,U) above.

    Returns 6 arrays, one for each variable in U. However, the flux
    for a and b is trivial.
    """
    gamma_half = geometry.get_gamma_half(r,a,b)
    T_ru_rd = tmunu.get_T_ru_rd(rho,p,ur,alpha,a)
    T_ru_rd = der.set_to_zero_at_origin(T_ru_rd)
    T_tu_ru = tmunu.get_T_tu_ru(rho,p,ur,alpha,a)
    T_tu_ru[0] = 0
    vr = tmunu.get_vr(ur,alpha,a)
    fD = D_tilde*vr
    # fSr moved to source because part of
    # the gradient is divergent and that part
    # cancels with a source term.
    fSr = np.zeros_like(r) #alpha*gamma_half*T_ru_rd
    ftau = (alpha**2)*gamma_half*T_tu_ru - D_tilde*vr
    fa = np.zeros_like(r)
    fb = np.zeros_like(r)
    fk = np.zeros_like(r)

    # dedensitized = fSr[1:]/gamma_half[1:]
    # pl.plot(r[1:],dedensitized)
    # pl.plot(r[1:],dedensitized,'ro')
    # pl.title(r'$fSr/\gamma$')
    # pl.show()
    # 
    # pl.plot(r[1:],der.first(dedensitized,r[1:]))
    # pl.plot(r[1:],der.first(dedensitized,r[1:]),'ro')
    # pl.title(r'$(fSr/\gamma)$ prime')
    # pl.show()
    # 
    # ddedensitized = der.first(fSr,r)[1:]/gamma_half[1:]
    # pl.plot(r[1:],ddedensitized)
    # pl.plot(r[1:],ddedensitized,'ro')
    # pl.title(r'$(dfSr/dr)/\gamma$')
    # pl.show()
    #fD[0] = 0.
    #fSr[0] = 0.
    #ftau[0] = 0.
    return np.vstack((fD,fSr,ftau,fa,fb,fk))

def source(r,rho,p,ur,D_tilde,Sr_tilde,tau_tilde,a,b,
           K,alpha,Krr=0,Ktt=0):
    """
    Calculates the source terms, M(t,U) as described above.

    Returns 6 arrays, one for each variable (other than r and alpha)
    given as input. For now, a, b, and K don't evolve, so their source
    terms are trivial.
    
    For now, since gravity is static, all K can be assumed to be zero.
    """
    # TODO: Is tdef flux(r,rho,p,ur,D_tilde,Sr_tilde,tau_tilde,a,b,K,alpha):
    """
    Calculates the flux term for the conserved quantities.
    Defined as FLUX(t,U) above.

    Returns 6 arrays, one for each variable in U. However, the flux
    for a and b is trivial.
    """
    # TODO: Is this right?
    TthetathetaupTIMESrquared=p/b/b 
    gamma_half = geometry.get_gamma_half(r,a,b)
    T_tu_tu = tmunu.get_T_tu_tu(rho,p,ur,alpha,a)
    T_ru_ru = tmunu.get_T_ru_ru(rho,p,ur,alpha,a)
    T_ru_rd = tmunu.get_T_ru_rd(rho,p,ur,alpha,a)
    T_ru_rd_0 = tmunu.get_T_ru_rd_0(rho,p,ur,alpha,a)

    # m values
    mD = np.zeros_like(r)
    # (d/dr) flux(Sr), calculated here for regularization
    mfSr = (alpha*gamma_half*der.first(T_ru_rd,r)
            + alpha*a*der.first(b*b*r*r,r)*T_ru_rd_0
            + der.first(alpha,r)*T_ru_rd*gamma_half)
    # source term for Sr
    mSr = alpha*(0.5*gamma_half*(T_tu_tu*der.first(-alpha*alpha,r)
                                 + T_ru_ru*der.first(a*a,r)))
                 # a dirty hack. We set the divergent term in the
                 # source to zero. It cancels witha  flux term.
                 # + a*p*(b*b*2.0*r + 2.0*b*der.first(b,r)*r*r))
    mSr -= mfSr
    mtau = alpha*gamma_half*(T_ru_ru*a*a*Krr
                             + 2.0*b*b*Ktt*TthetathetaupTIMESrquared)
    # Metric variables
    ma = np.zeros_like(r)
    mb = np.zeros_like(r)
    mK = np.zeros_like(r)

    return np.vstack((mD,mSr,mtau,ma,mb,mK))

def rhs(t,r,U):
    """
    The right-hand-side operator:
    M(t,U) - (d/dr)FLUX(t,U)
    """
    print "\t t = {}".format(t)
    U_2d = utilities.unflatten(U,NUM_VARIABLES)
    D_tilde = U_2d[0]
    Sr_tilde = U_2d[1]
    tau_tilde = U_2d[2]
    a = U_2d[3]
    b = U_2d[4]
    K = U_2d[5]
    # For now, since gravity is static, all K can be assumed to be zero.
    Krr = 0
    Ktt = 0
    alpha = np.ones_like(r)*geometry.FLAT_ALPHA
    # get primitives
    #try:
    rho,p,ur = variables.to_primitive(r,D_tilde,Sr_tilde,tau_tilde,
                                      a,b,alpha)
    #except: # placeholder for better error handling.
    #    utilities.simple_error_message(CONVERSION_MESSAGE,
    #                                   D_tilde,Sr_tilde,tau_tilde)

    # Get flux
    fU_2D = flux(r,rho,p,ur,D_tilde,Sr_tilde,tau_tilde,
                 a,b,K,alpha)
    # Get source terms
    mU_2D = source(r,rho,p,ur,D_tilde,Sr_tilde,tau_tilde,a,b,
                   K,alpha,Krr,Ktt)
    # Get 2D rhs
    rhs_2D = np.empty_like(fU_2D)
    for i, f in enumerate(fU_2D):
        rhs_2D[i] = mU_2D[i] - der.first(f,r)
        #rhs_2D[i][0] = 0
    # flatten
    rhs_1D = utilities.flatten(rhs_2D)

    gamma_half = geometry.get_gamma_half(r,a,b)

    # dd = rhs_2D[1][1:]/gamma_half[1:]
    # pl.plot(r[1:],dd)
    # pl.plot(r[1:],dd,'ro')
    # pl.plot(r,-der.first(p,r))
    # pl.legend([r'$\dot{S_r}$',r'$\frac{d}{dr}p$'])
    # pl.show()
    return rhs_1D

def make_rhs(r):
    """
    Given a radius grid, make the right-hand-side function, which is a closure.
    """
    return lambda t,U: rhs(t,r,U)

