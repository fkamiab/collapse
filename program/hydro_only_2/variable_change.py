#!/usr/bin/env python

"""variable_change.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-09-27 21:12:23 (jmiller)>

This file defines variable transformations between conserved and
primitive variables and related quantities.
"""

import pylab as pl
import numpy as np
import global_variables as gv
from scipy import optimize
import geometry
import energy_momentum as tmunu
import utilities


# ======================================================================
# Get conserved from primitive
# ======================================================================
def to_conserved(r, rho, p, ur, a, b, alpha,
                 gamma_half=False,
                 return_D=False):
    "Finds conserved variables from primitive ones"
    W=tmunu.get_w(ur,a)
    Gamma=gv.Gamma
    sqrt_det = geometry.get_density_factor(r,a,b,gamma_half)

    D_tilde = sqrt_det*W*tmunu.get_rho0(p)
    Sr_tilde=sqrt_det*alpha*tmunu.get_T_tu_rd(rho,p,ur,alpha,a)
    tau_tilde= ((alpha**2)*sqrt_det*tmunu.get_T_tu_tu(rho,p,ur,alpha,a)
                - D_tilde)

    if return_D:
        return D_tilde,Sr_tilde,tau_tilde
    else:
        return Sr_tilde, tau_tilde
# ======================================================================


# ======================================================================
# Get primitive from conserved
# ======================================================================
def to_primitive(r,Sr_tilde,tau_tilde,a,b,alpha):
    p = np.empty_like(r)
    ur = np.empty_like(r)
    p0 = 1e-3
    ur0 = 1e-3
    gamma_half = geometry.get_gamma_half(r,a,b)
    
    for i in range(1,len(r)+1):
        if i > 1:
            p0 = p[-(i-1)]
            ur0 = ur[-(i-1)]
        def f(data):
            p_value = data[0]
            ur_value = data[1]
            rho_value = tmunu.get_rho(p_value)
            Sr_value,tau_value = to_conserved(r[-i],
                                              rho_value,
                                              p_value,
                                              ur_value,
                                              a[-i],b[-i],
                                              alpha[-i])
            Sr_tilde_residual = 1e2*(Sr_tilde[-i] - Sr_value)
            tau_tilde_residual = 1e2*(tau_tilde[-i] - tau_value)
            return np.array([Sr_tilde_residual,
                             tau_tilde_residual])
        sol = optimize.root(f,np.array([p0,ur0]),
                            method='lm',
                            tol=1e-10,
                            options={'maxiter':2000})
        if sol.success:
            p_sol,ur_sol=np.split(sol.x,2)
        else:
            print "solver failed!"
            print "Failure at radius {}".format(r[i])
            print "Status = {}".format(sol.status)
            print sol.message
            print sol.fun
            raise ValueError("Solver failed!")
        p[-i] = p_sol
        ur[-i] = ur_sol
    return p,ur

# ======================================================================
    
