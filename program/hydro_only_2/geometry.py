#!/usr/bin/env python

"""variable_change.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-09-21 12:20:56 (jmiller)>

This file defines various quantities related to geometry, such as
components of the stress-energy tensor, metric determinants, etc.)
"""

import numpy as np

# a and b in flat spacetime
FLAT_A = 1.
FLAT_B = 1.
FLAT_ALPHA = 1.

def get_gamma_half(r,a,b):
    """
    Given the radius and metric components a and b, returns the square
    root of the determinant of the metric
    """
    metric_determinant=r*r*r*r*a*a*b*b*b*b
    sqrt_det=np.sqrt(metric_determinant)
    return sqrt_det

def get_density_factor(r,a,b,gamma_half=False):
    """
    Like Gamma half but may have a different power of r, designed to
    pull r out of the densitization for spherical symmetry.
    """
    if gamma_half:
        return get_gamma_half(r,a,b)
    return a*b*b

def get_flat_metric(r):
    """
    Given a grid r, returns the metric for r in a
    flat spacetime in spherical coordinates.

    Returns:
    g_rr, g_{theta theta}

    then g_{phi phi} = sin^2(theta) g_{theta theta}
    """
    a = FLAT_A
    b = FLAB_B
    g_rr = a*a
    g_theta_theta = r*r*b*b
    return g_rr,g_theta_theta
