#!/usr/bin/env python

"""utilities.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-09-27 20:51:05 (jmiller)>

This file defines some convenient utility functions
"""

import numpy as np
import global_variables as gv

def flatten(a):
    """
    Given an array of shape (num_variables,grid_size), return 1d array
    of length num_variablesxgrid_size
    """
    return np.ravel(a)

def unflatten(a,num_variables):
    """
    Given a raveled array, resize it to the shape (num_variables,grid_size)
    and return
    """
    return np.reshape(a,(num_variables,gv.RES))
