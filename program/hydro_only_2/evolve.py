#!/usr/bin/env python

"""evolve.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-09-27 20:40:13 (jmiller)>

This file is the main evolution loop for the system with hydro
variables only.
"""

import numpy as np
import global_variables as gv
import derivative_gundlach as der
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities
import rhs
import initial_data
from scipy.integrate import ode
import pylab as pl

#r = der.r
r,rho0,p0,ur0,a0,b0,K0,alpha0 = initial_data.gaussian_packet()

def evolve(t0, tfinal, nt,
           r,rho0,p0,ur0,a0,b0,K0,alpha0):
    """
    The main loop. Evolves system from t0 to tfinal with nt steps
    with initial data given by r,rho0,p0,ur0,a,b,K,alpha.
    """
    # initialize output arrays
    t_array = np.linspace(t0,tfinal,nt)
    soln_array = np.empty((len(t_array),gv.NUM_EVOLVED_VARIABLES,gv.RES))
    primitive_array = np.empty((len(t_array),
                                gv.NUM_PRIMITIVE_VARIABLES,
                                gv.RES))
    primitive_array[0] = np.vstack((rho0,p0,ur0))

    # convert to conserved variables
    Sr0,tau0 = variables.to_conserved(r,rho0,p0,ur0,a0,b0,alpha0)

    # initial state
    U0 = np.vstack((Sr0,tau0,a0,b0,K0))
    soln_array[0] = U0
    U0_flat = utilities.flatten(U0)

    # make right-hand-side
    my_rhs = rhs.make_rhs(r)

    integrator = ode(my_rhs)
    integrator.set_integrator('dopri5')# ,max_step=gv.STEP,first_step=gv.STEP)
    integrator.set_initial_value(U0_flat,t0)

    for i, t in enumerate(t_array[1:]):
        integrator.integrate(t)
        if gv.printme:
            print "integrated to {} out of {}.".format(t,tfinal)
        soln_array[i+1] = utilities.unflatten(integrator.y,
                                              gv.NUM_EVOLVED_VARIABLES)
        p,ur = variables.to_primitive(r,
                                      soln_array[i+1,0,...],
                                      soln_array[i+1,1,...],
                                      soln_array[i+1,2,...],
                                      soln_array[i+1,3,...],
                                      soln_array[i+1,4,...])
        rho = tmunu.get_rho(p)
        primitive_array[i+1] = np.array([rho,p,ur])
    # and return
    return t_array,soln_array,primitive_array

if __name__ == "__main__":
    print "Beginning evolution"
    t_array,soln_array,primitive_array = evolve(0,gv.TIME,gv.RESTIME,
                                                r,rho0,p0,ur0,a0,b0,K0,alpha0)
