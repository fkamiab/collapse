#!/usr/bin/env python

"""
rhs.py
Authors: Farbod Kamiab (fkamiab@gmail.com)
         Jonah Miller  (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-09-27 21:32:34 (jmiller)>

This file defines the right-hand-side of the pde system
(d/dt) U = f(t,U)

More precisely, the time evolution of the system U is is given by
(d/dt) U = - (d/dr) FLUX(t,U) + M(t,U)

we caluclate everything on the right-hand-side here.

U = [D_tilde,Sr_tilde,tau_tilde,a,b,K]

where a and b are metric components and
where K is the trace of the extrinsic curvature.
"""

import pylab as pl
import numpy as np
import global_variables as gv
import derivative_gundlach as der
import geometry
import energy_momentum as tmunu
import variable_change as variables
import utilities

NUM_VARIABLES = gv.NUM_EVOLVED_VARIABLES
CONVERSION_MESSAGE="conserved -> primitive failed!"

# symmetries for fluxes for Sr, tau, a, b, K respectively
FLUX_SYMMETRIES = [der.EVEN,der.ODD,der.ODD,der.ODD,der.ODD]

def flux(r,rho,p,ur,Sr_tilde,tau_tilde,a,b,K,alpha,
         gamma_half=False):
    """
    Calculates the flux term for the conserved quantities.
    Defined as FLUX(t,U) above.

    Returns 5 arrays, one for each variable in U. However, the flux
    for a and b is trivial.
    """
    rho0 = tmunu.get_rho0(p)
    W = tmunu.get_w(ur,a)

    density_factor = geometry.get_density_factor(r,a,b,gamma_half)
    T_ru_rd_0 = tmunu.get_T_ru_rd(rho,p,ur,alpha,a)
    T_tu_ru = tmunu.get_T_tu_ru(rho,p,ur,alpha,a)
    vr = tmunu.get_vr(ur,alpha,a)

    fSr = alpha*density_factor*T_ru_rd_0
    ftau = density_factor*((alpha**2)*T_tu_ru - W*rho0*vr)
    fa = np.zeros_like(r)
    fb = np.zeros_like(r)
    fk = np.zeros_like(r)

    return np.vstack((fSr,ftau,fa,fb,fk))

def source(r,rho,p,ur,Sr_tilde,tau_tilde,a,b,
           alpha,Krr=0,Ktt=0,Paether=0,
           gamma_half=False):
    """
    Calculates the source terms, M(t,U) as described above.

    Returns 6 arrays, one for each variable (other than r and alpha)
    given as input. For now, a, b, and K don't evolve, so their source
    terms are trivial.
    
    Calculates the flux term for the conserved quantities.
    Defined as FLUX(t,U) above.

    Returns 6 arrays, one for each variable in U. However, the flux
    for a and b is trivial.
    """
    TthetathetaupTIMESrquared=p/b/b 
    density_factor = geometry.get_density_factor(r,a,b,gamma_half)
    T_tu_tu = tmunu.get_T_tu_tu(rho,p,ur,alpha,a)
    T_ru_ru = tmunu.get_T_ru_ru(rho,p,ur,alpha,a)

    # source term for Sr
    mSr = -alpha*density_factor*(-T_tu_tu*alpha*der.grad(alpha,r,der.EVEN)
                                 +T_ru_ru*a*der.grad(a,r,der.EVEN)
                                 +2*p*(1.0/b)*der.grad(b,r,der.EVEN)
                                 -der.grad(p,r,der.EVEN))
    mtau = alpha*density_factor*(T_ru_ru*a*a*Krr
                                 + 2.0*b*b*Ktt*TthetathetaupTIMESrquared)
    # Metric variables
    ma = np.zeros_like(r)
    mb = np.zeros_like(r)
    mK = np.zeros_like(r)

    return np.vstack((mSr,mtau,ma,mb,mK))

def rhs(t,r,U,gamma_half=False):
    """
    The right-hand-side operator:
    M(t,U) - (d/dr)FLUX(t,U)
    """
    print "\t t = {}".format(t)
    U_2d = utilities.unflatten(U,NUM_VARIABLES)
    Sr_tilde = U_2d[0]
    tau_tilde = U_2d[1]
    a = U_2d[2]
    b = U_2d[3]
    K = U_2d[4]
    # For now, since gravity is static, all K can be assumed to be zero.
    Krr = 0
    Ktt = 0
    # same with the aether
    Paether = 0
    alpha = np.ones_like(r)*geometry.FLAT_ALPHA
    # get primitives
    p,ur = variables.to_primitive(r,Sr_tilde,tau_tilde,
                                  a,b,alpha)
    rho = tmunu.get_rho(p)

    # Get flux
    fU_2D = flux(r,rho,p,ur,Sr_tilde,tau_tilde,
                 a,b,K,alpha,
                 gamma_half)
    # Get source terms
    mU_2D = source(r,rho,p,ur,Sr_tilde,tau_tilde,a,b,
                   alpha,Krr,Ktt,Paether,
                   gamma_half)
    # Get 2D rhs
    rhs_2D = np.empty_like(fU_2D)
    for i, f in enumerate(fU_2D):
        rhs_2D[i] = mU_2D[i] - der.div(f,r,FLUX_SYMMETRIES[i])

    # Dirichlet boundaries
    rhs_2D[...,-1] = 0.0
    
    # flatten
    rhs_1D = utilities.flatten(rhs_2D)

    return rhs_1D

def make_rhs(r):
    """
    Given a radius grid, make the right-hand-side function, which is a closure.
    """
    return lambda t,U: rhs(t,r,U)

